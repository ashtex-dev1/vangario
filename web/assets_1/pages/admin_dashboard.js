$( document ).ready( function ( ) {
    $( function ( ) {
	Highcharts.setOptions( {
	    chart: {
		backgroundColor: {
		    linearGradient: [ 0, 0, 500, 500 ],
		    stops: [
			[ 0, 'rgb(255, 255, 255)' ],
			[ 1, 'rgb(240, 240, 255)' ]
		    ]
		},
		borderWidth: 0,
		plotBackgroundColor: 'rgba(255, 255, 255, .9)',
		plotShadow: true,
		plotBorderWidth: 0,
	    }
	} );

	var total_company_count = $( ".events-count" ).text( );
	var company_name = [ ];
	var publish_count = [ ];
	$( ".rep-company-details" ).each( function () {
	    var par = $( this );
	    var data = JSON.parse( $( "input", par ).val() );
	    console.log( data );
	    company_name.push( data.company_name );
	    publish_count.push( eval( data.company_count ) );
	} );

	Highcharts.chart( 'container', {
	    title: {
		text: 'Company, Published Surveys'
	    },
	    yAxis: {
		title: {
		    text: 'Surveys Analysis'
		}
	    },
	    xAxis: {
		categories: company_name
	    },
	    series: [ {
		    type: 'column',
		    name: 'Published Surveys',
		    data: publish_count
		}
	    ]
	} );
	var chart = Highcharts.chart( {
	    chart: {
		renderTo: 'container1',
		type: 'column',
	    },
	    shadow: {
		color: 'yellow',
		width: 10,
		offsetX: 0,
		offsetY: 0
	    },
	    title: {
		text: 'Companies'
	    },
	    plotOptions: {
		column: {
		    depth: 25
		}
	    },
	    xAxis: {
		categories: [ 'companies' ]
	    },
	    yAxis: {
		title: {
		    text: null
		}
	    },
	    series: [
		{
		    name: 'Companies',
		    data: [ eval( total_company_count ) ]
		}
	    ]

	} );
	//*****************************************************************

    } );
} );
$( document ).ready( function ( ) {
    $( document ).on( "click", "#add-btn", function ( ) {
	var par = $( "#question-model" );
	$( "input[name='action']", par ).val( $( this ).attr( 'data-method' ) );
	$( "input[name='question_id']", par ).val( $( this ).attr( 'data-question-id' ) );
	populate_options_data( {
	    question: {
		id: 0,
		type: 'short',
	    },
	    options: [
		{
		    answer: ""
		}
	    ]
	} );
	$( "#question-model" ).modal( "show" );
    } );
    $( document ).on( "click", "#edit", function ( ) {
	var par = $( "#question-model" );
	var data = JSON.parse( $( this ).attr( 'data-raw-data' ) );
	$( "input[name='action']", par ).val( $( this ).attr( 'data-action' ) );
	$( "input[name='question_id']", par ).val( data.question.id );
	populate_options_data( data );
    } );
    $( document ).on( "change", ".question-type", function ( ) {
	var par = $( this ).closest( '.question' ).parent( );
	var type = $( this ).val( );
	$( '.question .option, .display .option', par ).hide( );
	$( '.question .' + type + '-section, .display .' + type + '-section', par ).fadeIn( 200 );
	update_answer_options( type, par );
    } );
    $( document ).on( 'keyup', '.question #question-title', function ( ) {
	var par = $( this ).closest( '.question' ).parent( );
	$( '.display #question-title', par ).html( $( this ).val( ) );
    } );
    $( document ).on( 'keyup', '.question .multiple input', function ( ) {
	update_answer_options( $( this ).attr( 'id' ), $( this ).closest( '.question' ).parent( ) );
    } );
    $( document ).on( 'click', '.question .ion-android-close', function ( ) {
	var par = $( this ).closest( '.question' ).parent( );
	if ( $( '.multiple', $( this ).closest( '.option' ) ).length > 1 )
	    $( this ).closest( '.multiple' ).remove( );
	update_answer_options( $( this ).attr( 'data-type' ), par );
    } );
    $( document ).on( 'click', '.question .add-more-button', function ( ) {
	var par = $( this ).parent( );
	if ( $( 'div.multiple', par ).length > 7 ) {
	    toastr.error( "Maximum option limit reached...", "Error!!!" );
	    return false;
	} else {
	    var option = $( 'div.multiple:first', par ).clone( );
	    $( 'input', option ).val( '' ).attr( 'data-id', '0' );
	    if ( $( this ).data( 'type' ).indexOf( 'picture' ) > -1 ) {
		$( 'img', option ).attr( 'src', '/assets/dist/img/no-available.jpg' );
	    }
	    option.insertAfter( $( 'div.multiple:last', par ) );
	    update_answer_options( $( this ).attr( 'data-type' ), $( this ).closest( '.question' ).parent( ) );
	}
    } );
    function populate_options_data( data ) {
	switch ( data.question.type ) {
	    case 'short':
	    case 'long':
		var par = $( '.display .' + data.question.type + '-section' );
		$( 'input', par ).tagsinput( 'destroy' );
		$.each( data.options, function ( i, option_data ) {
		    $( 'input', par ).val( option_data.answer );
		} );
		$( 'input', par ).tagsinput( 'items' );
		break;
	    case 'radio':
	    case 'checkbox':
		var par = $( '.question .' + data.question.type + '-section' );
		var clone = $( 'div.multiple:first', par ).clone( );
		$( 'div.multiple', par ).remove( );
		$.each( data.options, function ( i, option_data ) {
		    var option = clone.clone( );
		    $( 'input', option )
			    .val( option_data.label )
			    .attr( 'data-id', option_data.id )
			    .attr( 'data-answer', option_data.answer );
		    option.insertBefore( $( 'div.add-more-button', par ) );
		} );
		break;
	}

	$( '.question #question-title' ).val( data.question.title ).trigger( 'keyup' );
	$( '.question-type' ).val( data.question.type ).trigger( 'change' );
	$( "#question-model" ).modal( "show" );
    }

    function update_answer_options( type, par ) {
	if ( type == "radio" || type == "checkbox" ) {
	    var node = $( '.display .' + type + '-section .entry:first', par ).clone( );
	    $( '.display .' + type + '-section', par ).empty( );
	    $( '.question .' + type + '-section .multiple input', par ).each( function ( ) {
		var clone = node.clone( );
		$( 'input', clone ).attr( 'value', $( this ).val( ) ).removeAttr( "checked" );
		$( 'label', clone ).html( $( this ).val( ) );
		if ( $( this ).attr( 'data-answer' ) == 1 || $( this ).attr( 'data-answer' ) == '1' ) {
		    $( 'input', clone ).attr( 'checked', 'checked' ).trigger( "click" );
		}
		clone.appendTo( $( '.display .' + type + '-section', par ) );
	    } );
	}
    }

    /*
     * Survey Page
     */

    $( document ).on( "click", ".remove-question", function ( ) {
	var par = $( this ).closest( '.question' ).parent( );
	var node_class = '.' + par.attr( 'class' ).split( ' ' ).join( '.' );
	if ( $( node_class ).length > 1 )
	    par.remove( );
    } );
    $( 'select' ).each( function ( ) {
	$( this ).trigger( 'change' );
	var par = $( this ).closest( '.survey-question' ); // '.display .' + data.question.type + '-section'
	$( '.display .' + $( this ).val( ) + '-section input.tagged-input', par ).val(
		$( '.question .' + $( this ).val( ) + '-section input', par ).attr( "data-answer" )
		);
	$( '.display .' + $( this ).val( ) + '-section .bootstrap-tagsinput', par ).remove( );
	$( '.display .' + $( this ).val( ) + '-section input.tagged-input', par ).tagsinput( 'destroy' );
	$( '.display .' + $( this ).val( ) + '-section input.tagged-input', par ).tagsinput( 'items' );
    } );
    $( document ).on( "click", "#add-srv-question", function ( ) {
	var node = $( ".survey-question:first" ).clone( );
	$( 'input', node ).val( '' ).trigger( 'keyup' );
	$( '.question .ion-android-close', node ).each( function ( ) {
	    if ( $( '.multiple', $( this ).closest( '.option' ) ).length > 1 )
		$( this ).closest( '.multiple' ).remove( );
	    update_answer_options( $( this ).attr( 'data-type' ), $( this ).closest( '.question' ).parent( ) );
	} );
	$( '.display .bootstrap-tagsinput', node ).remove( );
	$( '.display .tagged-input', node ).tagsinput( 'destroy' );
	$( '.display .tagged-input', node ).tagsinput( 'destroy' );
	$( '.display .tagged-input', node ).tagsinput( 'items' );
	$( 'select', node ).val( 'short' ).trigger( 'change' );
	// update options
	var par = $( 'select', node ).closest( '.question' ).parent( );
	var type = 'short';
	$( '.question .option, .display .option', par ).hide( );
	$( '.question .' + type + '-section, .display .' + type + '-section', par ).fadeIn( 200 );
	update_answer_options( type, par );
	// ----------

	node.insertAfter( $( ".survey-question:last" ) );
    } );
    $( document ).on( 'click', '.save', function ( ) {
	var all_ok = true;
	/*	var type = $( ".question-type" ).val( );
	 if ( type == 'short' || type == 'long' ) {
	 var answer = $( ".bootstrap-tagsinput input" );
	 var validate = $( answer ).parent().find( ".label-info" ).html();
	 if ( !validate ) {
	 $( ".bootstrap-tagsinput input" ).parent().parent().addClass( 'has-error' );
	 all_ok = false;
	 } else {
	 $( ".bootstrap-tagsinput input" ).parent().parent().removeClass( 'has-error' );
	 }
	 }*/

	var par = $( "#question_form" );
	$( "input[data-required='true']:visible, select[data-required='true']:visible, textarea[data-required='true']:visible", par ).each( function ( ) {
	    if ( !$( this ).val( ).length || $( this ).val( ) === '' || $( this ).val( ) === 0 || $( this ).val( ) === '0' ) {
		$( this ).parent( ).addClass( 'has-error' );
		all_ok = false;
	    } else if ( strcmp( $( this ).attr( 'type' ), 'email' ) === 0 && !validateEmail( $( this ).val( ) ) ) {
		$( this ).parent( ).addClass( 'has-error' );
		all_ok = false;
	    } else
		$( this ).parent( ).removeClass( 'has-error' );
	} );
	if ( all_ok ) {
	    var data = {
		question_id: $( 'input[name="question_id"]', par ).val( ),
		method: $( 'input[name="method"]', par ).val( ),
		action: $( 'input[name="action"]', par ).val( ),
		title: $( 'input[name="question-title"]', par ).val( ),
		type: $( 'select[name="question-type"]', par ).val( ),
		web: $( 'input[name="web"]', par ).val( ),
		options: [ ],
		answer: '',
	    };
	    $( '.question div.' + data.type + '-section input' ).each( function ( ) {
		data.options.push( { value: $( this ).val( ), id: $( this ).attr( "data-id" ) } );
	    } );
	    switch ( data.type ) {
		case 'radio':
		    data.answer = $( ".display input[name='" + data.type + "']:checked" ).val( );
		    break;
		case 'checkbox':
		    $( ".display input[name='" + data.type + "[]']:checked" ).each( function ( ) {
			data.answer += $( this ).val( ) + ",";
		    } );
		    data.answer = data.answer.slice( 0, -1 );
		    break;
		default:
		    data.answer = $( ".display input[name='" + data.type + "']" ).val( );
		    break;
	    }

	    $( this ).btnLoading( 'loading', 'Loading...' );
	    var response = send_ajax( data, par.attr( 'action' ) );
	    setTimeout( function ( ) {
		add_notification( response );
		setTimeout( function ( ) {
		    $( this ).btnLoading( 'reset', 'Save' );
		    if ( response.success ) {
			window.location.reload( );
			/* In Case we are not allowed for page refresh
			 $( 'input[name="action"]', par ).val( response.action );
			 $( 'input[name="question_id"]', par ).val( response.question_id );
			 $.each( response.options, function ( i, data ) {} );
			 */
		    }
		}, 1500 );
	    }, 200 );
	}

	return false;
    } );
    $( document ).on( 'click', '#save-survey', function ( ) {
	var all_ok = true;
	/*	var type = $( ".question-type" ).val( );
	 if ( type == 'short' || type == 'long' ) {
	 var answer = $( ".bootstrap-tagsinput input" );
	 var validate = $( answer ).parent().find( ".label-info" ).html();
	 if ( !validate ) {
	 $( ".bootstrap-tagsinput input" ).parent().parent().addClass( 'has-error' );
	 all_ok = false;
	 } else {
	 $( ".bootstrap-tagsinput input" ).parent().parent().removeClass( 'has-error' );
	 }
	 }*/
	$( "input[data-required='true']:visible, select[data-required='true']:visible, textarea[data-required='true']:visible", par ).each( function ( ) {
	    if ( !$( this ).val( ).length || $( this ).val( ) === '' || $( this ).val( ) === 0 || $( this ).val( ) === '0' ) {
		$( this ).parent( ).addClass( 'has-error' );
		all_ok = false;
	    } else if ( strcmp( $( this ).attr( 'type' ), 'email' ) === 0 && !validateEmail( $( this ).val( ) ) ) {
		$( this ).parent( ).addClass( 'has-error' );
		all_ok = false;
	    } else
		$( this ).parent( ).removeClass( 'has-error' );
	} );

	$( "img[data-required='true']:visible", par ).each( function ( ) {
	    var par = $( this ).parent();
	    if (
		    (
			    !$( 'input[type="file"]', par ).val( ).length ||
			    $( 'input[type="file"]', par ).val( ) === '0' ||
			    $( 'input[type="file"]', par ).val( ) === '' ||
			    $( 'input[type="file"]', par ).val( ) === 0
			    )
		    &&
		    (
			    !$( 'input[type="text"]', par ).attr( 'data-id' ).length ||
			    $( 'input[type="text"]', par ).attr( 'data-id' ) === '0' ||
			    $( 'input[type="text"]', par ).attr( 'data-id' ) === '' ||
			    $( 'input[type="text"]', par ).attr( 'data-id' ) === 0
			    )
		    ) {
		par.addClass( 'has-error' );
		all_ok = false;
	    } else
		par.removeClass( 'has-error' );
	} );

	if ( all_ok ) {
	    var par = $( "#survey_form" );
	    var fd = new FormData( );
	    fd.append( 'survey_id', $( 'input[name="survey_id"]', par ).val( ) );
	    fd.append( 'action', $( 'input[name="action"]', par ).val( ) );
	    fd.append( 'title', $( 'input[name="survey-title"]', par ).val( ) );
	    fd.append( 'web', $( 'input[name="web"]', par ).val( ) );
	    var questions = [ ];
	    $( '.survey-question' ).each( function ( i, el ) {
		var question_id = !$( 'input[name="question_id"]', $( this ) ).val( ) ? 0 : $( 'input[name="question_id"]', $( this ) ).val( );
		fd.append( 'questions[' + i + '][id]', question_id );
		fd.append( 'questions[' + i + '][title]', $( 'input[name="question-title"]', $( this ) ).val( ) );
		fd.append( 'questions[' + i + '][type]', $( 'select[name="question-type"]', $( this ) ).val( ) );
		fd.append( 'questions[' + i + '][options]', '' );
		fd.append( 'questions[' + i + '][answer]', '' );
		if ( $( 'select[name="question-type"]', $( this ) ).val( ) === "checkbox" || $( 'select[name="question-type"]', $( this ) ).val( ) === "radio" ) {
		    $( '.question div.' + $( 'select[name="question-type"]', $( this ) ).val( ) + '-section input', $( this ) ).each( function ( j, el ) {
			fd.append( 'questions[' + i + '][options][' + j + '][value]', $( this ).val( ) );
			fd.append( 'questions[' + i + '][options][' + j + '][id]', $( this ).attr( "data-id" ) );
		    } );
		}
		switch ( $( 'select[name="question-type"]', $( this ) ).val( ) ) {
		    case'single-image':
			$( '.question div.' + $( 'select[name="question-type"]', $( this ) ).val( ) + '-section .multiple', $( this ) ).each( function ( j, el ) {
			    var option_id = $( 'input[type="text"]', $( this ) ).attr( "data-id" );
			    fd.append( 'file[' + question_id + '][' + ( option_id == '' || option_id == 0 || option_id == '0' ? "" : option_id ) + '][single-image]', $( ".picture-file", $( this ) )[0].files[0] );
			    fd.append( 'questions[' + i + '][options][' + j + '][value]', $( '#single-image-option-title', $( this ) ).val( ) );
			    fd.append( 'questions[' + i + '][options][' + j + '][id]', option_id );
			} );
			break;
		    case'multi-image':
			$( '.question div.' + $( 'select[name="question-type"]', $( this ) ).val( ) + '-section .multiple', $( this ) ).each( function ( j, el ) {
			    var option_id = $( 'input[type="text"]', $( this ) ).attr( "data-id" );
			    fd.append( 'file[' + question_id + '][' + ( option_id == '' || option_id == 0 || option_id == '0' ? "" : option_id ) + '][multi-image]', $( ".picture-file", $( this ) )[0].files[0] );
			    fd.append( 'questions[' + i + '][options][' + j + '][value]', $( '#multi-image-option-title', $( this ) ).val( ) );
			    fd.append( 'questions[' + i + '][options][' + j + '][id]', option_id );
			} );
			break;
		    case'rating-image':
			$( '.question div.' + $( 'select[name="question-type"]', $( this ) ).val( ) + '-section .multiple', $( this ) ).each( function ( j, el ) {
			    var option_id = $( 'input[type="text"]', $( this ) ).attr( "data-id" );
			    fd.append( 'file[' + question_id + '][' + ( option_id == '' || option_id == 0 || option_id == '0' ? "" : option_id ) + '][rating-image]', $( ".picture-file", $( this ) )[0].files[0] );
			    fd.append( 'questions[' + i + '][options][' + j + '][value]', $( '#rating-image-option-title', $( this ) ).val( ) );
			    fd.append( 'questions[' + i + '][options][' + j + '][id]', option_id );
			} );
			break;
		    case 'radio':
			fd.append( 'questions[' + i + '][answer]', $( ".display input[name='" + $( 'select[name="question-type"]', $( this ) ).val( ) + "']:checked", $( this ) ).val( ) );
			break;
		    case 'checkbox':
			var answers = '';
			$( ".display input[name='" + $( 'select[name="question-type"]', $( this ) ).val( ) + "[]']:checked", $( this ) ).each( function ( ) {
			    answers += $( this ).val( ) + ",";
			} );
			fd.append( 'questions[' + i + '][answer]', answers.slice( 0, -1 ) );
			break;
		    default:
			fd.append( 'questions[' + i + '][answer]', $( ".display input[name='" + $( 'select[name="question-type"]', $( this ) ).val( ) + "']", $( this ) ).val( ) );
			break;
		}
	    } );
	    $( '.overlay' ).fadeIn( 200 );
	    setTimeout( function ( ) {
		var response = send_ajax_with_additional_prams( fd, par.attr( 'action' ) );
		add_notification( response );
		setTimeout( function ( ) {
		    if ( response.success ) {
			window.location.href = response.target;
			/* In Case we are not allowed for page refresh
			 $( 'input[name="action"]', par ).val( response.action );
			 $( 'input[name="question_id"]', par ).val( response.question_id );
			 $.each( response.options, function ( i, data ) {
			 
			 } );
			 */
		    } else
			$( '.overlay' ).fadeOut( 200 );
		}, 1500 );
	    }, 200 );
	}

	return false;
    } );
    $( document ).delegate( '#picture', 'click', function ( ) {
	var par = $( this ).parent( );
	$( ".picture-file", par ).click( );
	function readURL( input ) {
	    if ( input.files && input.files[0] ) {
		var reader = new FileReader( );
		reader.onload = function ( e ) {
		    $( '#picture', par ).attr( 'src', e.target.result );
		}

		reader.readAsDataURL( input.files[0] );
	    }
	}

	$( ".picture-file", par ).change( function ( ) {
	    readURL( this );
	} );
    } );
} );

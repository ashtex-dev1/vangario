$( document ).ready( function () {
    $( "form#register_form, form#profile_form" ).submit( function () {
	var all_ok = true;
	var par = $( this );
	$( "input[data-required='true'], select[data-required='true'], textarea[data-required='true']", par ).each( function () {
	    if ( !$( this ).val().length || $( this ).val() === '' ) {
		$( this ).parent().addClass( 'has-error' );
		all_ok = false;
	    } else if ( strcmp( $( this ).attr( 'type' ), 'email' ) === 0 && !validateEmail( $( this ).val() ) ) {
		$( this ).parent().addClass( 'has-error' );
		all_ok = false;
	    } else
		$( this ).parent().removeClass( 'has-error' );
	} );

	if ( strcmp( $( 'input[name="password"]' ).val(), $( 'input[name="confirm_password"]' ).val() ) !== 0 ) {
	    $( 'input[name="confirm_password"]' ).parent().addClass( 'has-error' );
	    all_ok = false;
	} else {
	    $( 'input[name="confirm_password"]' ).parent().removeClass( 'has-error' );
	}

	return all_ok;
    } );

    $( "form#store_form" ).submit( function () {
	var all_ok = true;
	var par = $( this );
	$( "input[data-required='true'], select[data-required='true'], textarea[data-required='true']", par ).each( function () {
	    if ( !$( this ).val().length || $( this ).val() === '' ) {
		$( this ).parent().addClass( 'has-error' );
		all_ok = false;
	    } else if ( strcmp( $( this ).attr( 'type' ), 'email' ) === 0 && !validateEmail( $( this ).val() ) ) {
		$( this ).parent().addClass( 'has-error' );
		all_ok = false;
	    } else
		$( this ).parent().removeClass( 'has-error' );
	} );

	if ( all_ok ) {
	    var data = $( this ).serializeObject();
	    $( '.overlay' ).fadeIn( 200 );
	    var response = send_ajax( data, par.attr( 'action' ) );
	    setTimeout( function () {
		add_notification( response );
		$( '.overlay' ).fadeOut( 200 );
	    }, 200 );
	}

	return false;
    } );
} );
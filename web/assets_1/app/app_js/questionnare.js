$( document ).ready( function ( ) {

   $( ".fancybox" ).on( "click", function ( ) {
	$( "#" + $( this ).attr( "id" ) ).fancybox( {
	    helpers: {
		title: {
		    type: 'float'
		}
	    }
	} );
    } );

/*    $( ".fancybox" ).fancybox( {
	prevEffect: 'none',
	nextEffect: 'none',
    } );*/

    $( document ).delegate( ".fancybox-prev", "click", function () {
	$( '.fancybox' ).addClass( 'show' );
    } );
    $( document ).delegate( ".fancybox-next", "click", function () {
	$( '.fancybox' ).addClass( 'show' );
    } );
    $( document ).delegate( ".fancybox-close", "click", function () {
	$( '.fancybox' ).addClass( 'show' );
    } );
    $( ".fancybox" ).on( "click", function () {
	$( this ).addClass( 'show' );
    } );
    $( '.questionare' ).each( function ( ) {
	if ( $( this ).attr( 'data-num' ) == "0" || $( this ).attr( 'data-num' ) == "undefined" || $( this ).attr( 'data-num' ) == "" ) {
	    $( this ).remove( );
	}
    } );
    $( '.rating-image-section' ).each( function ( ) {
	if ( $( this ).attr( 'data-type' ) == "rating-image" ) {
	    var node = $( '.rating-image-select' );
	    for ( var i = 1; i <= node.length; i++ ) {
		$( node ).append( "<option value='" + i + "'>" + i + "</option>" );
	    }
	}
    } );
    $( document ).off( 'change', 'select.rating-image-select' );
    $( document ).on( 'change', 'select.rating-image-select', function ( ) {
	var selected = $( this ).val( );
	var num = $( this ).attr( 'data-id' );
	$( 'option[value!="' + selected + '"]', $( this ) ).remove( );
	$( ".rating-image-select" ).each( function ( ) {
	    if ( num != $( this ).attr( 'data-id' ) )
		$( 'option[value="' + selected + '"]', $( this ) ).remove( );
	} );
    } );
    $( document ).off( 'click', '.reset' );
    $( document ).on( 'click', '.reset', function ( ) {
	var options = "<option value=''>Rate Image</option>";
	for ( var j = 1; j <= $( '.option', $( this ).closest( '.rating-image-section' ) ).length; j++ ) {
	    options += "<option value='" + j + "'>" + j + "</option>";
	}

	$( "select", $( this ).closest( '.rating-image-section' ) ).each( function ( i, el ) {
	    $( this ).attr( 'data-num', i ).html( options );
	} );
    } );
    //-----------------
    var active_node = $( '.questionare:visible' );
    if ( $( ".lft_iconImg" ).hasClass( active_node.attr( "data-type" ) ) ) {
	$( "." + active_node.attr( "data-type" ) ).addClass( "activeImg" );
    }
//-----------------
    $( document ).off( 'click', '.next' );
    $( ".next" ).on( "click", function ( ) {
	var all_ok = true;
	var active_node = $( '.questionare:visible' );
	var index = parseInt( active_node.attr( 'data-num' ) );
//	console.log( $( "." + $( ".questionare" ).closest( "div[data-num=" + ( index + 1 ) + "]" ).attr( 'data-type' ) ) );
	if ( active_node.attr( "data-type" ) == "radio" || active_node.attr( "data-type" ) == "checkbox" || active_node.attr( "data-type" ) == "single-image" || active_node.attr( "data-type" ) == "multi-image" ) {
	    all_ok = $( 'input', active_node ).is( ":checked" );
	} else if ( active_node.attr( "data-type" ) == "rating-image" ) {
	    var arr = new Array( );
	    $( "select" ).each( function ( ) {
		var value = $( this ).val( ).trim( ) != "";
		arr.push( value );
	    } );
	    $.each( arr, function ( i, val ) {
		if ( !val ) {
		    all_ok = false;
		}
	    } );
	} else {
	    all_ok = $( 'input, textarea, select', active_node ).val( ).trim( ) != "";
	}

	if ( all_ok ) {
	$( ".prev" ).show( );
	    if ( $( ".questionare" ).closest( "div[data-num=" + ( index + 1 ) + "]" ).attr( 'data-type' ) ) {
		if ( $( ".lft_iconImg" ).hasClass( $( ".questionare" ).closest( "div[data-num=" + ( index + 1 ) + "]" ).attr( 'data-type' ) ) ) {
		    $( "." + $( ".questionare" ).closest( "div[data-num=" + ( index ) + "]" ).attr( 'data-type' ) ).removeClass( "activeImg" );
		    $( "." + $( ".questionare" ).closest( "div[data-num=" + ( index + 1 ) + "]" ).attr( 'data-type' ) ).addClass( "activeImg" );
		}
	    }

	    if ( $( '[data-num="' + ( index + 1 ) + '"]' ).length ) {
		$( '.questionare' ).hide( );
		$( '[data-num="' + ( index + 1 ) + '"]' ).show( );
		$( '.prev' ).removeAttr( "disabled" );
		if ( !$( '[data-num="' + ( index + 2 ) + '"]' ).length ) {
		    $( ".next" ).hide( );
		    $( this ).attr( "disabled", "disabled" ).hide( );
		    $( '.submit' ).show( );
		}
	    } else {
		$( ".next" ).hide( );
		$( this ).attr( "disabled", "disabled" ).hide( );
		$( '.submit' ).show( );
	    }
	} else {
	    swal( {
		title: "Fill Question",
		text: "Please fill given question's answer",
		type: "warning",
	    }, function ( ) {
	    } );
	}
    } );
    $( ".fill-question" ).on( "click", function ( ) {
	$( ".empaty-question" ).hide( );
	$( ".modal-backdrop" ).hide( );
    } );
    $( document ).off( 'click', '.prev' );
    $( document ).on( 'click', '.prev', function ( ) {
	var index = parseInt( $( '.questionare:visible' ).attr( 'data-num' ) );
	if ( index == '2' ) {
	    $( this ).hide( );
	    $( "." + $( ".questionare" ).closest( "div[data-num=" + ( index ) + "]" ).attr( 'data-type' ) ).removeClass( "activeImg" );
	}

	if ( $( ".questionare" ).closest( "div[data-num=" + ( index ) + "]" ) ) {
	    if ( $( ".lft_iconImg" ).hasClass( $( ".questionare" ).closest( "div[data-num=" + ( index - 1 ) + "]" ).attr( 'data-type' ) ) ) {
		if ( index != '2' ) {
		    $( "." + $( ".questionare" ).closest( "div[data-num=" + ( index ) + "]" ).attr( 'data-type' ) ).removeClass( "activeImg" );
		}
		$( "." + $( ".questionare" ).closest( "div[data-num=" + ( index - 1 ) + "]" ).attr( 'data-type' ) ).addClass( "activeImg" );
	    }
	}

	if ( $( '[data-num="' + ( index - 1 ) + '"]' ).length ) {
	    $( '.questionare' ).hide( );
	    $( '[data-num="' + ( index - 1 ) + '"]' ).show( );
	    $( '.submit' ).hide( );
	    $( '.next' ).show( ).removeAttr( "disabled" );
	    if ( !$( '[data-num="' + ( index - 2 ) + '"]' ).length ) {
		$( ".next" ).show( );
		$( this ).attr( "disabled", "disabled" ).hide( );
		$( '.submit' ).hide( );
	    }
	} else {
	    $( ".next" ).hide( );
	    $( this ).attr( "disabled", "disabled" ).hide( );
	    $( '.submit' ).show( );
	}
    } );
    $( document ).off( 'click', '.submit' );
    $( document ).on( 'click', '.submit', function ( ) {
	var all_ok = true;
	var active_node = $( '.questionare:visible' );
	if ( active_node.attr( "data-type" ) == "radio" || active_node.attr( "data-type" ) == "checkbox" || active_node.attr( "data-type" ) == "single-image" || active_node.attr( "data-type" ) == "multi-image" ) {
	    all_ok = $( 'input', active_node ).is( ":checked" );
	} else if ( active_node.attr( "data-type" ) == "rating-image" ) {
	    var arr = new Array( );
	    $( "select" ).each( function ( ) {
		var value = $( this ).val( ).trim( ) != "";
		arr.push( value );
	    } );
	    $.each( arr, function ( i, val ) {
		if ( !val ) {
		    all_ok = false;
		}
	    } );
	} else {
	    all_ok = $( 'input, textarea', active_node ).val( ).trim( ) != "";
	}

	if ( all_ok ) {
	    var data = {
		action: api_actions.SURVEY_RESPONSE,
		user: $( "#user_id" ).val( ),
		survey: $( "#survey_id" ).val( ),
		method: api_actions.SUBMITTED,
		questions: [ ]
	    };
	    $( ".questionare" ).each( function ( ) {
		var answer = {
		    id: $( this ).attr( "data-id" ),
		    options: [ ],
		};
		switch ( $( this ).attr( 'data-type' ) ) {
		    case 'single-image':
		    case 'radio':
			answer.options.push( {
			    id: $( "input:checked", $( this ) ).attr( "data-id" ),
			    value:
				    $( "input:checked", $( this ) ).val( )
			} );
			break;
		    case 'checkbox':
		    case 'multi-image':
			$( "input:checked", $( this ) ).each( function ( ) {
			    answer.options.push( {
				id: $( this ).attr( "data-id" ),
				value:
					$( this ).val( ),
			    } );
			} );
			break;
		    case 'rating-image':
			$( "select", $( this ) ).each( function ( ) {
			    answer.options.push( {
				id: $( this ).attr( "data-id" ),
				value: $( this ).val( ),
			    } );
			} );
			break;
		    default:
			answer.options.push( {
			    id: $( "input, textarea", $( this ) ).attr( "data-id" ),
			    value: $( "input, textarea", $( this ) ).val( ),
			} );
			break;
		}
		data.questions.push( answer );
	    } );
//	    console.log( data );

	    send_request( api_base_url, "POST", data, function ( data ) {
		if ( data.success ) {
		    swal( {
			title: "Successful",
			text: "Thank you for filling out this survey. RS. 30,  has been added into your account.",
			type: "success",
		    }, function ( ) {
			window.location.replace( '/dashboard' );
		    } );
		} else {
		    swal( {
			title: "Already",
			text: "You have already submitetd this survey.",
			type: "warning",
		    }, function ( ) {
			window.location.replace( '/dashboard' );
		    } );
		}
	    } );
	} else {
	    swal( {
		title: "Fill Question",
		text: "Please fill given question's answer",
		type: "warning",
	    }, function ( ) {
//		window.location.replace( '/dashboard' );
	    } );
	}
    } );
} );

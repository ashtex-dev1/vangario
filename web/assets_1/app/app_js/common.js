$( document ).ready( function () {
//-----------Modals-----------------
    $( ".close" ).on( "click", function () {
	$( ".modal" ).hide();
	$( ".modal-backdrop" ).hide();
    } );
    $( ".closed" ).on( "click", function () {
	$( ".modal" ).hide();
	$( ".modal-backdrop" ).hide();
    } );
    $( ".search-cross-close" ).on( "click", function () {
	$( ".modal" ).hide();
	$( ".modal-backdrop" ).hide();
    } );

    $( "#search-main" ).on( "click", function () {
	$( ".search-modal" ).show();
	$( ".modal-backdrop" ).show();
    } );
//-----------Modals------------------

//----------------Search-------------
    $( '#search' ).keyup( function () {
	var val = $.trim( this.value ).toUpperCase();
	$( ".search_title" ).each( function () {
	    var parent = $( this ).closest( 'li' ),
		    length = $( this ).text().length > 0;

	    if ( $( '.search_ul > li:visible' ).length == 0 && length > 0 ) {
		$( ".no-record" ).removeClass( "hide" );
	    } else {
		$( ".no-record" ).addClass( "hide" );
	    }

	    if ( length && $( this ).text().search( new RegExp( val, "i" ) ) < 0 ) {
		parent.addClass( "hide" );
	    } else {
		parent.removeClass( "hide" );
	    }
	} );
    } );

    $( document ).delegate( '.remove-node', 'click', function ( ) {
	swal( {
	    title: "Are you sure?",
	    text: "You will not be able to revert this operation!",
	    type: "warning",
	    showCancelButton: true,
	    confirmButtonColor: "#DD6B55",
	    confirmButtonText: "Yes, delete it!"
	},
		function () {
		    $.post( action, {
			id: id,
			action: method,
		    }, function ( data ) {
			data = JSON.parse( data );
			if ( data.success )
			    window.location.reload( );
			else
			    add_notification( data );
		    } );
		    // swal( "Deleted!", "Your imaginary file has been deleted.", "success" );
		} );
    } );

//----------------Search--------------
} );
$( document ).ready( function ( ) {

    $( 'h3[data-toggle="tooltip"]' ).tooltip( );
    var $lis = $( ".likes_ul li" ).hide( );
    $lis.slice( 0, 10 ).show( );
    var size_li = $lis.length;
    $( ".current-page" ).html( parseInt( size_li / 10 ) + 1 );
    if ( size_li > 10 ) {
	$( ".forward" ).removeClass( "hide" );
    }

    var x = 10, start = 0;
    $( document ).off( 'click', '.forward' );
    $( ".forward" ).on( "click", function () {

	if ( start == size_li ) {
	    $( this ).addClass( "hide" );
	}
	if ( start + x < size_li ) {
	    $( ".first-page" ).html( ( parseInt( $( ".first-page" ).html( ) ) + 1 ) );
	    $lis.slice( start, start + x ).hide( );
	    start += x;
	    $lis.slice( start, start + x ).show( );
	    $( ".prev" ).removeClass( "hide" );
	} else {
	    $( this ).addClass( "hide" );
	}
    } );

    $( document ).off( 'click', '.prev' );
    $( ".prev" ).on( "click", function () {
	$( ".forward" ).removeClass( "hide" );
	if ( start - x >= 0 ) {
	    $( ".first-page" ).html( ( parseInt( $( ".first-page" ).html( ) ) - 1 ) );
	    $lis.slice( start, start + x ).hide( );
	    start -= x;
	    $lis.slice( start, start + x ).show( );
	} else {
	    $( this ).addClass( "hide" );
	}
    } );

    $( ".refresh-likes" ).on( "click", function ( ) {
	swal( {
	    title: "Are you sure?",
	    text: "Do you want to refresh likes?",
	    type: "warning",
	    showCancelButton: true,
	    confirmButtonColor: "#DD6B55",
	    confirmButtonText: "Yes!"
	},
		function () {
		    $( "#preloader" ).show();
		    $( "div[data-loader='circle-side']" ).show();
		    get_user_fb_details();
		} );
    } );
} );
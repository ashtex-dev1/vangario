
var in_progress = false;
var servicename = 'BrandVibe';
var total_vars = 0;
var URL = "";
var likes = [ ];
var api_base_url = 'http://dev.brandvibe.io/api/';
var refresh_interests = false;

var api_actions = {
    // actions
    USER_UPDATE: '1',
    SURVEY_DETAILS: '2',
    SURVEY_RESPONSE: '3',
    USER_DETAILS: '4',
    SURVEYS: '5',
    FILLED_SURVEYS: '6',
    PUBLIC_URL: '7',
    // methods
    SUBMITTED: '1',
    ABORTED: '0',
    GET: '1',
    UPDATE: '2',
};
var global_vars = {
    DEVICE_ID: 'deviceID',
    USER_ID: 'appUserID',
    USER_DATA: 'appUserData',
    USER_FB_DATA: 'appUserFBData',
    SURVEY_DATA: 'surveyData',
    SURVEY_COUNT: 'surveyCount',
    FB_ID: 'fbID',
    FB_ACCESS_TOKEN: 'accessToken',
    FB_IMAGE: 'fbImage',
    FB_PERMISSIONS: 'email, public_profile, user_work_history, user_education_history, user_birthday, user_likes, user_relationships, user_relationship_details, user_hometown,user_location, user_friends',
    FB_FIELDS: 'id,name,location,birthday,languages,relationship_status,work,hometown,age_range,currency,gender,inspirational_people,interested_in,email,education,likes',
};

fb_init();

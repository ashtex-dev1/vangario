<?php

/**
 * @Package  Config\\ Constants
 * @author Bugs - Ashtex
 * The constant define class for the package
 */

namespace Config;

class Constants
{

    const ON_SCREEN_DEBUG = 0;
    // constansts for install utility
    const PACKAGE_NAME = 'Config/dumps';
    const ARG_NAME = 'mysql_root';
    const DB_RE_INSTALL_MSG = 'The database already exists. Please type "yes" to confirm that you want to delete and install it again';
    const DB_RE_INSTALL_GOAHEAD = 'yes';
    const HTML_PATH = '/Views';
    const EXCEL_PATH = '/files/';
    const PHASE1_HTML_PATH = 'phase1';
    const ARCHIVE = 'archive';
    // pagination for the entire package
    const PAGINATION_COUNT = 20;
    // logging directory
    const LOG_DIR = "/Logs/"; //The directory will not be created automatically. Please create it manually
    // constants for global settings incase they are missing from the database --- safe fall back
    const GLOBAL_SETTING_ID = 1;
    // to enable the fragmentation
    const ENABLE_FRAGMENTIFY = 1;
    //-----------------------------------------------------------
    const UNKNOWN_ERROR = "The error source cannot be identified";
    const DBHOST_NOTAVAILABLE = "The database host is not defined";
    const DBNAME_NOTAVAILABLE = "The database name is not defined";
    const DBUSER_NOTAVAILABLE = "The database user is not defined";
    const DBPASSWORD_NOTAVAILABLE = "The database password is not defined";
    const DBDUMP_NOTAVAILABLE = "The database dump file is not available";
    const DB_USERPASSWORD_NOTAVAILABLE = "The provided password is either empty or null";
    const DBCONNECTION_NOTAVAILABLE = "Connection to the Database failed";
    const DBDELETE_ERROR = "Unable to delete the specified database";
    const DBCREATE_ERROR = "Unable to create the specified database";
    const DBSELECT_ERROR = "Unable to select the specified database";
    const DUMP_FILEOPEN_ERROR = "Unable to open the sql dump file";
    const DUMP_CORRUPTED = "The dump file is corrupted. Unable to load to load the dump";
    const ARG_NOTAVAILABLE = "The argument passed is rejected. Please provide argument as 'mysql_root=MYROOTPASS'";
    const ALIAS_NOTAVAILABLE = "Alias cannot be null or empty";
    const DBINSERT_ERROR = "Unable to insert data into the database";
    const SMS_SERVICE_PRICE = 0.5;
    const MARKETING_REPORT_PRICE = 0.5;
    // ---------------------------
    const STATUS = "status";
    const STATUS_ALL = "all";
    const STATUS_ACTIVE = "active";
    const STATUS_INACTIVE = "inactive";
    const STATUS_ARCHIVED = "archived";
    const STATUS_DELETED = "deleted";
    const STATUS_CONFIRMED = "1";
    // ---------------------------
    const INFUSION_SOFT_CLIENT_ID = "j56v3w6kv6xzmeyjztazabbg";
    const INFUSION_SOFT_CLIENT_SECRET = "wU4AwtKjQQ";
    const INFUSION_SOFT_CLIENT_REDIRECT_URI = "http://162.243.108.220/?r=ImportClientsAction";
    //----------------------------
    const ERROR_PAGE = "/?r=Login";
    const ERROR_MESSAGE = "Something went wrong. Please try again.";
    //----------------------------
    const USER_ROLE_ADMIN = "admin";
    const USER_ROLE_COMPANY = "company";
    const USER_ROLE_USER = "user";
    //----------------------------
    const BEANSTALKD_HOST = "127.0.0.1";
    const BEANSTALKD_PORT = "11300";
    const BEANSTALKD_LIKES_TUBE = "brand_vibe_likes_tube_staging";
    //----------------------------
    const STATUS_PENDING = "pending";
    const STATUS_PARTIAL = "partial";
    const STATUS_PAID = "paid";
    // Survey statuses
    const STATUS_DRAFT = "draft";
    const STATUS_PUBLISHED = "published";
    const STATUS_UNPUBLISHED = "un-published";
    //----------------------------
    const FACEBOOK_APP_ID = "2000798346815610";
    const FACEBOOK_APP_SECRET = "23b52c6c58de24fc6c1cd7749228e694";
    //----------------------------
    const FROM_USER = 'BrandVibe';
    const FROM_EMAIL = 'dojomagic@gmail.com';
    const FOLLOWUP_FROM_EMAIL = "all@thewoodlandsluxurypoolservice.com";
    const FROM_EMAIL_PASS = 'qwffejeuugybsgqt';
    const EMAIL_SMTP = 'smtp.gmail.com';
    const EMAIL_SMTP_PORT = 465;
    const WELCOME_EMAIL_SUBJECT = 'Welcome to The Woodlands Luxury Pool Service';
    const WELCOME_EMAIL_BODY = "
	    <p style='padding:25px 15px 5px 0px;font:normal 18px Arial,serif;text-align:center;color:#000000'>
		Account opening confirmation
	    </p>
	    <p style='font:normal 18px Arial,serif;color:#000000'>Dear %USER_NAME%! </p>
	    <p style='color:#000000'>
		Thank you for applying for an account on 'The Woodlands Luxury Pool Service'. 
		Your request to open an account has been received. It is currently pending due to 
		approval by the site administrator. To complete the sign up procedure, you will 
		receive a confirmation email shortly with the activation link.
	    </p>
	    <p style='color:#000000'>
		We appreciate your patience. If you have any queries, feel free to contact us.
	    </p>
	    <p style='color:#000000'>Thank you for choosing The Woodlands Luxury Pool Service!</p><br/>
	    <p style='color: #757679'><span>Customer Service</span><br/>
	    <span>Email: info@thewoodlandsluxurypoolservice.com</span><br/>
	    <span>Web: http://www.thewoodlandsluxurypoolservice.com/</span></p>
	";
    const ADMIN_WELCOME_EMAIL_BODY = "
	    <p style='padding:25px 15px 5px 0px;font:normal 18px Arial,serif;text-align:center;color:#000000'>
		Account opening confirmation
	    </p>
	    <p style='font:normal 18px Arial,serif;color:#000000'>Dear %USER_NAME%! </p>
	    <p style='color:#000000'>
		Your account has been created on 'The Woodlands Luxury Pool Service'. 
		To complete the registration procedure, Please <a href='%NEW_ACCOUNT_LINK%'>click here</a> 
		To Login.If you didn't request this activation, you can ignore this email.
	    </p>
	    <p style='color:#000000'>
		We appreciate your patience. If you have any queries, feel free to contact us.
	    </p>
	    <p style='color:#000000'>Thank you for choosing The Woodlands Luxury Pool Service!</p><br/>
	    <p style='color: #757679'><span>Customer Service</span><br/>
	    <span>Email: info@thewoodlandsluxurypoolservice.com</span><br/>
	    <span>Web: http://www.thewoodlandsluxurypoolservice.com/</span></p>
	";
    const CONFIRMATION_EMAIL_BODY = "
	    <p style='text-align:center;color:#000000'> <strong> Welcome To 'The Woodlands Luxury Pool Service'!</strong> </p>
	    <p style='font:normal 18px Arial,serif;color:#000000'>Dear %USER_NAME%! </p>
	    <p style='color:#000000'>
		Thank you for signing up for 'The Woodlands Luxury Pool Service'. Your Account has been Successfully Verified. 
		To complete the sign up procedure, Please <a href='%NEW_ACCOUNT_LINK%'>click here</a> 
		To Login.If you didn't request this activation, you can ignore this email.
	    </p>
	    <p style='color:#000000'>
		FYI: Privacy is important to us. We will not sell, rent, or give your name or address 
		to anyone.
	    </p>
	    <p style='color:#000000' >
		Thank you again for joining! If you have any questions or comments, feel free to 
		contact us.
	    </p><br/>
	    <p style='color:#757679'><span>Sincerely, <br/> Customer Service </span> <br/>
	    <span>Email: info@thewoodlandsluxurypoolservice.com</span><br/>
	    <span>Web: http://www.thewoodlandsluxurypoolservice.com/</span></p>
	";
    const FORGET_EMAIL_BODY = "
	    <p style='padding:25px 15px 5px 0px;font:normal 18px Arial,serif'>
		Change Password Request
	    </p>
	    <p>
		You are receiving this email because a request has been made for a reset 
		password link for %USER_EMAIL%.<br>
		<a href='%RESET_PASSWORD_LINK%'>
		Change my password</a> <br>
		If you didn't request this, please ignore this email. 
		Your password won't change until you access the link above and create a new one.
	    </p>
	    <p style='color:#757679'>Sincerely, \n Customer Service</p>
	    <span>Email: info@brandvibe.com</span><br/>
	    <span>Web: http://www.brandvibe.com/</span></p>
	";
    const DANGER_LEVEL_EMAIL_BODY = "
             <p style='padding:25px 15px 5px 0px;font:normal 18px Arial,serif'>
		Warning
	    </p>
            <p>
            Some values has been reached to danger level for %CLIENT_NAME%. Please review the following details:<br><br>
            
            Filter Pressure : %FILTER_PRESSURE%<br>
            Chlorine Reading: %CLORINE_READING%<br>
            
            </p><br>
            <p style='color:#757679'>Sincerely, \n Customer Service<br/>
	    <span>Email: info@thewoodlandsluxurypoolservice.com</span><br/>
	    <span>Web: http://www.thewoodlandsluxurypoolservice.com/</span></p>"

    ;
    const FOLLOWUP_EMAIL_BODY = "
			 <b>Dear Admin,</b>
             <p style='padding:5px 10px 0px 0px;font:normal 18px Arial,serif'>
		Follow up will be required for Client. Please review the following details:<br>
	    </p>
            <p>
            Contact Id : %CONTACT_ID%<br>Name : %CLIENT_NAME% <br>Address : %ADDRESS%<br>Email : %EMAIL%<br>Phone No : %PHONE%<br><br>
            <b>General Notes </b>: %GENERAL_NOTES%<br>
            
            </p>
            <p >Thank You</p>"

    ;

//" . $this->central->get_server_url() . "?r=SChangePassword&" . Central::base64url_encode( 'temp_link' ) . "={$temporary_link}
//" . $this->central->get_server_url() . "?r=SLogin&" . Central::base64url_encode( "new_account" ) . "=" . Central::base64url_encode( "yes" ) . "
}

?>

<?php

@session_start();

use Config\Constants;
use Config\Central;
use Config\URLs;

class LoginAction implements RocketSled\Runnable
{

    //--private members
    private $profile = "login";
    private $central;

    //--constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function run()
    {
	try
	{
	    $this->update_main_contents();
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $corrupt = false;
	    $email = $this->central->getargs( 'email', $_POST, $corrupt );
	    $password = $this->central->getargs( 'password', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		try
		{
		    $user = Plusql::from( $this->profile )->user->select( '*' )->where( "email='{$email}' AND status = '" . Constants::STATUS_ACTIVE . "'" )->limit( '0,1' )->run()->user;
		    $bcrypt = new \Config\Bcrypt();
		    if ( $bcrypt->verify( $password, $user->password ) )
		    {
			$_SESSION[ 'user' ][ 'user_id' ] = $user->user_id;
			$_SESSION[ 'user' ][ 'role' ] = $user->role;
			$_SESSION[ 'user' ][ 'name' ] = ucwords( $user->user_name );
			$_SESSION[ 'user' ][ 'registration' ] = $user->created_at;
			unset( $_SESSION[ 'error' ] );
			if ( $this->central->getargs( 'logged_in', $_POST, $corrupt, 0 ) )
			    @setcookie( 'user', serialize( $_SESSION[ 'user' ] ), time() + ( 3600 * 24 * 365 ), '/' );

			@header( 'location: ' . URLs::DASHBOARD );
		    }
		    else
		    {
			$_SESSION[ 'error' ][ 'email' ] = 0;
			$_SESSION[ 'error' ][ 'password' ] = 1;
			@header( 'location: ' . URLs::LOGIN );
		    }
		}
		catch ( Exception $ex )
		{
		    $_SESSION[ 'error' ][ 'email' ] = 1;
		    $_SESSION[ 'error' ][ 'password' ] = 0;
		    @header( 'location: ' . URLs::LOGIN );
		}
	    }
	    else
	    {
		$_SESSION[ 'error' ][ 'email' ] = 1;
		$_SESSION[ 'error' ][ 'password' ] = 1;
		@header( 'location: ' . URLs::LOGIN );
	    }
	}
	catch ( Exception $ex )
	{
	    $_SESSION[ 'error' ][ 'email' ] = 1;
	    $_SESSION[ 'error' ][ 'password' ] = 1;
	    @header( 'location: ' . URLs::LOGIN );
	}
    }

}

?>
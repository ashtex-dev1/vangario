<?php

use Config\Central;
use Config\Constants;
use Config\URLs;

class RegisterAction implements \RocketSled\Runnable
{

    //--private members
    protected $central;
    protected $profile = 'dln';
    protected $esc;
    private $info = array();
    private $redirect;
    private $insert;

    //--public members
    //--constructor
    public function __construct()
    {
	try
	{
	    // date_default_timezone_set("America/Chicago");
	    @session_start();
	    // Load DB credentials and supported classes
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function run()
    {
	try
	{
	    if ( isset( $_POST[ "email" ] ) && isset( $_POST[ "agreed" ] ) )
	    {
		$data = $this->get_form_data();
		if ( $data && !$this->check_user_existance() )
		{
		    Plusql::into( $this->profile )->users( $data )->insert();
		    $email_body = str_replace( "%USER_NAME%", ucwords( $data[ "first_name" ] ), Constants::WELCOME_EMAIL_BODY );
		    $email_data = array( 'email' => $data[ "email" ],
			'subject' => Constants::WELCOME_EMAIL_SUBJECT,
			'text_body' => str_replace( '<br/>', '\n', $email_body ),
			'html_body' => str_replace( '\n', '<br />', $email_body )
		    );
		    $this->central->send_email( $email_data );
		    $_SESSION[ "success" ][ "signup" ] = true;
		    $location = URLs::LOGIN;
		}
		else
		    $location = URLs::REGISTER;
	    }
	    else
		$location = URLs::REGISTER;
	    @header( "location: {$location}" );
	}
	catch ( Exception $ex )
	{
	    echo $ex->getMessage();
	}
    }

    private function get_form_data()
    {
	$corrupt = false;
	$data_arr = array();
	$data_arr[ "name" ] = $this->central->getargs( "full_name", $_POST, $corrupt );
	$data_arr[ "agreed" ] = $this->central->getargs( "agreed", $_POST, $corrupt );
	$data_arr[ "password" ] = $this->central->getargs( "password", $_POST, $corrupt );
	$data_arr[ "email" ] = $this->central->getargs( "email", $_POST, $corrupt );
	$data_arr[ "role" ] = Constants::USER_ROLE_ADMIN;

	if ( $corrupt )
	{
	    $_SESSION[ "error" ][ "full_name" ] = !strlen( $_POST[ 'full_name' ] );
	    $_SESSION[ "error" ][ "agreed" ] = !strlen( $_POST[ 'agreed' ] );
	    $_SESSION[ "error" ][ "password" ] = !strlen( $_POST[ 'password' ] );
	    $_SESSION[ "error" ][ "email" ] = !strlen( $_POST[ 'email' ] );
	    $data_arr = array();
	}
	else
	{
	    $bcrypt = new Config\Bcrypt();
	    $data_arr[ "password" ] = $bcrypt->hash( $data_arr[ "password" ] );
	}

	return $data_arr;
    }

    private function check_user_existance()
    {
	try
	{
	    $corrupt = false;
	    $user_email = $this->central->getargs( 'email', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		try
		{
		    $user = Plusql::from( $this->profile )->users->select( '*' )->where( "email = '{$user_email}' AND status <> '" . Constants::STATUS_DELETED . "'" )->run()->users;
		    $_SESSION[ "error" ][ "duplicate_email" ] = true;
		    return true;
		}
		catch ( EmptySetException $ex )
		{
		    return false;
		}
	    }
	}
	catch ( Exception $ex )
	{
	    return false;
	}
    }

}

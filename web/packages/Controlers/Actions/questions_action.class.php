<?php

@session_start();

use Config\Constants;
use Config\Central;

class QuestionsAction implements RocketSled\Runnable
{

    const ADD = 1;
    const UPDATE = 2;
    const REMOVE = 3;

    //--private members
    private $profile = "supplier";
    private $central;

    //--constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function run()
    {
	try
	{
	    $data = $this->update_main_contents();
	    die( json_encode( $data ) );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{

	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $method = $this->central->getargs( 'action', $_POST, $corrupt );
	    switch ( $method )
	    {
		case self::ADD:
		case self::UPDATE:
		    $return = $this->add_update_data();
		    break;
		case self::REMOVE:
		    $return = $this->remove_data();
		    break;
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    private function add_update_data()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $action = $this->central->getargs( 'action', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$data = array(
		    'title' => $this->central->getargs( 'title', $_POST, $corrupt ),
		    'type' => $this->central->getargs( 'type', $_POST, $corrupt ),
		    'user_id' => $this->central->get_logged_in_user_id( $this->profile ),
		    'survey_id' => 0,
		    'compulsory' => 1,
		    'deleted' => 0,
		    'updated_at' => date( "Y-m-d H:i:s" ),
		);

		switch ( $action )
		{
		    case self::ADD:
			$data[ 'created_at' ] = date( "Y-m-d H:i:s" );
			$where = "title = '{$data[ 'title' ]}' AND type = '{$data[ 'type' ]}'";
			if (
				!$this->central->check_existance( $this->profile, 'question', $where ) &&
				Plusql::into( $this->profile )->question( $data )->insert()
			)
			{
			    $question_id = $this->central->get_accurate_last_id( 'question', $this->profile );
			    if ( $question_id )
			    {
				$return = array(
				    'success' => 1,
				    'error' => 0,
				    'message' => 'Operation performed successfully.',
				    'question_id' => $question_id,
				    'options' => $this->insert_update_options( $data[ 'type' ], $question_id ),
				    'action' => self::UPDATE,
				);
			    }
			}
			else
			    $return[ 'message' ] = 'This data already exists';
			break;
		    case self::UPDATE:
			$corrupt = false;
			$question_id = $this->central->getargs( 'question_id', $_POST, $corrupt );
			if ( !$corrupt && Plusql::on( $this->profile )->question( $data )->where( 'question_id = ' . $question_id )->update() )
			    $return = array(
				'success' => 1,
				'error' => 0,
				'message' => 'Operation performed successfully.',
				'question_id' => $question_id,
				'options' => $this->insert_update_options( $data[ 'type' ], $question_id ),
				'action' => self::UPDATE,
			    );
			break;
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    private function insert_update_options( $type, $question_id )
    {
	try
	{
	    $return = array();
	    $options = $this->central->getargs( 'options', $_POST, $corrupt );
	    $answers = $this->central->getargs( 'answer', $_POST, $corrupt );

	    // detach all the removed options
	    $this->remove_existing_options( $question_id );

	    switch ( $type )
	    {
		case 'short':
		case 'long':
		    $return[ $question_id ][] = array(
			'id' => $this->add_update_record( $question_id, $options[ 0 ], $answers ),
			'label' => '',
		    );
		    break;
		case 'radio':
		    foreach ( $options as $option )
		    {
			$answer = strcmp( $option[ 'value' ], $answers ) === 0 ? 1 : 0;
			$return[ $question_id ][] = array(
			    'id' => $this->add_update_record( $question_id, $option, $answer ),
			    'label' => $option[ 'value' ],
			);
		    }
		    break;
		case 'checkbox':
		    $answers = explode( ',', $answers );
		    foreach ( $options as $option )
		    {

			$answer = in_array( $option[ 'value' ], $answers ) ? 1 : 0;
			$return[ $question_id ][] = array(
			    'id' => $this->add_update_record( $question_id, $option, $answer ),
			    'label' => $option[ 'value' ],
			);
		    }
		    break;
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $return;
    }

    private function add_update_record( $question_id, $option, $answer )
    {
	try
	{
	    $id = 0;
	    $data = array(
		'question_id' => $question_id,
		'label' => $option[ 'value' ],
		'value' => $option[ 'value' ],
		'answer' => $answer,
		'updated_at' => date( "Y-m-d H:i:s" ),
	    );
	    $where = "question_options_id = {$option[ 'id' ]}";
	    if (
		    !$this->central->check_existance( $this->profile, 'question_options', $where )
	    )
	    {
		if ( $option[ 'id' ] )
		    $data[ 'question_options_id' ] = $option[ 'id' ];

		$data[ 'created_at' ] = date( "Y-m-d H:i:s" );
		if ( Plusql::into( $this->profile )->question_options( $data )->insert() )
		    $id = $this->central->get_accurate_last_id( 'question_options', $this->profile );
	    }
	    else
	    {
		if ( Plusql::on( $this->profile )->question_options( $data )->where( "question_id = {$question_id} AND question_options_id = {$option[ 'id' ]}" )->update() )
		    $id = $option[ 'id' ];
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $id;
    }

    private function remove_existing_options( $question_id )
    {
	try
	{
	    Plusql::against( $this->profile )->run( "DELETE FROM question_options WHERE question_id = {$question_id}" );
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

    private function remove_data()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $action = $this->central->getargs( 'action', $_POST, $corrupt );
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		if ( Plusql::against( $this->profile )->run( "DELETE FROM question WHERE question_id = {$id}" ) )
		{
		    Plusql::against( $this->profile )->run( "DELETE FROM question_options WHERE question_id = {$id}" );
		    $return = array( 'success' => 1, 'error' => 0, 'message' => "" );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ "message" ] = $ex->getMessage();
	}

	return $return;
    }

}

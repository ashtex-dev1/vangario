<?php

use Config\Constants;
use Config\Central;

class PaymentDaemon implements \RocketSled\Runnable
{

    private $profile = "bv";

    public function __construct( $test_mode = 0 )
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
//	    $this->pheanstalk_client = new Pheanstalk_Pheanstalk( Constants::BEANSTALKD_HOST, Constants::BEANSTALKD_PORT );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function __call( $closure, $argv )
    {
	$escape = Plusql::escape( $this->profile );
	return $escape( $argv[ 0 ] );
    }

    public function run()
    {
	$this->amount_data();
    }

    public function amount_data()
    {
	try
	{
	    $this->central->set_alias_connection( $this->profile );
	    try
	    {
		$sql_cancel_number = "SELECT * FROM payment_response INNER JOIN user ON payment_response.user_id = user.user_id INNER JOIN user_details ON user.user_id = user_details.user_id WHERE payment_response.status <> 1 AND signup = 1 ORDER BY payment_response.payment_response_id ASC";
//		$sql_cancel_number = PluSQL::from( $this->profile )->payment_response->user->user_details->select( "*" )->where( "payment_response.status <> 1" );
		$data = Plusql::against( $this->profile )->run( $sql_cancel_number );
		while ( $row = $data->nextRow() )
		{
		    $mobile_number = $this->central->check_existance( $this->profile, "user_details", "user_id = {$row[ 'user_id' ]}" );
		    $rsa = new RSA();
		    $get_rsa = $rsa->transfer_rsa_amount( $survey_id = 0, $row[ 'user_id' ], $mobile_number->mobile_number );
		    if ( $get_rsa[ 'data' ]->code == 200 )
		    {
			$data = array(
			    'amount' => $get_rsa[ 'amount' ],
			    'amount_id' => $get_rsa[ 'data' ]->data->transactionId,
			    'reference_id' => $get_rsa[ 'data' ]->refId,
			    'invoice_id' => $get_rsa[ 'invoiceId' ],
			    'data_response' => json_encode( $get_rsa[ 'data' ] ),
			    'status' => 1,
			    'created_at' => date( "Y-m-d H:i:s" ),
			);
			Plusql::on( $this->profile )->payment_response( $data )->where( "payment_response_id = {$row[ 'payment_response_id' ]}" )->update();
		    }
		}
	    }
	    catch ( Exception $e )
	    {
//		echo $e->getMessage();
	    }
	}
	catch ( Exception $e )
	{
//	    echo $e->getMessage();
//	     continue;
	}
    }

}

<?php

use Config\Constants;
use Config\Central;
use Config\URLs;

class RSA
{
    public function transfer_rsa_amount( $survey_id, $invoice_id, $mobile_number )
    {
	$rsa = new Crypt_RSA(); // object initialization
	$rsa->setEncryptionMode( CRYPT_RSA_ENCRYPTION_PKCS1 );

	if ( $survey_id != 0 )
	{
	   $plaintext = 10000; // string to encrypt
           $rsa->loadKey( $this->get_public_key() );
           $ciphertext = $rsa->encrypt( $plaintext );
           $ciphertext = base64_encode( $ciphertext );
	   $invoice_id = $survey_id . ($invoice_id + 1);
	}
	else
	{
	    $plaintext = 5000; // string to encrypt
            $rsa->loadKey( $this->get_public_key() );
            $ciphertext = $rsa->encrypt( $plaintext );
            $ciphertext = base64_encode( $ciphertext );
	    $invoice_id = $invoice_id + 1;
	}
	$total_amount  = $plaintext/100 ;
	$json = 0;
	$pri_data = $this->rsa_curl_call( $ciphertext, $json, $mobile_number, $plaintext, $invoice_id );
	$data = array( 'data' => $pri_data, 'amount' => $total_amount, 'invoiceId' => $invoice_id );
//	echo PHP_EOL . "Api Result " . print_r( $data, 1 ) . PHP_EOL;
	return $data;
//	    $this->print_messages( "curl -H \"Content-Type: application/json\" -X POST -d '{\"customerId\":506530299,\"payeeMobileNumber\":923077374974,\"amount\":\"{$ciphertext}\",\"invoiceID\": 1001,\"text\":\"Dummy Payment\"}' http://devmerchant.finpay.pk/public/api/payOrPayAnyOne", "Request" );
    }

    private function get_public_key()
    {
	return "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJwNwGWI/DM9Oa1G6H0ORzuFEpgfcFZblHadNkJ+oVbJ8Ce4hybE4ijxFxvlVNl230aZZyrJvOWFa8Cy+JN2zFUCAwEAAQ==";
//	return "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMrZXXzhIizb5tGEsPD8ngLVBeEpcNWC3tYprb0PVdIJxrHVvrZJxWKiXXhXWTzZw6awB60rVUO7dGCEonQSoTMCAwEAAQ==";
    }

    private function print_messages( $message, $type )
    {
	echo PHP_EOL . "[ " . date( "M d H:i:s" ) . " ][ {$type} ] " . print_r( $message, 1 ) . PHP_EOL;
    }

    public function rsa_curl_call( $encoded_amount, $json, $mobile_number, $invoice_id )
    {
	try
	{
	    $required_data = array(
//		'customerId' => '506530299',
		'customerId' => '556213061',
		'payeeMobileNumber' => $mobile_number,
		'amount' => $encoded_amount,
		'invoiceID' => $invoice_id,
		'text' => 'Payment Has been made',
	    );
	    $data = json_encode( $required_data, $json );

	    $curlObj = curl_init();
//	    curl_setopt( $curlObj, CURLOPT_URL, 'http://devmerchant.finpay.pk/public/api/payOrPayAnyOne' );
	    curl_setopt( $curlObj, CURLOPT_URL, 'https://merchant.finpay.pk/public/api/payOrPayAnyOne' );
	    curl_setopt( $curlObj, CURLOPT_RETURNTRANSFER, 1 );
	    curl_setopt( $curlObj, CURLOPT_SSL_VERIFYPEER, 0 );
	    curl_setopt( $curlObj, CURLOPT_HEADER, 0 );
	    curl_setopt( $curlObj, CURLOPT_HTTPHEADER, array( 'Content-type:application/json' ) );
	    curl_setopt( $curlObj, CURLOPT_POST, 1 );
	    curl_setopt( $curlObj, CURLOPT_POSTFIELDS, $data );
	    $response = curl_exec( $curlObj );

	    $json = json_decode( $response );
	    curl_close( $curlObj );

	    return $json;
	}
	catch ( Exception $ex )
	{
	    return $ex->getMessage();
	}
    }

}

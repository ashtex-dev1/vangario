<?php

require_once(__DIR__ . '/../Helpers/facebook.php');

use Config\Constants;
use Config\Central;

class PagesDaemon implements \RocketSled\Runnable
{

    private $profile = "bv";

    public function __construct( $test_mode = 0 )
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	    $this->pheanstalk_client = new Pheanstalk_Pheanstalk( Constants::BEANSTALKD_HOST, Constants::BEANSTALKD_PORT );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function __call( $closure, $argv )
    {
	$escape = Plusql::escape( $this->profile );
	return $escape( $argv[ 0 ] );
    }

    public function run()
    {
	while ( 1 )
	{
	    while ( $job = $this->pheanstalk_client->watch( Constants::BEANSTALKD_LIKES_TUBE )->reserve() )
	    {
		//make sure we never spam anyone
		try
		{
		    echo ( PHP_EOL . " - - - - - Job Started " . date( "Y-m-d H:i:s" ) . " - - - - " );
		    $this->pheanstalk_client->bury( $job );
		    try
		    {
			// extract email data from the job
			$data = $job->getData();
			if ( !$data )
			{
			    return;
			}
			$_POST = unserialize( $data );
			@file_put_contents( "{$_POST[ 'user_id' ]}_likes_data", print_r( $_POST, 1 ) );
			$this->add_update_categories();
			Api::update_user_interests( $this->profile, $_POST[ 'user_id' ] );
		    }
		    catch ( Exception $e )
		    {
			print 'Error sending email: ' . $e->getMessage() . PHP_EOL;
		    }
		    $this->pheanstalk_client->delete( $job );
		    echo ( PHP_EOL . " - - - - - Job Completed @ " . date( "Y-m-d H:i:s" ) . " - - - - " );
		}
		catch ( Exception $e )
		{
		    echo $e->getMessage();
		    // continue;
		}
	    }

	    $sleep = rand( 1, 10 );
	    echo ( PHP_EOL . " - - - - - Sleep for : {$sleep} - - - - " );
	    sleep( $sleep );
	}
    }

    private function add_update_categories()
    {
	try
	{
	    $pages = $this->filter_fetched_pages( prepare_pages_list() );
	    $page_details = get_facebook_page_details( $pages );
	    foreach ( $page_details as $page )
	    {
		$categories = array();
		$categories[] = array(
		    'title' => $page[ 'category' ],
		    'fb_id' => $page[ 'id' ],
		    'page' => $this->esc( $page[ 'title' ] ),
		    'page_id' => $page[ 'page_id' ],
		);

		if ( isset( $page[ 'category_list' ] ) )
		{
		    foreach ( $page[ 'category_list' ] as $category )
		    {
			$categories[] = array(
			    'title' => $this->esc( $category->name ),
			    'fb_id' => $category->id,
			    'page' => $this->esc( $page[ 'title' ] ),
			    'page_id' => $page[ 'page_id' ],
			);
		    }
		}

		foreach ( $categories as $category )
		{
		    $survey_categories = $this->central->check_existance( $this->profile, "survey_categories", "title = '" . $category[ 'title' ] . "'" );
		    if ( !$survey_categories )
		    {
			Plusql::into( $this->profile )->survey_categories( $category )->insert();
			$survey_categories_id = $this->central->get_accurate_last_id( "survey_categories", $this->profile );
		    }
		    else
		    {
			$survey_categories_id = $survey_categories->survey_categories_id;
			Plusql::on( $this->profile )->survey_categories( $category )->where( "survey_categories_id = {$survey_categories_id}" )->update();
		    }

		    $fb_page = $this->central->check_existance( $this->profile, "fb_pages", "fb_id = '" . $category[ 'page_id' ] . "'" );
		    if ( !$fb_page )
		    {
			Plusql::into( $this->profile )->fb_pages( array(
			    'title' => $this->esc( $category[ 'page' ] ),
			    'fb_id' => $category[ 'page_id' ],
			    'survey_categories_id' => $survey_categories_id,
			) )->insert();
		    }
		    else
		    {
			Plusql::on( $this->profile )->fb_pages( array(
			    'title' => $this->esc( $category[ 'page' ] ),
			    'survey_categories_id' => $survey_categories_id,
			) )->where( "fb_id = '{$category[ 'page_id' ]}'" )->update();
		    }
		}
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

    private function filter_fetched_pages( $pages )
    {
	try
	{
	    $return = "";
	    foreach ( $pages as $page )
	    {
		//echo ( PHP_EOL . " - - - - - page : {$page} - - - - " );
		$fb_page = $this->central->check_existance( $this->profile, "fb_pages", "fb_id = '{$page[ 'id' ]}'" );
		if ( !$fb_page )
		{
		    $return[] = $page;
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return = $pages;
	}

	return $return;
    }

}

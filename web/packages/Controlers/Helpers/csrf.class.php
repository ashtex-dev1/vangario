<?php

use Config\Constants;
use Config\Central;
use Config\URLs;

class Csrf
{

    public function generate_token( $key )
    {
	@session_start();
	$key = $result = preg_replace( '/[^a-zA-Z0-9]+/', '', $key );

	$extra = sha1( $_SERVER[ 'REMOTE_ADDR' ] . $_SERVER[ 'HTTP_USER_AGENT' ] );
	// time() is used for token expiration
	$token = base64_encode( time() . $extra . $this->randomString( 32 ) );

	return $token;
    }

    public function generate_session_token( $key, $token )
    {
	$this->set_token( $key, $token );

	return $token;
    }

    public function check_token( $key, $token, $timespan = null, $multiple = false )
    {
	$key = $result = preg_replace( '/[^a-zA-Z0-9]+/', '', $key, -1, $count = null );

	if ( !$token )
	{
	    throw new \Exception( 'Missing CSRF form token.' );
	}
	$session_token = $this->get_token( $key );

	if ( !$session_token )
	{
	    throw new \Exception( 'Missing CSRF session token.' );
	}

//	if ( !$multiple )
//	{
//	    $this->set_token( $key, null );
//	}

	if ( sha1( $_SERVER[ 'REMOTE_ADDR' ] . $_SERVER[ 'HTTP_USER_AGENT' ] ) != substr( base64_decode( $session_token ), 10, 40 ) )
	{
	    throw new \Exception( 'Form origin does not match token origin.' );
	}
	if ( $token != $session_token )
	{
	    throw new \Exception( 'Invalid CSRF token.' );
	}

	// Check for token expiration
	if ( $timespan != null && is_int( $timespan ) && intval( substr( base64_decode( $session_token ), 0, 10 ) ) + $timespan < time() )
	{
	    throw new \Exception( 'CSRF token has expired.' );
	}
    }

    public function get_token( $key )
    {
	if ( isset( $_SESSION[ $key ] ) )
	{
	    return $_SESSION[ $key ];
	}
	else
	{
	    return null;
	}
    }

    public function set_token( $key, $value )
    {
	$_SESSION[ $key ] = $value;
    }

    public function randomString( $length )
    {
	$seed = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijqlmnopqrtsuvwxyz0123456789';
	$max = strlen( $seed ) - 1;
	$string = '';
	for ( $i = 0; $i < $length; ++$i )
	{
	    $string .= $seed{intval( mt_rand( 0.0, $max ) )};
	}

	return $string;
    }

    public function generateNewToken( $token, $rendom_key )
    {
	try
	{
	    if ( $_SESSION[ 'csrf_session' ] == $token )
	    {
		$csrf_token = $this->generate_session_token( $rendom_key, $token );
		$check_token = $this->check_token( $rendom_key, $csrf_token );
		if ( !$check_token && !$_SESSION[ $rendom_key ] )
		{
		    $_SESSION[ 'error' ][ 'something' ] = 1;
		}
		else
		{
		    return $csrf_token;
		}
	    }
	    else
	    {
		$_SESSION[ 'error' ][ 'something' ] = 1;
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
    }

}

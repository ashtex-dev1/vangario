<?php

use Config\Central;
use Config\Constants;
use Config\URLs;

class Filledsurvey extends Config\RSBase
{

    private $file_name = "pages/app/filled_survey.html";

    public function __construct()
    {
	try
	{
	    parent::__construct();
	    $this->template = $this->central->load_normal( $this->file_name );
	}
	catch ( Exception $e )
	{
	    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $user_id = $_SESSION[ 'user' ][ 'user_id' ];
	    if ( $user_id )
	    {
		$get_filled_surveys = new Api();
		$get_filled_survey = $get_filled_surveys->get_filled_surveys( $user_id );

		if ( $get_filled_survey[ 'success' ] )
		{
//		    $count = 1;
		    $total_amount = 1;
		    $amount = 30;
		    $repeat = $this->template->repeat( '.filled-survey-li' );
		    foreach ( $get_filled_survey[ 'surveys' ] as $survey )
		    {
			$this->template->setValue( '.survey-amount', ($amount * $total_amount++ ) );
//			$repeat->setValue( '.survey-count', $count++ . "-" );
			$repeat->setValue( '.survey-href@href', URLs::FILLEDSURVEYDETAILS . $this->central->encode_url_param( ( str_replace( "%ID%", $survey[ 'sid' ], URLs::ID ) ) ) );
			$repeat->setValue( '.survey-title', $survey[ 'stitle' ] );
			$repeat->setValue( '.survey-date', date( 'j M, Y H:i A', strtotime( $survey[ 'published' ] ) ) );

			$repeat->next();
		    }
		    Central::remove_last_repeating_element( $this->template, '.stop', 1, 2, 0 );
		}
		else
		{
		    $this->template->remove( ".survey-amount" );
		    $this->template->remove( "#bottom-wizard" );
		    $this->template->setValue( '.filled-survey-ul', '<li> No Record Found... </li>', 1 );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $this->template->setValue( '.filled-survey-li', '<li> No Record Found... </li>', 1 );
	}
    }

}

?>
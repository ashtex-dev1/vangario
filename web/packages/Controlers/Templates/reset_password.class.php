<?php

@session_start();

use \Config\Constants;
use \Config\Central;
use Config\URLs;

class ResetPassword implements RocketSled\Runnable
{

    //--private members
    private $file_name = "pages/user/reset_password.html";
    private $profile = 'change_pass';
    private $template;
    private $central;

    //--constructor
    public function __construct()
    {
	try
	{
	    if ( isset( $_SESSION ) && isset( $_SESSION[ "user" ] ) )
	    {
		unset( $_SESSION[ "user" ] );
		unset( $_SESSION );
		session_destroy();
	    }
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	    $this->template = $this->central->load_normal( $this->file_name );
	    $this->register_args();
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    //register args
    private function register_args()
    {
	
    }

    public function run()
    {
	try
	{
	    if ( !$this->central->check_user_login_status( $this->profile ) )
		$this->render();
	    else
	    {
		if ( $_SESSION[ "user" ][ "role" ] === "admin" )
		    @header( 'location: /' );
		/*
		 * newly modified
		 */
	    }
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    private function render( $display = TRUE )
    {
	try
	{
	    $this->update_main_contents();
	    if ( $display )
		$this->central->render( $this->template );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $this->template->setValue( "#login", "&larr; Back to Sign-in", 1 );
	    $this->template->setValue( "#login@href", URLs::LOGIN );
	    $this->reset_password();
	    $this->check_for_valid_salt();

	    if ( isset( $_SESSION[ 'cpass' ] ) && $_SESSION[ 'cpass' ] )
		$this->template->setValue( ".confirm_password_error@style", "display: block;" );

	    if ( isset( $_SESSION[ 'pass' ] ) && $_SESSION[ 'pass' ] )
		$this->template->setValue( ".password_error@style", "display: block;" );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function reset_password()
    {
	try
	{
	    $corrupt = false;
	    $temp_link = $this->central->getargs( 'temp_link', $_GET, $corrupt );
	    $password = $this->central->getargs( 'password', $_POST, $corrupt );
	    $confirm_password = $this->central->getargs( 'confirm_password', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		if ( strcmp( $password, $confirm_password ) == 0 )
		{
		    try
		    {
			$user = Plusql::from( $this->profile )->user->select( "*" )->where( "temp_link = '{$temp_link}'" )->limit( '0,1' )->run()->user;
			try
			{
			    $bcrypt = new \Config\Bcrypt();
			    $reset_pwd_data = array(
				"password" => $bcrypt->hash( $password ),
				"temp_link" => ''
			    );
			    Plusql::on( $this->profile )->user( $reset_pwd_data )->where( "user_id=" . $user->user_id )->update();
			    unset( $_SESSION[ 'pass' ] );
			    unset( $_SESSION[ 'cpass' ] );
			    unset( $_SESSION );
			    @header( 'location: /' );
			}
			catch ( EmptySetException $ese )
			{
			    $_SESSION[ 'pass' ] = 1;
			}
		    }
		    catch ( Exception $ex )
		    {
			$_SESSION[ 'pass' ] = 1;
		    }
		}
		else
		{
		    $_SESSION[ 'cpass' ] = 1;
		}
	    }
	}
	catch ( Exception $e )
	{
	    $_SESSION[ 'pass' ] = 1;
	}
    }

    private function check_for_valid_salt()
    {
	try
	{
	    $corrupt = false;
	    $temp_link = $this->central->getargs( 'temp_link', $_GET, $corrupt );
	    if ( !$corrupt )
	    {
		$user = Plusql::from( $this->profile )->user->select( "*" )->where( "temp_link = '{$temp_link}'" )->limit( '0,1' )->run()->user;
	    }
	    else
		@header( "location: /" );
	}
	catch ( EmptySetException $ex )
	{
	    @header( "location: /" );
	}
	catch ( Exception $ex )
	{
	    @header( "location: /" );
	}
    }

}

?>
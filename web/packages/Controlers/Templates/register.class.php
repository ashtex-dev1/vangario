<?php

use Config\Central;
use Config\Constants;
use Config\URLs;

class Register implements RocketSled\Runnable
{

    private $file_name = "pages/user/register.html";
    private $profile = "register";
    private $central = "";

    public function __construct()
    {
	@session_start();
	// Load DB credentials and supported classes
	$this->central = Central::instance();
	$this->central->set_alias_connection( $this->profile );
	$this->template = $this->central->load_normal( $this->file_name );
    }

    public function run()
    {
	try
	{
	    if ( !$this->central->check_user_login_status( $this->profile ) )
		$this->render();
	    else
		@header( 'location: ' . URLs::DASHBOARD );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    private function render( $display = TRUE )
    {
	try
	{
	    $this->update_main_contents();
	    if ( $display )
		$this->central->render( $this->template );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $this->template->setValue( "#register_form@action", URLs::REGISTER . URLs::ACTION );
	    $this->template->setValue( "#login-page@href", URLs::LOGIN );
	    if ( isset( $_SESSION ) && isset( $_SESSION[ "error" ] ) )
	    {
		if ( $_SESSION[ "error" ][ "full_name" ] )
		    $this->template->setValue( ".full_name", "display: block;" );
		if ( $_SESSION[ "error" ][ "password" ] )
		    $this->template->setValue( ".password", "display: block;" );
		if ( $_SESSION[ "error" ][ "email" ] )
		    $this->template->setValue( ".email", "display: block;" );
		if ( $_SESSION[ "error" ][ "duplicate_email" ] )
		{
		    $this->template->setValue( ".email_error@style", "display: block;" );
		    $this->template->setValue( ".email_error", '<i class="fa fa-warning"> </i> This email already exists please try with a different email address.', 1 );
		}

		unset( $_SESSION[ "error" ] );
		session_destroy();
	    }
	}
	catch ( Exception $ex )
	{
	    // echo $ex->getMessage();
	}
    }

}

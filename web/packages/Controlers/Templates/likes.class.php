<?php

use Config\Central;
use Config\Constants;
use Config\URLs;

class Likes extends Config\RSBase
{

    private $file_name = "pages/app/likes.html";

    public function __construct()
    {
	try
	{
	    parent::__construct();
	    $this->template = $this->central->load_normal( $this->file_name );
	}
	catch ( Exception $e )
	{
	    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $this->get_user_likes();
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    private function get_user_likes()
    {
	try
	{
	    $user_id = $_SESSION[ 'user' ][ 'user_id' ];
	    if ( $user_id )
	    {

		$likes = Plusql::from( $this->profile )->user_interests->fb_pages->select( "*" )->where( "user_interests.user_id = {$user_id}" )->run()->user_interests;
		$repeat = $this->template->repeat( '.likes_li' );
		foreach ( $likes as $like )
		{
		    $repeat->setValue( '.likes-pages', $this->central->string_formating( $like->title ) );
		    $repeat->setValue( '#likes-pages@title', $this->central->string_formating( $like->title ) );
		    $repeat->setValue( '.likes-page-id@href', "https://www.facebook.com/" . $like->fb_pages->fb_id );
		    $repeat->next();
		}
		Central::remove_last_repeating_element( $this->template, '.stop', 1, 2, 0 );
	    }
	}
	catch ( Exception $ex )
	{
	    $this->template->setValue( '.likes_ul', '<li> No Record Found... </li>', 1 );
	}
	try
	{
	    $user_id = $_SESSION[ 'user' ][ 'user_id' ];
	    if ( $user_id )
	    {

		$likes = Plusql::from( $this->profile )->user_interests->fb_pages->select( "*" )->where( "user_interests.user_id = {$user_id}" )->run()->user_interests;
		$repeat = $this->template->repeat( '.search_li' );
		foreach ( $likes as $like )
		{
		    $repeat->setValue( '.search_title', $this->central->string_formating( $like->title ) );
		    $repeat->setValue( '#likes-pages@title', $this->central->string_formating( $like->title ) );
		    $repeat->setValue( '.search_href@href', "https://www.facebook.com/" . $like->fb_pages->fb_id );
		    $repeat->next();
		}
		Central::remove_last_repeating_element( $this->template, '.search_stop', 1, 2, 0 );
	    }
	}
	catch ( Exception $ex )
	{
	    $this->template->setValue( '.search_li', '<li class="no-record"> No Record Found... </li>', 1 );
	}
    }

}

?>
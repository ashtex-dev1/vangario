<?php

@session_start();

use \Config\Constants;
use \Config\Central;
use Config\URLs;

class ForgetPassword implements RocketSled\Runnable
{

    //--private members
    private $file_name = "pages/user/reset_email.html";
    private $profile = 'forgot_password';
    private $template;
    private $central;

    //--constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	    $this->template = $this->central->load_normal( $this->file_name );
	    $this->register_args();
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    //register args
    private function register_args()
    {
	
    }

    public function run()
    {
	try
	{
	    if ( !$this->central->check_user_login_status( $this->profile ) )
	    {
		$this->render();
	    }
	    else
	    {
		if ( $_SESSION[ "user" ][ "role" ] === "admin" )
		    @header( 'location: /' );
	    }
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    private function render( $display = TRUE )
    {
	try
	{
	    $this->update_main_contents();
	    if ( $display )
		$this->central->render( $this->template );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $this->template->setValue( "#login", "&larr; Back to Sign-in", 1 );
	    $this->template->setValue( "#login@href", URLs::LOGIN );
	    $this->template->setValue( "#reset_pass_form@action", URLs::FORGET_PASSWORD );
	    if ( isset( $_SESSION[ 'email_success' ] ) && $_SESSION[ 'email_success' ] )
	    {
		$this->template->setValue( ".email_seccuss@style", "display: block;" );
		unset( $_SESSION[ 'email_success' ] );
	    }
	    else if ( isset( $_SESSION[ 'email_error' ] ) && $_SESSION[ 'email_error' ] )
	    {
		$this->template->setValue( ".email_id_error@style", "display: block;" );
		unset( $_SESSION[ 'email_error' ] );
	    }
	    $this->forgot_password_action();
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function forgot_password_action()
    {
	try
	{
	    $corrupt = false;
	    $email = $this->central->getargs( 'email', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$user = Plusql::from( $this->profile )->user->select( "*" )->where( "email = '{$email}' AND status <> '" . Constants::STATUS_DELETED . "'" )->limit( '0,1' )->run()->user;
		try
		{
		    $temporary_link = md5( $user->user_id . rand( 00000, 999999 ) );
		    $reset_pwd_data = array(
			"email" => $_POST[ "email" ],
			"temp_link" => $temporary_link
		    );
		    Plusql::on( $this->profile )->user( $reset_pwd_data )->where( "email='" . $user->email . "'" )->update();
		}
		catch ( EmptySetException $ese )
		{
		    @header( "location: " . URLs::PAGE_NOT_FOUND );
		}
		$email_body = str_replace( "%USER_EMAIL%", $user->email, Constants::FORGET_EMAIL_BODY );
		$email_body = str_replace( "%RESET_PASSWORD_LINK%", $this->central->get_server_url() . URLs::RESET_PASSWORD . $this->central->encode_url_param( "temp_link-{$temporary_link}" ), $email_body );
		$email_data = array( 'email' => $user->email,
		    'subject' => 'Password Reset',
		    'to' => $email,
		    'text_body' => str_replace( '<br/>', '\n', $email_body ),
		    'html_body' => str_replace( '\n', '<br />', $email_body )
		);
		if ( $this->central->send_email( $email_data ) )
		{
		    unset( $_SESSION[ 'email_error' ] );
		    $_SESSION[ 'email_success' ] = 1;
		    @header( "location: " . URLs::FORGET_PASSWORD );
		}
		else
		    @header( "location: " . URLs::LOGIN );
	    }
	}
	catch ( EmptySetException $e )
	{
	    $_SESSION[ 'email_error' ] = 1;
	}
	catch ( Exception $e )
	{
	    $_SESSION[ 'email_error' ] = 1;
	}
    }

}

?>

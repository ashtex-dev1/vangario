<?php

use \Config\Constants;
use \Config\Central;
use Config\URLs;

class Questions extends Config\RSBase
{

    //--private members
    private $file_name = "pages/admin/view_questionnaire.html";
    private $question_types = array(
	'short' => 'Short Answer',
	'long' => 'Paragraph',
	'checkbox' => 'Checkboxes',
	'radio' => 'Radios',
    );

    //--constructor
    public function __construct()
    {
	try
	{
	    parent::__construct();
	    $this->template = $this->central->load_normal( $this->file_name );
//	    $this->central->populate_user_contents( $this->template );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $this->central->repeat_select_options( $this->template, '#question-type', $this->question_types );
	    $this->template->setValue( '#question_form@action', URLs::QUESTIONS . URLs::ACTION );
	    $this->template->setValue( '#add-btn@data-method', QuestionsAction::ADD );
	    $this->populate_questions();
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    private function populate_questions()
    {
	try
	{
	    $questions = PluSQL::from( $this->profile )->question->question_options->select( "*" )->where( "question.deleted <> 1 AND question_options.deleted <> 1 AND user_id = {$this->central->get_logged_in_user_id( $this->profile )} AND compulsory = 1" )->run()->question;
	    $item = $this->template->repeat( '.questions' );
	    foreach ( $questions as $question )
	    {
		$item->setValue( "#title", $question->title );
		$item->setValue( ".{$question->type}-section@style", "" );
		if ( in_array( $question->type, array( "radio", "checkbox" ) ) )
		{
		    $sub_item = $item->repeat( ".{$question->type}-section/.option" );
		    foreach ( $question->question_options as $option )
		    {
			$sub_item->setValue( "#label", $option->label );
			if ( strcmp( $question->type, "radio" ) === 0 )
			    $sub_item->query( 'i' )->item( 0 )->setAttribute( "class", $option->answer ? "ion-android-radio-button-on" : "ion-android-radio-button-off"  );
			else
			    $sub_item->query( 'i' )->item( 0 )->setAttribute( "class", $option->answer ? "ion-android-checkbox-outline" : "ion-android-checkbox-outline-blank"  );
			$sub_item->next();
		    }
		    Central::remove_last_repeating_element( $sub_item, '.', 1, 0, 0 );
		}
		$item->setValue( "#edit@data-raw-data", $this->get_raw_data( $question ) );
		$item->setValue( "#edit@data-action", QuestionsAction::UPDATE );
		$item->setValue( "#remove@data-action", URLs::QUESTIONS . URLs::ACTION );
		$item->setValue( "#remove@data-method", QuestionsAction::REMOVE );
		$item->setValue( "#remove@data-id", $question->question_id );
		$item->next();
	    }
	    Central::remove_last_repeating_element( $item, '.', 1, 0, 0 );
	}
	catch ( Exception $ex )
	{
	    $this->template->setValue( '.questions', '<h4> No Record Found... </h4>', 1 );
	}
    }

    private function get_raw_data( $question )
    {
	try
	{
	    $raw_data = array();
	    $raw_data[ 'question' ][ 'id' ] = $question->question_id;
	    $raw_data[ 'question' ][ 'title' ] = $question->title;
	    $raw_data[ 'question' ][ 'type' ] = $question->type;
	    foreach ( $question->question_options as $option )
	    {
		$raw_data[ 'options' ][] = array(
		    'id' => $option->question_options_id,
		    'label' => $option->label,
		    'answer' => $option->answer,
		);
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return json_encode( $raw_data );
    }

    private function messages()
    {
	if ( isset( $_SESSION[ 'admin' ][ 'questionnaire' ][ 'deleted' ] ) )
	{
	    $this->template->setValue( '.comdelscs@style', 'display:block' );
	    $this->template->setValue( '#comindelmsg', 'Questionnaire has been Deleted Successfully.' );
	    unset( $_SESSION[ 'admin' ][ 'questionnaire' ][ 'deleted' ] );
	}
	if ( isset( $_SESSION[ 'admin_company' ][ 'updated' ] ) )
	{
	    $this->template->setValue( '.cominscs@style', 'display:block' );
	    $this->template->setValue( '#cominsuccmsg', 'Company has been Updated Successfully.' );
	    unset( $_SESSION[ 'admin_company' ][ 'updated' ] );
	}
    }

}

?>
<?php

use \Config\Constants;
use \Config\Central;
use Config\URLs;

class Home implements RocketSled\Runnable
{

    //--private members
    private $file_name = "pages/home/home.html";

    //--constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->template = $this->central->load_normal( $this->file_name );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function run()
    {
	try
	{
//	    $this->check_logout_request();
//	    if ( !$this->central->check_user_login_status( $this->profile ) )
	    $this->render();
//	    else
//		@header( 'location: ' . URLs::HOME );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    private function render( $display = TRUE )
    {
	try
	{
	    $this->update_main_contents();
	    if ( $display )
		$this->central->render( $this->template );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{
//	    print_r($_SERVER['HTTP_HOST']);
//	    die();
	    $this->template->setValue( ".home-login@href", URLs::LOGIN );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

}

?>
<?php

use Config\Central;
use Config\Constants;
use Config\URLs;

class Questionnaire extends Config\RSBase
{

    private $file_name = "pages/app/questionare.html";
    private $question_types = array(
	'short' => 'Short Answer',
	'long' => 'Paragraph',
	'checkbox' => 'Checkboxes',
	'radio' => 'Radios',
    );

    public function __construct()
    {
	try
	{
	    parent::__construct();
	    $this->template = $this->central->load_normal( $this->file_name );
	}
	catch ( Exception $e )
	{
	    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
	}
    }

    public function update_main_contents()
    {
	try
	{
//	    $this->template->setValue('#questionnaire-popup',"<script>$(document).ready(function(){swal('LPC', 'BC', 'error');});</script>", 1);
	    $user_id = $_SESSION[ 'user' ][ 'user_id' ];
	    $corrupt = false;
	    $survey_id = $this->central->getargs( "id", $_GET, $corrupt );
	    if ( !$corrupt )
	    {
		try
		{
		    $survey = PluSQL::from( $this->profile )->survey->question->question_options->select( "*, survey.title AS survey_title" )->where( "survey.survey_id = {$survey_id} AND survey.deleted <> 1 AND question.deleted <> 1 AND question_options.deleted <> 1" )->orderBy( 'question.question_id ASC' )->run()->survey;
		    $this->template->setValue( "#survey_id@value", $survey->survey->survey_id );
		    $this->template->setValue( "#user_id@value", $user_id );
		    $this->template->setValue( ".survey-title", $this->central->string_formating( $survey->survey_title ) );
		    $count = 0;
		    $item = $this->template->repeat( '.survey-question' );
		    foreach ( $survey->question as $question )
		    {
			$item->setValue( ".question_id@value", $question->question_id );
			$item->setValue( ".question-title", $this->central->string_formating( $question->title ) );
			$item->query( ".{$question->type}-section" )->item( 0 )->setAttribute( 'data-num', $count + 1 );
			$item->query( ".{$question->type}-section" )->item( 0 )->setAttribute( 'data-id', $question->question_id );
			if ( $count == 0 )
			{
			    $item->setValue( ".{$question->type}-section@style", "display:block" );
			}
			else
			{
			    $item->setValue( ".{$question->type}-section@style", "display:none" );
			}
//			if ( $question->type == 'short' )
//			{
//			    $this->template->addClass( ".{$question->type}", "activeImg" );
//			}

			if ( in_array( $question->type, array( "short", "long", "radio", "checkbox", "single-image", "multi-image", "rating-image" ) ) )
			{
			    $sub_item = $item->repeat( ".{$question->type}-section/.multiple" );
			    foreach ( $question->question_options as $option )
			    {
				if ( $question->type == "single-image" || $question->type == "multi-image" || $question->type == "rating-image" )
				{
				    try
				    {
					$image_urls = PluSQL::from( $this->profile )->survey_option_image->select( "*" )->where( "survey_option_image.deleted <> 1 AND question_options_id={$option->question_options_id}" )->limit( "0, 1" )->run()->survey_option_image;
					foreach ( $image_urls as $image_url )
					{
					    $sub_item->setValue( "#picture@src", $image_url->image_url );
					}
				    }
				    catch ( Exception $ex )
				    {
					// do nothing
				    }
				    $sub_item->setValue( "#image-title-value@value", $this->central->string_formating( $option->label ) );
				    $sub_item->setValue( ".image-title-name", $this->central->string_formating( $option->label ) );
				}
				$sub_item->setValue( "#{$question->type}@data-id", $option->question_id );
				$sub_item->setValue( ".image-options@data-id", $option->question_options_id );
				$sub_item->setValue( ".image-options@name", $question->type . "_" . $option->question_id );
				$sub_item->setValue( "#{$question->type}@name", $question->type . "_" . $option->question_id );
				$sub_item->setValue( "#{$question->type}@value", $this->central->string_formating( $option->value ) );
				$sub_item->setValue( ".{$question->type}-option-title", $this->central->string_formating( $option->value ) );
				$sub_item->next();
			    }
			    Central::remove_last_repeating_element( $sub_item, '.', 1, 0, 0 );
			}
			$item->next();
			$count++;
		    }
		    Central::remove_last_repeating_element( $item, '.', 1, 0, 0 );
//		    $this->template->setValue( ".question-form@action", URLs::APPQUESTIONNAIRE . URLs::ACTION );
		}
		catch ( EmptySetException $ex )
		{
		    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

}

?>
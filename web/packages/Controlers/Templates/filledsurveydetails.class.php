<?php

use Config\Central;
use Config\Constants;
use Config\URLs;

class Filledsurveydetails extends Config\RSBase
{

    private $file_name = "pages/app/filled_survey_details.html";

    public function __construct()
    {
	try
	{
	    parent::__construct();
	    $this->template = $this->central->load_normal( $this->file_name );
	}
	catch ( Exception $e )
	{
	    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $user_id = $_SESSION[ 'user' ][ 'user_id' ];
	    if ( $user_id )
	    {
		$filled_survey_data = Plusql::from( $this->profile )->user->survey->survey_response->select( "*" )->where( "survey_response.survey_id = {$_GET[ 'id' ]} AND survey_response.status = 1 AND survey_response.user_id={$user_id} " )->run()->user;
		$this->template->setValue( '.survey-title', $filled_survey_data->survey->title );
		$this->template->setValue( '.survey-brand', $filled_survey_data->user->user_name );
		$this->template->setValue( '.survey-date', date( 'j M, Y H:i A', strtotime( $filled_survey_data->survey->published_at ) ) );
	    }
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

}

?>
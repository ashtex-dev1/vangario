$( document ).ready( function ( ) {

    $( document ).on( 'click', '#preview', function ( ) {
	try {
	    $( '.overlay' ).fadeIn( 200 );
	    var response = send_ajax( {
		action: $( this ).attr( 'data-method' ),
		id: $( this ).attr( 'data-id' ),
	    }, $( this ).attr( 'data-action' ) );
	    setTimeout( function ( ) {
		$( '.overlay' ).fadeOut( 200 );
		$( '.survey-preview-model .modal-body' ).html( response.content );
		$( '.survey-preview-model' ).modal( 'show' );
	    }, 200 );
	} catch ( e ) {
	    $( '.overlay' ).fadeOut( 200 );
	}
    } );

    $( "form#publish_survey_form .save" ).click( function ( ) {
	publish_survey();
    } );

    function publish_survey( ) {
	var all_ok = true;
	var par = $( "form#publish_survey_form" );
	if ( $( '.zero-audience' ).is( ':checked' ) ) {
	    all_ok;
	} else {
	    publish_validation( );
	    all_ok = validate_form( par, all_ok );
	}

	if ( all_ok ) {
	    var data = par.serializeObject();
	    $( 'button[type="submit"], button.save', par ).btnLoading( 'loading', 'Loading...' );
	    setTimeout( function () {
		var response = send_ajax( data, par.attr( 'action' ) );
		add_notification( response );
		if ( response.success ) {
		    window.location.reload();
		}
	    }, 1500 );
	} else {
	    /*
	     add_notification( {
	     message: "Something went wrong."
	     } );
	     */
	}

	return false;
    }

    function publish_validation( ) {
	var all = false;
	var undefinde = "undefined";
	var demographics = $( "#demographics" ).val( );
	var location = $( "#location" ).val( );
	var education = $( "#education" ).val( );
	var age = $( "#age" ).val( );
	var relationship = $( "#relationship" ).val( );
	if (
		typeof demographics == undefinde || demographics == null

		) {
	    $( "#demographics" ).find( 'option' ).prop( "selected", true );
	    all = true;
	}
	if (
		typeof location == undefinde || location == null

		) {
	    $( "#location" ).find( 'option' ).prop( "selected", true );
	    all = true;
	}
	if (
		typeof education == undefinde || education == null

		) {
	    $( "#education" ).find( 'option' ).prop( "selected", true );
	    all = true;
	}
	if (
		typeof age == undefinde || age == null

		) {
	    $( "#age" ).find( 'option' ).prop( "selected", true );
	    all = true;
	}
	if (
		typeof relationship == undefinde || relationship == null

		) {
	    $( "#relationship" ).find( 'option' ).prop( "selected", true );
	    all = true;
	}
	return all;
    }

    $( document ).on( "click", "#un-publish", function ( ) {
	var action = $( this ).data( 'action' );
	var id = $( this ).data( 'id' );
	var method = $( this ).data( 'method' );
	swal( {
	    title: "Are you sure?",
	    text: "You will not be able to revert this operation!",
	    type: "warning",
	    showCancelButton: true,
	    confirmButtonColor: "#DD6B55",
	    confirmButtonText: "Yes, unpublish it!"
	},
		function ( ) {
		    $.post( action, {
			id: id,
			action: method,
		    }, function ( data ) {
			data = JSON.parse( data );
			if ( data.success )
			    window.location.reload( );
			else
			    add_notification( data );
		    } );
		} );
    } );

    $( document ).delegate( ".input-group-addon i.fa-copy", "click", function ( ) {
	copy_to_clipboard( $( ".copy-link" ) );
	add_notification( {
	    info: true,
	    message: 'URL Coppied...'
	} );
    } );

    $( document ).delegate( "#short-link", "click", function ( ) {
	$( ".survey-short-link" ).modal( 'show' );
	$( ".generate-short-link" ).btnLoading( 'loading', 'Loading' );
	$( ".shirt-data-til" ).attr( 'data-action', $( this ).data( 'action' ) );
	$( ".shirt-data-til" ).attr( 'data-id', $( this ).data( 'id' ) );
	$( ".shirt-data-til" ).attr( 'data-method', $( this ).data( 'method' ) );
	var response = send_ajax( {
	    ajax: 1,
	    survey_id: $( this ).data( 'id' ),
	    action: $( this ).data( 'method' ),
	}, $( this ).data( 'action' ) );
	setTimeout( function ( ) {
	    if ( response.success ) {
		var result = response.data;
		$( ".valid-til" ).val( result.valid_date );
		$( ".copy-link" ).val( result.short_link );
	    }
	    $( ".loader-main" ).hide();
	    $( ".generate-short-link" ).btnLoading( 'reset', 'Generate' );
	}, 1500 );
    } );

    $( '.valid-til' ).datepicker( {
	dateFormat: 'yy-mm-dd',
	minDate: 0,
	setDate: new Date(),
    } );

    $( document ).delegate( ".generate-short-link", "click", function ( ) {
	var action = $( this ).parent().parent().find( ".shirt-data-til" ).attr( 'data-action' );
	var id = $( this ).parent().parent().find( ".shirt-data-til" ).attr( 'data-id' );
	var method = $( this ).parent().parent().find( ".shirt-data-til" ).attr( 'data-method' );
	var date = $( ".valid-til" ).val( );
	if ( date == '' ) {
	    $( ".valid-til" ).parent().addClass( "has-error" );
	    return false;
	} else {
	    $( ".valid-til" ).parent().removeClass( "has-error" );
	    $( this ).btnLoading( 'loading', 'Generating...' );
	    $( ".loader-main" ).show();
	    var response = send_ajax( {
		survey_id: id,
		date: date,
		action: method,
	    }, action );
	    setTimeout( function ( ) {
		if ( response.success ) {
		    var result = response.data;
		    $( ".copy-short-link" ).fadeIn( 200 );
		    $( ".copy-link" ).show();
		    $( ".copy-link" ).val( result.short_link );
		}
		$( ".loader-main" ).hide();
		$( ".generate-short-link" ).btnLoading( 'reset', 'Generate' );
	    }, 1500 );
	}
    } );

    $( document ).on( "click", "#clone", function ( ) {
	var action = $( this ).data( 'action' );
	var id = $( this ).data( 'id' );
	var method = $( this ).data( 'method' );
	$( '.overlay' ).fadeIn( 200 );
	var response = send_ajax( {
	    id: id,
	    action: method,
	}, action );
	setTimeout( function ( ) {
	    add_notification( response );
	    if ( response.success ) {
		window.location.reload( );
	    } else {
		$( '.overlay' ).fadeOut( 200 );
	    }
	}, 1500 );
    } );
    $( '.zero-audience' ).click( function () {
	if ( $( '.zero-audience' ).is( ':checked' ) ) {
	    $( ".audiance-count" ).html( "0" );
	    $( "select" ).attr( "disabled", true );
	    $( 'a' ).css( 'pointer-events', 'none' );
	    $( '#audiance_count' ).val( '0' );
	    $( '#gender' ).remove();
	} else {
	    $( '.gender' ).append( '<input type="hidden" name="grender" id="gender" value="all">' );
	    $( "select" ).attr( "disabled", false );
	    get_user_counts( );
	}
    } );
    $( document ).on( "click", "#publish", function ( ) {
	$( '#survey_id', $( '.survey-publish-model' ) ).val( $( this ).attr( 'data-id' ) );
	$( '.survey-publish-model' ).modal( 'show' );
    } );
    $( document ).on( "click", ".gender a", function ( ) {
	$( ".gender a" ).removeClass( 'active' );
	$( ".gender input" ).val( $( this ).attr( 'data-value' ) );
	$( this ).addClass( 'active' );
	get_user_counts( );
    } );
    $( document ).on( "change", "#demographics, #location, #education, select[name='age'], #relationship", function ( ) {
	get_user_counts( );
    } );
    $( document ).on( "click", "#publish", function ( ) {
	get_user_counts( );
    } );
    $( "form#survey_publish_form" ).submit( function ( ) {
	var all_ok = true;
	var par = $( this );
	all_ok = validate_form( par, all_ok );
	if ( !validateEmail( $( "input[name='email']" ).val( ) ) ) {
	    $( "input[name='email']" ).parent( ).addClass( 'has-error' );
	    all_ok = false;
	} else
	    $( "input[name='email']" ).parent( ).removeClass( 'has-error' );
	if ( all_ok ) {
	    var data = $( this ).serializeObject( );
	    $( '.overlay' ).fadeIn( 200 );
	    var response = send_ajax( data, par.attr( 'action' ) );
	    setTimeout( function ( ) {
		if ( response.success ) {
		    $( "#action", par ).val( response.action );
		    $( "#company_id", par ).val( response.company_id );
		    if ( response.active ) {
			$( "#password" ).attr( "disabled", true );
			if ( typeof data.password.length == "undefined" || data.password.length < 1 )
			    $( "#password" ).val( "123456789" );
		    } else {
			$( "#password" ).removeAttr( "disabled" );
			$( "#password" ).val( data.password );
		    }
		}
		add_notification( response );
		$( '.overlay' ).fadeOut( 200 );
	    }, 1500 );
	}

	return false;
    } );

    if ( window.location.href.indexOf( 'cHVibGlzaA' ) > -1 )
	$( "#publish" ).trigger( "click" );

    function get_user_counts( ) {
	try {
	    var data = $( "form#publish_survey_form" ).serializeObject( );
	    console.log("-------");
	    console.log(data);
	    console.log("-------");
	    data.action = 8;
	    $( ".loader-main" ).fadeIn( 200 );
	    var response = send_ajax( data, $( "form#publish_survey_form" ).attr( 'action' ) );
	    $( '.audiance-count' ).html( '...' );
	    setTimeout( function ( ) {
		$( '.audiance-count' ).html( response.count );
		$( 'input[name="audiance_count"]' ).val( response.count );
		$( ".loader-main" ).fadeOut( 200 );
	    }, 500 );
	} catch ( e ) {
	    $( '.audiance-count' ).html( '...' );
	}
    }

    /*
     $( '<script>', {
     src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70&libraries=places&callback=initAutocomplete',
     } ).appendTo( 'head' );
     */
} );
var autocomplete;
function initAutocomplete( ) {
    autocomplete = new google.maps.places.Autocomplete(
	    ( document.getElementById( 'autocomplete' ) ),
	    {
		types: [ 'geocode' ]
	    }
    );
}

function geolocate( ) {
    if ( navigator.geolocation ) {
	console.log( 'yahoo' );
	navigator.geolocation.getCurrentPosition( function ( position ) {
	    var geolocation = {
		lat: position.coords.latitude,
		lng: position.coords.longitude
	    };
	    var circle = new google.maps.Circle( {
		center: geolocation,
		radius: position.coords.accuracy
	    } );
	    autocomplete.setBounds( circle.getBounds( ) );
	} );
    }
}



/*
 $( '#cc' ).combotree( 'loadData', [ {
 id: 1,
 text: 'Languages',
 children: [ {
 id: 11,
 text: 'Java'
 }, {
 id: 12,
 text: 'C++'
 } ]
 } ] );
 
 $( '#tree' ).treeview( { data: [
 {
 text: "Parent 1",
 nodes: [
 {
 text: "Child 1",
 nodes: [
 {
 text: "Grandchild 1"
 },
 {
 text: "Grandchild 2"
 }
 ]
 },
 {
 text: "Child 2"
 }
 ]
 },
 {
 text: "Parent 2"
 },
 {
 text: "Parent 3"
 },
 {
 text: "Parent 4"
 },
 {
 text: "Parent 5"
 }
 ] } );
 */

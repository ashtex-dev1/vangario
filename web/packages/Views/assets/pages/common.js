//-----------common functions-----------
function check_error( obj )
{
    if ( strcmp( obj.val(), '' ) === 0 || strcmp( obj.val(), 0 ) === 0 )
    {
	obj.addClass( 'error' );
	all_ok = false;
    } else
    {
	obj.removeClass( 'error' );
    }
}

function strcmp( str1, str2 )
{
    return ( ( str1 == str2 ) ? 0 : ( ( str1 > str2 ) ? 1 : -1 ) );
}

function validate_form( form, all_ok ) {
    $( "input[data-required='true'], select[data-required='true'], textarea[data-required='true']", form ).each( function () {
	if ( typeof $( this ).val() === "undefined" || $( this ).val() === '' || $( this ).val() === 0 || $( this ).val() === '0' || $( this ).val() === null || !$( this ).val().length ) {
	    $( this ).parent().addClass( 'has-error' );
	    all_ok = false;
	} else if ( strcmp( $( this ).attr( 'type' ), 'email' ) === 0 && !validateEmail( $( this ).val() ) ) {
	    $( this ).parent().addClass( 'has-error' );
	    all_ok = false;
	} else
	    $( this ).parent().removeClass( 'has-error' );
    } );

    return all_ok;
}

$.fn.serializeObject = function ()
{
    var o = { };
    var a = this.serializeArray();
    $.each( a, function () {
	if ( o[this.name] ) {
	    if ( !o[this.name].push ) {
		o[this.name] = [ o[this.name] ];
	    }
	    o[this.name].push( this.value || '' );
	} else {
	    o[this.name] = this.value || '';
	}
    } );
    return o;
};

$.fn.btnLoading = function ( mode, text ) {
    switch ( mode ) {
	case 'loading':
	    this.empty()
		    .html( '<i class="fa fa-spinner fa-spin"> </i> ' + text )
		    .removeClass( 'disabled' )
		    .addClass( 'disabled' );
	    break;
	case 'reset':
	    this.empty()
		    .html( text )
		    .removeClass( 'disabled' );
	    break;
    }
};

function GetURLParameter( sParam )
{
    var sPageURL = window.location.search.substring( 1 );
    var sURLVariables = sPageURL.split( '&' );
    for ( var i = 0; i < sURLVariables.length; i++ )
    {
	var sParameterName = sURLVariables[i].split( '=' );
	if ( sParameterName[0] == sParam )
	{
	    return sParameterName[1];
	}
    }
    return '';
}

function validateEmail( email ) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test( email );
}

function isNumeric( n ) {
    return !isNaN( parseFloat( n ) ) && isFinite( n );
}

function getGetOrdinal( n ) {
    var s = [ "th", "st", "nd", "rd" ],
	    v = n % 100;
    return n + ( s[( v - 20 ) % 10] || s[v] || s[0] );
}

function select_option_box( callback ) {
    $( document ).delegate( '.input-group-btn a.option', 'click', function () {
	try {
	    var par = $( this ).closest( '.input-group-btn' );
	    $( '#selected_option', par ).text( $( this ).html() ).html( $( this ).html() );
	    callback( $( this ).closest( 'tr' ) );
	} catch ( e ) {
	    // do nothing
	}
    } );
}

function copy_to_clipboard( element ) {
    var $temp = $( "<input>" );
    $( "body" ).append( $temp );
    $temp.val( $( element ).val() ).select();
    document.execCommand( "copy" );
    $temp.remove();
}

function send_ajax( data, action, flag )
{
    var response = $.parseJSON( $.ajax( {
	url: action,
	type: "POST",
	data: data,
	dataType: "json",
	timeout: 0,
	async: false
    } ).responseText );

    return response;
}

// function for submiting form data object with ajax
function send_ajax_with_additional_prams( data, action )
{
    var response = $.parseJSON( $.ajax( {
	url: action,
	type: "POST",
	contentType: false,
	processData: false,
	cache: false,
	data: data,
	dataType: "json",
	async: false
    } ).responseText );

    return response;
}

// toaster options
toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "10000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

function add_notification( data ) { // message
    /*
     $( '<li><a href="javascript:void(0);">' + message + '</a></li>' ).appendTo( $( '#notification_area' ) );
     $( 'body' ).animate( { scrollTop: 0 }, 500, function ( ) {
     var count = parseInt( $( '.notifications-menu .count' ).html( ) ) ? parseInt( $( '.notifications-menu .count' ).html( ) ) : 0;
     $( '.notifications-menu a' ).trigger( 'click' );
     $( '.notifications-menu' ).addClass( 'open' );
     $( '.notifications-menu .count' ).html( count + 1 );
     } );
     */
    if ( data.success )
	toastr.success( data.message, 'Success!' )
    else if ( data.info )
	toastr.info( data.message, 'Info!' )
    else
	toastr.error( data.message, 'Error!' )

}

// common key bindings
$( document ).delegate( '.remove-node', 'click', function ( ) {
    var action = $( this ).data( 'action' );
    var id = $( this ).data( 'id' );
    var method = $( this ).data( 'method' );

    swal( {
	title: "Are you sure?",
	text: "You will not be able to revert this operation!",
	type: "warning",
	showCancelButton: true,
	confirmButtonColor: "#DD6B55",
	confirmButtonText: "Yes, delete it!"
    },
    function () {
	$.post( action, {
	    id: id,
	    action: method,
	}, function ( data ) {
	    data = JSON.parse( data );
	    if ( data.success )
		window.location.reload( );
	    else
		add_notification( data );
	} );
	// swal( "Deleted!", "Your imaginary file has been deleted.", "success" );
    } );
} );
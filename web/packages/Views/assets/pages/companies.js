$( document ).ready( function () {
    $( "form#company_form" ).submit( function () {
	var all_ok = true;
	var par = $( this );
	all_ok = validate_form( par, all_ok );

	if ( !validateEmail( $( "input[name='email']" ).val() ) ) {
	    $( "input[name='email']" ).parent().addClass( 'has-error' );
	    all_ok = false;
	} else
	    $( "input[name='email']" ).parent().removeClass( 'has-error' );

	if ( all_ok ) {
	    var data = $( this ).serializeObject();
	    $( '.overlay' ).fadeIn( 200 );
	    var response = send_ajax( data, par.attr( 'action' ) );
	    setTimeout( function () {
		if ( response.success ) {
		    $( "#action", par ).val( response.action );
		    $( "#company_id", par ).val( response.company_id );
		    if ( response.active ) {
			$( "#password" ).attr( "disabled", true );
			if ( typeof data.password == "undefined" || typeof data.password.length == "undefined" || data.password.length < 1 )
			    $( "#password" ).val( "123456789" );
		    } else {
			$( "#password" ).removeAttr( "disabled" );
			$( "#password" ).val( data.password );
		    }
		}
		add_notification( response );
		$( '.overlay' ).fadeOut( 200 );
	    }, 1500 );
	}

	return false;
    } );
} );

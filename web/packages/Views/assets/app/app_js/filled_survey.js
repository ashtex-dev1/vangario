$( document ).ready( function () {

    var $lis = $( ".filled-survey-ul li" ).hide();
    $lis.slice( 0, 10 ).show();
    var size_li = $lis.length;
    if ( size_li > 10 ) {
	$( ".forward" ).removeClass( "hide" );
    }
    var x = 10,
	    start = 0;

    $( '.forward' ).click( function () {
	if ( start + x < size_li ) {
	    $lis.slice( start, start + x ).hide();
	    start += x;
	    $lis.slice( start, start + x ).show();
	    $( ".prev" ).removeClass( "hide" );
	}
    } );

    $( '.prev' ).click( function () {
	$( ".forward" ).removeClass( "hide" );
	if ( start - x >= 0 ) {
	    $lis.slice( start, start + x ).hide();
	    start -= x;
	    $lis.slice( start, start + x ).show();
	}
    } );
} );
/*
 * All common function below
 */

function isset( value ) {
    return typeof value !== 'undefined';
}

function sanitize_value( value, default_value ) {
    return isset( value ) ? value : default_value;
}

function validate( value ) {
    return isset( value ) && value.length > 0 && value !== '' ? true : false;
}

function ucwords( str ) {
    return ( str + '' ).replace( /^([a-z])|\s+([a-z])/g, function ( $1 ) {
	return $1.toUpperCase();
    } );
}

function validate_email( email ) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test( email );
}

function is_numeric( num ) {
    return !isNaN( parseFloat( num ) ) && isFinite( num );
}


$.fn.serializeObject = function ()
{
    var o = { };
    var a = this.serializeArray();
    $.each( a, function () {
	if ( o[this.name] ) {
	    if ( !o[this.name].push ) {
		o[this.name] = [ o[this.name] ];
	    }
	    o[this.name].push( this.value || '' );
	} else {
	    o[this.name] = this.value || '';
	}
    } );
    return o;
};

function encode_data( data ) {
    return encodeURIComponent( data );
}

function decode_data( data ) {
    return decodeURIComponent( data );
}

function social_share( message, subject, image, link ) {
    window.plugins.socialsharing.share( message, subject, image, link );
}

var servicename = "BrandVibe";

function get_global_var( key ) {
    try {
	var result = "";
	if ( !is_web && get_device_platform() == "ios" ) {
	    try {
		new Keychain().getForKey( function ( value ) {
		    set_global_var( key, value );
		}, function ( message ) {
		}, key, servicename );
	    } catch ( e ) {
		log( "Exception: get_global_var()" );
		// will do something here
	    }
	}

	if ( isset( localStorage ) && isset( localStorage.getItem( key ) ) ) {
	    result = localStorage.getItem( key );
	}
    } catch ( e ) {
    }
    return result;
}

function set_global_var( key, value ) {
    try {
	try {
	    new Keychain().setForKey( function ( data ) {
	    }, function ( data ) {
	    }, key, servicename, value );
	} catch ( e ) {
	    // will do something here
	}

	localStorage.setItem( key, value );
    } catch ( e ) {
    }
}

function json_to_array( json ) {
    try {
	var array = new Array();
	for ( var i in json )
	    array[i] = json[i];

    } catch ( e ) {
    }

    return array;
}

$.fn.serializeObject = function () {
    var o = { };
    var a = this.serializeArray();
    $.each( a, function () {
	if ( o[this.name] ) {
	    if ( !o[this.name].push ) {
		o[this.name] = [ o[this.name] ];
	    }
	    o[this.name].push( this.value || '' );
	} else {
	    o[this.name] = this.value || '';
	}
    } );
    return o;
};

function print_map( node, longitude, latitude ) {
    try {
	var position = new google.maps.LatLng( latitude, longitude );
	var map = new google.maps.Map( node[0], { center: position, zoom: 10 } );
	var marker = new google.maps.Marker( {
	    position: position
	} );
	marker.setMap( map );
    } catch ( e ) {
    }
}

function get_app_settings() {
    try {
	var app_settings_data = { };
	if ( isset( get_global_var( global_vars.APP_SETTINGS_DATA ) ) && get_global_var( global_vars.APP_SETTINGS_DATA ) )
	    app_settings_data = JSON.parse( get_global_var( global_vars.APP_SETTINGS_DATA ) );
	else {
	    // will do something here
	}
	return app_settings_data;
    } catch ( e ) {
    }
}

function get_user_id() {
    try {
	var id = "";
	if ( isset( get_global_var( global_vars.USER_ID ) ) && get_global_var( global_vars.USER_ID ) ) {
	    id = get_global_var( global_vars.USER_ID );
	} else {
	    set_global_var( global_vars.USER_ID, 0 );
	    id = 0;
	    /*
	     if ( !in_progress ) {
	     in_progress = true;
	     }
	     */
	}
    } catch ( e ) {
    }

    return id;
}

function send_request( action, request_type, data, callback ) {
    try {
	$.ajax( {
	    url: action,
	    type: request_type,
	    data: data,
	    crossDomain: true,
	    cache: false,
	    dataType: "json",
	    success: function ( data ) {
		callback( data );
	    }
	} );
    } catch ( e ) {
	hide_indicator();
    }
}

function send_syncronize_request( action, request_type, data, callback ) {
    try {
	$.ajax( {
	    url: action,
	    type: request_type,
	    data: data,
	    async: false,
	    dataType: "json",
	    success: function ( data ) {
		callback( data );
	    }
	} );
    } catch ( e ) {
    }
}

function $_GET( param ) {
    try {
	var vars = { };
	URL.replace( location.hash, '' ).replace(
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function ( m, key, value ) { // callback
		    vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
	    return vars[param] ? vars[param] : null;
	}
    } catch ( e ) {
    }
    return vars;
}

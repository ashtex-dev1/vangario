$( document ).ready( function ( ) {

    $( ".refresh-likes" ).on( "click", function ( ) {
	swal( {
	    title: "Are you sure?",
	    text: "Do you want to refresh likes?",
	    type: "warning",
	    showCancelButton: true,
	    confirmButtonColor: "#DD6B55",
	    confirmButtonText: "Yes!"
	},
		function () {
		    $( "#preloader" ).show();
		    $( "div[data-loader='circle-side']" ).show();
		    get_user_fb_details();
		} );
//	swal( "Are you sure you want to do this?", {
//	    buttons: [ "Cancel", "Okay" ],
//	}, function () {
//	    $( "#preloader" ).show();
//	    $( "div[data-loader='circle-side']" ).show();
//	    get_user_fb_details();
//	} );
//	swal( {
//	    title: "Confirmation",
//	    text: "Do you want to refresh your likes?",
//	    type: "success",
//	}, function () {
//	    $( "#preloader" ).show();
//	    $( "div[data-loader='circle-side']" ).show();
//	    get_user_fb_details();
//	} );
//	$( ".modal-backdrop" ).show();
    } );
//    $( ".likes-okay" ).on( "click", function () {
//	$( "#preloader" ).show();
//	$( "div[data-loader='circle-side']" ).show();
//    } );

//    $( ".likes-cross-close" ).on( "click", function () {
//	$( ".refresh-likes-modal" ).hide();
//	$( ".modal-backdrop" ).hide();
//    } );

    $( 'h3[data-toggle="tooltip"]' ).tooltip( );
    var $lis = $( ".likes_ul li" ).hide( );
    $lis.slice( 0, 10 ).show( );
    var size_li = $lis.length;
    if ( size_li > 10 ) {
	$( ".forward" ).removeClass( "hide" );
    }
    var x = 10,
	    start = 0;
    $( '.forward' ).click( function ( ) {
	if ( start + x < size_li ) {
	    $lis.slice( start, start + x ).hide( );
	    start += x;
	    $lis.slice( start, start + x ).show( );
	    $( ".prev" ).removeClass( "hide" );
	}
    } );
    $( '.prev' ).click( function ( ) {
	$( ".forward" ).removeClass( "hide" );
	if ( start - x >= 0 ) {
	    $lis.slice( start, start + x ).hide( );
	    start -= x;
	    $lis.slice( start, start + x ).show( );
	}
    } );
} );
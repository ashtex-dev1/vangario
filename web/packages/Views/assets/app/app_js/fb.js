/*
 * 
 * init
 */
function fb_init() {
    try {
	FB.init( {
	    appId: '2000798346815610',
	    autoLogAppEvents: true,
	    status: true,
	    xfbml: true,
	    version: 'v2.9'
	} );
    } catch ( e ) {
//	log( e );
    }
}

/*
 * Check login status
 */
function check_fb_login() {
    try {
	$( "#preloader" ).show();
	$( "div[data-loader='circle-side']" ).show();
	FB.getLoginStatus( function ( response ) {
	    console.log( "*******" );
	    console.log( response );
	    console.log( "*******" );
	    if ( response.status === 'connected' ) {
		set_global_var( global_vars.FB_ID, response.authResponse.userID );
		set_global_var( global_vars.FB_ACCESS_TOKEN, response.authResponse.accessToken );
		get_user_fb_details();
	    } else
		fb_login();
	} );
    } catch ( e ) {
	$( "#preloader" ).hide();
	$( "div[data-loader='circle-side']" ).hide();
	swal( {
	    title: "Error",
	    text: "Something Went Wrong...",
	    type: "warning",
	}, function () {
	    window.location.replace( '/login' );
	} );
    }
}


/*
 * Login
 */
function fb_login() {
    try {
	FB.login( function ( response ) {
	    if ( response.authResponse ) {
		check_fb_login();
	    } else {
		swal( {
		    title: "Error",
		    text: "Unable to login. Please try again...",
		    type: "warning",
		}, function () {
		    window.location.replace( '/login' );
		} );
	    }
	}, {
	    scope: global_vars.FB_PERMISSIONS,
	} );

    } catch ( e ) {

    }
}

/*
 * Likes
 */
function get_user_fb_details() {
    try {
	FB.api( '/me/?fields=' + global_vars.FB_FIELDS, function ( response ) {
	    var user_data = response;
	    /*if ( isset( response.picture.data.url ) )
	     set_global_var( global_vars.FB_IMAGE, response.picture.data.url );
	     else
	     set_global_var( global_vars.FB_IMAGE, "assets/imgs/default-user.png" );
	     
	     console.log( user_data );
	     */
	    response.action = api_actions.USER_UPDATE;
	    response.web = "web";
	    if ( isset( response.likes.data ) ) {
		likes = [ ];

		if (
			( isset( response.likes.paging ) && isset( response.likes.paging.next ) ) ||
			( isset( response.likes.paging ) && isset( response.likes.paging.previous ) )
			) {
		    var url = ( isset( response.likes.paging ) && isset( response.likes.paging.previous ) ) ? response.likes.paging.previous : response.likes.paging.next;
		    fetch_pages( url, "likes", function () {
			var data = [ ];
			$.each( likes, function ( i, entries ) {
			    $.each( entries, function ( i, entry ) {
				data.push( entry );
			    } );
			} );
			response.likes = data;
			send_user_data( response, user_data );
		    } );
		}
	    } else {
		send_user_data( response, user_data );
	    }
	} );
    } catch ( e ) {

    }
}

function fetch_pages( page, variable, callback ) {
    try {
	$.get( page, function ( response ) {
	    if ( isset( response ) && isset( response.data ) ) {
		eval( variable ).push( response.data );
		if ( isset( response.paging ) && isset( response.paging.next ) )
		    fetch_pages( response.paging.next, variable, callback );
		else
		    callback();
	    } else
		callback();
	}, "json" );
    } catch ( e ) {

    }
}

function send_user_data( data, user_data ) {
    set_global_var( global_vars.USER_FB_DATA, JSON.stringify( data ) );
    send_request( api_base_url, "POST", data, function ( response ) {
	if ( response.success ) {
	    set_global_var( global_vars.USER_ID, response.id );
	    set_global_var( global_vars.USER_DATA, JSON.stringify( user_data ) );
	    if ( window.location.href == 'http://bv.brandvibe.me/likes' ) {
		window.location.replace( "/likes" );
	    } else {
		var Base64 = { _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function ( e ) {
			var t = "";
			var n, r, i, s, o, u, a;
			var f = 0;
			e = Base64._utf8_encode( e );
			while ( f < e.length ) {
			    n = e.charCodeAt( f++ );
			    r = e.charCodeAt( f++ );
			    i = e.charCodeAt( f++ );
			    s = n >> 2;
			    o = ( n & 3 ) << 4 | r >> 4;
			    u = ( r & 15 ) << 2 | i >> 6;
			    a = i & 63;
			    if ( isNaN( r ) ) {
				u = a = 64
			    } else if ( isNaN( i ) ) {
				a = 64
			    }
			    t = t + this._keyStr.charAt( s ) + this._keyStr.charAt( o ) + this._keyStr.charAt( u ) + this._keyStr.charAt( a )
			}
			return t
		    }, decode: function ( e ) {
			var t = "";
			var n, r, i;
			var s, o, u, a;
			var f = 0;
			e = e.replace( /[^A-Za-z0-9+/=]/g, "" );
			while ( f < e.length ) {
			    s = this._keyStr.indexOf( e.charAt( f++ ) );
			    o = this._keyStr.indexOf( e.charAt( f++ ) );
			    u = this._keyStr.indexOf( e.charAt( f++ ) );
			    a = this._keyStr.indexOf( e.charAt( f++ ) );
			    n = s << 2 | o >> 4;
			    r = ( o & 15 ) << 4 | u >> 2;
			    i = ( u & 3 ) << 6 | a;
			    t = t + String.fromCharCode( n );
			    if ( u != 64 ) {
				t = t + String.fromCharCode( r )
			    }
			    if ( a != 64 ) {
				t = t + String.fromCharCode( i )
			    }
			}
			t = Base64._utf8_decode( t );
			return t
		    }, _utf8_encode: function ( e ) {
			e = e.replace( /rn/g, "n" );
			var t = "";
			for ( var n = 0; n < e.length; n++ ) {
			    var r = e.charCodeAt( n );
			    if ( r < 128 ) {
				t += String.fromCharCode( r )
			    } else if ( r > 127 && r < 2048 ) {
				t += String.fromCharCode( r >> 6 | 192 );
				t += String.fromCharCode( r & 63 | 128 )
			    } else {
				t += String.fromCharCode( r >> 12 | 224 );
				t += String.fromCharCode( r >> 6 & 63 | 128 );
				t += String.fromCharCode( r & 63 | 128 )
			    }
			}
			return t
		    }, _utf8_decode: function ( e ) {
			var t = "";
			var n = 0;
			var r = c1 = c2 = 0;
			while ( n < e.length ) {
			    r = e.charCodeAt( n );
			    if ( r < 128 ) {
				t += String.fromCharCode( r );
				n++
			    } else if ( r > 191 && r < 224 ) {
				c2 = e.charCodeAt( n + 1 );
				t += String.fromCharCode( ( r & 31 ) << 6 | c2 & 63 );
				n += 2
			    } else {
				c2 = e.charCodeAt( n + 1 );
				c3 = e.charCodeAt( n + 2 );
				t += String.fromCharCode( ( r & 15 ) << 12 | ( c2 & 63 ) << 6 | c3 & 63 );
				n += 3
			    }
			}
			return t
		    } }
		window.location.replace( "/dashboard/" + Base64.encode( 'id' ) + '-' + Base64.encode( response.id ) );
//		window.location.replace( "/dashboard" );
	    }
	} else {
	    $( "#preloader" ).hide();
	    $( "div[data-loader='circle-side']" ).hide();
	    swal( {
		title: "Error",
		text: "Something Went Wrong",
		type: "warning",
	    }, function () {
		window.location.replace( "/login" );
	    } );
	}
    } );
}

/*
 * TESTING
 */

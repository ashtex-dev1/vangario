<?php

include('Crypt/RSA.php');

$rsa = new Crypt_RSA();
extract( $rsa->createKey() );

$plaintext = '10';

$rsa->loadKey( $privatekey );
$ciphertext = $rsa->encrypt( $plaintext );

$publickey = array( "publickey" => "-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAOx5il/lsVI+KXXNn7hOVtQzNLsTKEVU
ict6i0ex52M2j4W9/r0nADSwXvHZ2NW6Z9swM9l6REzrk0Ke/Xe+esECAwEAAQ==
-----END PUBLIC KEY-----" );
$rsa->loadKey( $publickey );

echo $rsa->decrypt( $ciphertext );

/*

define('DECODING', false);

include('Crypt/RSA.php');

$rsa = new Crypt_RSA();

$rsa->setPublicKeyFormat(CRYPT_RSA_PUBLIC_FORMAT_XML);
$rsa->setPrivateKeyFormat(CRYPT_RSA_PRIVATE_FORMAT_XML);

$key_arrays = $rsa->createKey(1024);

$privatekey = $key_arrays['privatekey'];
$publickey = $key_arrays['publickey'];

file_put_contents('test-private.xml', $privatekey);
file_put_contents('test-public.xml', $publickey);

$plaintext = 'terrafrost';

//$privatekey = decode($privatekey);

//$publickey = decode($publickey);

$rsa->loadKey($publickey, CRYPT_RSA_PUBLIC_FORMAT_XML);
$rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
//echo "Private Key:<BR><pre>$privatekey</pre><br><br>Public Key:<BR><pre>$publickey</pre><BR><BR>";

$ciphertext =  encode($rsa->encrypt($plaintext));

//echo "Encrypted message:<BR><pre>$ciphertext</pre><br>";

$rsa->loadKey($privatekey, CRYPT_RSA_PRIVATE_FORMAT_XML);
$rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
echo $rsa->decrypt(decode($ciphertext));


function encode($string){
if(DECODING){
return base64_encode($string);
}else{
return $string;
}
}

function decode($string){
if(DECODING){
return base64_decode($string);
}else{
return $string;
}
}*/
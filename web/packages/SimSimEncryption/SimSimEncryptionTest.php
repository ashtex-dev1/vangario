<?php

/*
 * Sample code file for encryption testing
 * Usage: php SimSimEncryptionTest.php
 */

define( 'DEBUG', 0 );

//include_once 'Crypt/RSA.php';

/*
 * Method 1 using CryptRSA
 */

print_message( "--------METHOD #1--------", "-" );

$rsa = new Crypt_RSA(); // object initialization
$rsa->setEncryptionMode( CRYPT_RSA_ENCRYPTION_PKCS1 );
//extract( $rsa->createKey( 512 ) );
//
//print_message( $publickey, "Public" );
//print_message( $privatekey, "Private" );
//
//die();


$plaintext = 10; // string to encrypt
print_message( $plaintext, "Plain Text" );

// loading public key that we generated
$rsa->loadKey( get_public_key() );

// encryption of plain text using public key
$ciphertext = $rsa->encrypt( $plaintext );
print_message( $ciphertext, "Ciphered Text" );

// base64 encode ciphered text
$ciphertext = base64_encode( $ciphertext );
print_message( $ciphertext, "Encoded Text" );
if ( !DEBUG )
    print_message( "curl -H \"Content-Type: application/json\" -X POST -d '{\"customerId\":506530299,\"payeeMobileNumber\":923077374974,\"amount\":\"{$ciphertext}\",\"invoiceID\": 1001,\"text\":\"Dummy Payment\"}' http://devmerchant.finpay.pk/public/api/payOrPayAnyOne", "Request" );

if ( DEBUG || 1 )
{
    // loading private key for decryption
    $rsa->loadKey( get_private_key() );

    // base64 decode ciphered text
    $ciphertext = base64_decode( $ciphertext );

    $decrypted_text = $rsa->decrypt( $ciphertext );
    print_message( $decrypted_text, "Decrypted Text" );
}


exit();

/*
 * Method 2 using OpenSSL PKey
 */
print_message( "--------METHOD #2--------", "-" );

// Configuration settings for the key
$config = array(
    "private_key_bits" => 512,
    "private_key_type" => OPENSSL_KEYTYPE_RSA,
);

// Create the private and public key
$res = openssl_pkey_new( $config );

// Extract the private key into $private_key
openssl_pkey_export( $res, $private_key );

$private_key = DEBUG ? $private_key : "-----BEGIN RSA PRIVATE KEY-----
MIIBOgIBAAJBAMrZXXzhIizb5tGEsPD8ngLVBeEpcNWC3tYprb0PVdIJxrHVvrZJ
xWKiXXhXWTzZw6awB60rVUO7dGCEonQSoTMCAwEAAQJAIwc4CpmB4kcy65lDSMoG
qyyH8GLMlkIcItVdqBke078HQFVUIMQS7IB3zRvCNwIVWq5vTvjJemenTZcLLmgU
KQIhAP5SWtf4fIZ7/dKQAxheDxAWZZLLE/1MP120cReGAqidAiEAzDANvtBBeoQ1
63xHgrPCnhoOd2KHtriYh5E3siq5wA8CIBVjx9AyTgbhls+G3TvHbWzk9VYLTnsX
Vp0C+CAKXV7hAiEAgyrLaax2o7GSwbeQaJH5j37FlEOjUum6cjwDP1za/lkCIARf
u+m9TkfhVzVsk6m1j/BWiS1DmklNkEvAFFH54yzS
-----END RSA PRIVATE KEY-----";

// Extract the public key into $public_key
$public_key = openssl_pkey_get_details( $res );

// Override public key if DEBUG mode is off
$public_key = DEBUG ? $public_key[ "key" ] : "-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAOx5il/lsVI+KXXNn7hOVtQzNLsTKEVU
ict6i0ex52M2j4W9/r0nADSwXvHZ2NW6Z9swM9l6REzrk0Ke/Xe+esECAwEAAQ==
-----END PUBLIC KEY-----";

$plaintext = 10; // string to encrypt
print_message( $plaintext, "Plain Text" );

// Encrypt using the public key
openssl_public_encrypt( $plaintext, $encrypted, $public_key );

print_message( $encrypted, "Ciphered Text" );

$encoded = base64_encode( $encrypted );

print_message( $encoded, "Encoded Text" );

if ( !DEBUG )
    print_message( "curl -H \"Content-Type: application/json\" -X POST -d '{\"customerId\":506530299,\"payeeMobileNumber\":923077374974,\"amount\":\"{$encoded}\",\"invoiceID\": 1001,\"text\":\"Dummy Payment\"}' http://devmerchant.finpay.pk/public/api/payOrPayAnyOne", "Request" );

if ( DEBUG || 1 )
{
    // Decrypt the data using the private key
    openssl_private_decrypt( base64_decode( $encoded ), $decrypted, $private_key );
    print_message( $decrypted, "Decrypted Text" );
}

function get_public_key()
{
    return DEBUG ? "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAPJS/6itaNmbhOZYEzflZCkn8TrMwN45
FxRxafIjLK2D2IWeSNAE8bIQ1tvmmE4jKFdd0uK+A0AY7SpWQx9RFd8CAwEAAQ==" : "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMrZXXzhIizb5tGEsPD8ngLVBeEpcNWC
3tYprb0PVdIJxrHVvrZJxWKiXXhXWTzZw6awB60rVUO7dGCEonQSoTMCAwEAAQ==";
}

function get_private_key()
{
    return DEBUG ? "MIIBPAIBAAJBAPJS/6itaNmbhOZYEzflZCkn8TrMwN45FxRxafIjLK2D2IWeSNAE
8bIQ1tvmmE4jKFdd0uK+A0AY7SpWQx9RFd8CAwEAAQJBANM4f3pi9QLA6X3yhYXy
YHTzSPTM5syNDM4X8ZZLDLWMheJNEEr2TVQZHRrtuxpxAmLicj/YK1RRNE7LN4Jb
G5kCIQD79qYpQB/R0A5sXwFUKl3z8ngeJI/EDC/zYTnhqJQcWwIhAPY00S+sMBsk
61OujNthuaXdGSEmsanF1L5vFg03JPPNAiEA+H3mneQp/YjN0L9Z1iH6iu5k3IV+
bAEBAWyv4weZGuUCIDKtuIlplhVgShhVi6T0vrQHiM6sGlHp6NqyHwXlXfPZAiEA
4wP6NTL7VO2hMF2tpLuC4Izk786w45bPjpqUhSal5xU=" : "MIIBOgIBAAJBAMrZXXzhIizb5tGEsPD8ngLVBeEpcNWC3tYprb0PVdIJxrHVvrZJ
xWKiXXhXWTzZw6awB60rVUO7dGCEonQSoTMCAwEAAQJAIwc4CpmB4kcy65lDSMoG
qyyH8GLMlkIcItVdqBke078HQFVUIMQS7IB3zRvCNwIVWq5vTvjJemenTZcLLmgU
KQIhAP5SWtf4fIZ7/dKQAxheDxAWZZLLE/1MP120cReGAqidAiEAzDANvtBBeoQ1
63xHgrPCnhoOd2KHtriYh5E3siq5wA8CIBVjx9AyTgbhls+G3TvHbWzk9VYLTnsX
Vp0C+CAKXV7hAiEAgyrLaax2o7GSwbeQaJH5j37FlEOjUum6cjwDP1za/lkCIARf
u+m9TkfhVzVsk6m1j/BWiS1DmklNkEvAFFH54yzS
";
}

function print_message( $message, $type )
{
    echo PHP_EOL . "[ " . date( "M d H:i:s" ) . " ][ {$type} ] " . print_r( $message, 1 ) . PHP_EOL;
}

#!/bin/bash
if [ ! -f /var/www/html/dev_portal/.lock ]; then
   ps aux | grep -i "staging=1" | grep -v "grep" |  awk '{print $2}' |xargs  kill -9
   echo "daemon killed"
fi

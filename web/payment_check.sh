#!/bin/bash
result=`ps aux | grep -i "payment_web=1" | grep -v "grep" | wc -l`
if [ $result -ge 1 ]
   then
        echo "payment daemon is up"
   else
	cd /var/www/html/web && sh payment.sh
        echo "payment daemon restarted"
fi

function populate_profile() {
    console.log( "run_profile()" );
    show_indicator();
    
    send_request( api_base_url, "POST", {
	id: get_global_var( global_vars.USER_ID ),
	action: api_actions.USER_DETAILS,
	method: api_actions.GET,
    }, function ( data ) {
	console.log( data );
	hide_indicator();

	if ( isset( get_global_var( global_vars.FB_IMAGE ) ) && get_global_var( global_vars.FB_IMAGE ) !== null ) {
	    $( ".facebook-avatar img" ).attr( 'src', get_global_var( global_vars.FB_IMAGE ) );
    }

	if ( isset( get_global_var( global_vars.USER_FB_DATA ) ) && get_global_var( global_vars.USER_FB_DATA ) !== null ) {
	    var user_data = JSON.parse( get_global_var( global_vars.USER_FB_DATA ) );
        log( get_global_var( global_vars.USER_FB_DATA ) );
        var work = "";
        var education = "";
        var user_likes = "";
        $.each( user_data.work, function( i, record ) {
            if ( isset( record.employer ) )
                work += record.employer.name + ", ";
        } );
        
        $.each( user_data.education, function( i, record ) {
            if ( isset( record.school ) )
                education += record.school.name + ", ";
        } );

        $.each( user_data.likes.data, function( i, record ) {
            if ( isset( record.name ) )
                user_likes += "<tr><td class='label-cell'>" + record.name + "</td></tr>";
        } );
        
        $( "#work" ).val( work.replace(/,+$/,'') );
        $( "#education" ).val( education.replace(/,+$/,'') );
        // $( "table tbody" ).html( user_likes );
    }

	if ( data.success ) {
	    $( '.facebook-name' ).html( data.details.name );
	    $.each( data.details, function ( id, value ) {
		$( '#' + id ).val( value );
	    } );
	} else {
	    show_alert( data.message );
	}
    } );

    $( document ).off( 'click', '.save' );
    $( document ).on( 'click', '.save', function () {
        try {
            var all_ok = true;
            if ( !validate_email( $( 'input[name="email"]' ).val() ) )
                all_ok = false;

            if ( !is_numeric( $( 'input[name="age"]' ).val() ) )
            all_ok = false;
            if ( all_ok ) {
                show_indicator();
                var data = $.extend( {
                id: get_global_var( global_vars.USER_ID ),
                action: api_actions.USER_DETAILS,
                method: api_actions.UPDATE,
                }, $( "#profile-form" ).serializeObject() );
                send_request( api_base_url, "POST", data, function ( response ) {
                console.log( response );
                hide_indicator();

                $( '.facebook-name' ).html( data.name );
                } );
            } else {
                show_alert( "Invalid data" );
            }
        } catch ( e ) {
            show_alert( "Something went wrong. Please try again..." );
            hide_indicator();
        }
    } );
}

populate_profile();
function login_action() {
    $( ".login-button" ).on( "click", function () {
	try {
	    if ( is_web ) {
		check_fb_login();
	    } else {
		facebookConnectPlugin.login( global_vars.FB_PERMISSIONS.split( ', ' ), function ( data ) {
		    setTimeout( function () {
			main_app.addNotification( {
			    title: 'Brand Vibe',
			    message: 'Finding surveys related to your interests...'
			} );
		    }, 1000 );
		    show_indicator();
		    if ( isset( get_global_var( global_vars.USER_FB_DATA ) ) ) {
			facebookConnectPlugin.api( 'me/?fields=id,email,name,first_name,likes,education,work,groups,birthday,relationship_status', global_vars.FB_PERMISSIONS.split( ', ' ),
				function ( response ) {
				    var user_data = response;
				    try {
					response.action = api_actions.USER_UPDATE;
					response.device_id = get_device_id();
					if ( isset( response.likes ) && isset( response.likes.data ) ) {
					    likes = [ ];
					    if ( isset( response.likes.paging ) && isset( response.likes.paging.next ) ) {
						likes.push( response.likes.data );
						fetch_pages( response.likes.paging.next, "likes", function () {
						    var data = [ ];
						    $.each( likes, function ( i, entries ) {
							$.each( entries, function ( i, entry ) {
							    data.push( entry );
							} );
						    } );
						    response.likes = data;
						    send_user_data( response, user_data );
						} );
					    } else
						send_user_data( response, user_data );
					} else {
					    send_user_data( response, user_data );
					}

				    } catch ( e ) {
					show_alert( "Something went wrong. Please reopen the app and try again..." );
				    }
				} );
		    } else {
			send_user_data( JSON.parse( get_global_var( global_vars.USER_FB_DATA ) ), user_data );
		    }
		} );
	    }
	} catch ( e ) {
	    show_alert( "Something went wrong. Please try again..." );
	}
    } );
}

login_action();

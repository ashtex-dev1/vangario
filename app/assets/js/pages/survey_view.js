function populate_survey_view() {
    log( "populate_survey_view()" );
    var index = global_vars.SURVEY_DATA + "_" + $_GET( 'id' );
    var par = $( '.survey_view' );
    if ( isset( get_global_var( index ) ) && get_global_var( index ) != null ) {
	var data = JSON.parse( get_global_var( index ) );
	$( '.title input', par ).val( data.stitle );
	$( '.company input', par ).val( data.brand );
	$( '.date input', par ).val( data.attempted );
	$( '.amount input', par ).val( '30' );
	hide_indicator();
    } else {
	$( '.amount input', par ).val( '0' );
	show_alert( "Something went wrong. Please try again..." )
    }
}

populate_survey_view();

function populate_survey() {
    try {
	//console.log( "ID" );
	//console.log( $_GET( 'id' ) );
	show_indicator();
	send_request( api_base_url, "POST", {
	    action: api_actions.SURVEY_DETAILS,
	    id: $_GET( 'id' ),
	}, function ( data ) {
	    hide_indicator();
	    var long = $( '.long-section:first' ).clone();
	    var short = $( '.short-section:first' ).clone();
	    var radio = $( '.radio-section:first' ).clone();
	    var checkbox = $( '.checkbox-section:first' ).clone();
	    var single_image = $( '.single-image-section:first' ).clone();
	    var multi_image = $( '.multi-image-section:first' ).clone();
	    var rating_image = $( '.rating-image-section:first' ).clone();
	    $( '.checkbox-section, .radio-section, .short-section, .long-section, .single-image-section, .multi-image-section, .rating-image-section' ).remove();
	    if ( isset( data ) && isset( data.success ) && isset( data.survey ) ) {
		var survey = data.survey;
		$( ".survey-title" ).html( survey.title );
		$.each( survey.questions, function ( i, question ) {
		    var node;
		    switch ( question.type ) {
			case 'single-image':
			case 'multi-image':
			case 'rating-image':
			    node = eval( question.type.replace( '-', '_' ) ).clone();
			    break;
			default:
			    node = eval( question.type ).clone();
			    break;
		    }

		    node.attr( 'data-num', i )
			    .attr( 'data-id', question.id )
			    .attr( 'data-type', question.type );
		    $( ".question_title", node ).html( 'Q: ' + question.title );
		    var answer_option = $( ".option:first", node ).clone();
		    $( ".option:first", node ).remove();
		    $.each( question.options, function ( i, option ) {
			var sub_option = answer_option.clone();
			if ( question.type == "radio" || question.type == "checkbox" ) {
			    $( 'p', sub_option ).html( option.label );
			    $( 'input', sub_option )
				    .attr( 'name', question.type + "_" + question.id )
				    .attr( 'data-id', option.id )
				    .val( option.value );
			} else {
			    $( 'input, textarea', sub_option )
				    .attr( 'name', question.type + "_" + question.id )
				    .attr( 'data-id', option.id );
			}
			if ( question.type == "single-image" ) {
			    $( 'p', sub_option ).html( option.label );
			    $( 'img', sub_option ).attr( 'src', option.image_url );
			    $( 'span.correct-option', sub_option ).html( option.value );
			    $( 'input', sub_option )
				    .attr( 'name', question.type + "_" + question.id )
				    .attr( 'data-id', option.id )
				    .val( option.value );
			} else if ( question.type == "multi-image" ) {
			    $( 'p', sub_option ).html( option.label );
			    $( 'img', sub_option ).attr( 'src', option.image_url );
			    $( 'span.correct-option', sub_option ).html( option.value );
			    $( 'select', sub_option )
				    .attr( 'name', question.type + "_" + question.id )
				    .attr( 'data-id', option.id )
				    .val( option.value );
			} else if ( question.type == "rating-image" ) {
			    $( 'p', sub_option ).html( option.label );
			    $( 'img', sub_option ).attr( 'src', option.image_url );
			    $( 'span.tittle', sub_option ).html( option.value );
			    var options = "<option value=''>Rate Image</option>";
			    for ( var j = 1; j <= question.options.length; j++ ) {
				options += "<option value='" + j + "'>" + j + "</option>";
			    }
			    $( 'select', sub_option )
				    .html( options )
				    .attr( 'name', question.type + "_" + question.id )
				    .attr( 'data-id', option.id );
			} else {
			    $( 'input, textarea', sub_option )
				    .attr( 'name', question.type + "_" + question.id )
				    .attr( 'data-id', option.id );
			}


			sub_option.appendTo( $( '.answers', node ) );
		    } );

		    node.insertBefore( $( '.navigation-buttons' ) );
		} );

		$( '[data-num="0"]' ).fadeIn();
	    }
	} );
    } catch ( e ) {
	log( e );
	hide_indicator();
	show_alert( "Something went wrong. Please try again." );
    }
//    $( '.select' ).on( 'change', function () {
//	alert();    
//    } );
    $( document ).off( 'change', 'select.select' );
    $( document ).on( 'change', 'select.select', function () {
	var selected = $( this ).val();
	var num = $( this ).attr( 'data-id' );
	$( 'option[value!="' + selected + '"]', $( this ) ).remove();
	$( ".select" ).each( function () {
	    if ( num != $( this ).attr( 'data-id' ) )
		$( 'option[value="' + selected + '"]', $( this ) ).remove();
	} );
    } );
    $( document ).off( 'click', '.reset' );
    $( document ).on( 'click', '.reset', function (  ) {
	var options = "<option value=''>Rate Image</option>";
	for ( var j = 1; j <= $( '.option', $( this ).closest( '.rating-image-section' ) ).length; j++ ) {
	    options += "<option value='" + j + "'>" + j + "</option>";
	}

	$( "select", $( this ).closest( '.rating-image-section' ) ).each( function ( i, el ) {
	    $( this ).attr( 'data-num', i ).html( options );
	} );
    } );

    $( document ).off( 'click', '.next' );
    $( document ).on( 'click', '.next', function () {
	var all_ok = true;
	var active_node = $( '.question:visible' );
	var index = parseInt( active_node.attr( 'data-num' ) );

	if ( active_node.attr( "data-type" ) == "radio" || active_node.attr( "data-type" ) == "checkbox" || active_node.attr( "data-type" ) == "single-image" || active_node.attr( "data-type" ) == "multi-image" ) {
	    all_ok = $( 'input', active_node ).is( ":checked" );
	} else {
	    all_ok = $( 'input, textarea, select', active_node ).val().trim() != "";
	}

	if ( all_ok ) {
	    if ( $( '[data-num="' + ( index + 1 ) + '"]' ).length ) {
		$( '.question' ).hide();
		$( '[data-num="' + ( index + 1 ) + '"]' ).show();
		$( '.prev' ).removeAttr( "disabled" );
		if ( !$( '[data-num="' + ( index + 2 ) + '"]' ).length ) {
		    $( this ).attr( "disabled", "disabled" ).hide();
		    $( '.submit' ).show();
		}
	    } else {
		$( this ).attr( "disabled", "disabled" ).hide();
		$( '.submit' ).show();
	    }
	} else {
	    show_alert( "Please answer the question." );
	}
    } );

    $( document ).off( 'click', '.prev' );
    $( document ).on( 'click', '.prev', function () {
	var index = parseInt( $( '.question:visible' ).attr( 'data-num' ) );
	if ( $( '[data-num="' + ( index - 1 ) + '"]' ).length ) {
	    $( '.question' ).hide();
	    $( '[data-num="' + ( index - 1 ) + '"]' ).show();

	    $( '.next' ).show().removeAttr( "disabled" );
	    $( '.submit' ).hide();
	    if ( !$( '[data-num="' + ( index - 2 ) + '"]' ).length ) {
		$( this ).attr( "disabled", "disabled" );
	    }
	} else {
	    $( this ).attr( "disabled", "disabled" );
	}
    } );

    $( document ).off( 'click', '.submit' );
    $( document ).on( 'click', '.submit', function () {
	var all_ok = true;
	var active_node = $( '.question:visible' );
	if ( active_node.attr( "data-type" ) == "radio" || active_node.attr( "data-type" ) == "checkbox" || active_node.attr( "data-type" ) == "single-image" || active_node.attr( "data-type" ) == "multi-image" ) {
	    all_ok = $( 'input', active_node ).is( ":checked" );
	} else {
	    console.log( all_ok );
	    all_ok = $( 'input, textarea, select', active_node ).val().trim() != "";
	    console.log( all_ok );
	}

	if ( all_ok ) {
	    var data = {
		action: api_actions.SURVEY_RESPONSE,
		user: get_global_var( global_vars.USER_ID ),
		survey: $_GET( 'id' ),
		method: api_actions.SUBMITTED,
		questions: [ ]
	    };

	    $( ".question" ).each( function () {
		var answer = {
		    id: $( this ).attr( "data-id" ),
		    options: [ ],
		};
		switch ( $( this ).attr( 'data-type' ) ) {
		    case 'single-image':
		    case 'radio':
			answer.options.push( {
			    id: $( "input:checked", $( this ) ).attr( "data-id" ),
			    value:
				    $( "input:checked", $( this ) ).val()
			} );
			break;
		    case 'checkbox':
		    case 'multi-image':
			$( "input:checked", $( this ) ).each( function () {
			    answer.options.push( {
				id: $( this ).attr( "data-id" ),
				value:
					$( this ).val(),
			    } );
			} );
			break;
		    case 'rating-image':
			$( "select", $( this ) ).each( function () {
			    answer.options.push( {
				id: $( this ).attr( "data-id" ),
				value: $( this ).val(),
			    } );
			} );
			break;
		    default:
			answer.options.push( {
			    id: $( "input, textarea", $( this ) ).attr( "data-id" ),
			    value: $( "input, textarea", $( this ) ).val(),
			} );
			break;
		}
		data.questions.push( answer );
	    } );

	    send_request( api_base_url, "POST", data, function ( data ) {
		log( "Success" );
		log( data );

		main_app.alert( data.message, 'BrandVibe', function () {
		    main_view.router.loadPage( "pages/dashboard.html" );
		} );
	    } );

	} else {
	    show_alert( "Please answer the question." );
	}
    } );

    $( document ).off( 'click', '.abort-survey' );
    $( document ).on( 'click', '.abort-survey', function () {
	main_app.confirm( "Are you sure you want to abort this survey? \n This action would be irreversible.", 'BrandVibe', function () {
	    var data = {
		action: api_actions.SURVEY_RESPONSE,
		user: get_global_var( global_vars.USER_ID ),
		survey: $_GET( 'id' ),
		method: api_actions.ABORTED,
	    };

	    show_indicator();
	    send_request( api_base_url, "POST", data, function ( data ) {
		log( "Success" );
		log( data );

		hide_indicator();
		main_view.router.loadPage( "pages/dashboard.html" );
	    } );
	} );
    } );
}

populate_survey();

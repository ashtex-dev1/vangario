function populate_filled_surveys() {
    log( "populate_surveys()" );
    show_indicator();
    
    var all = $( '.filled-survey li:first' ).clone();
    $( '.filled-survey li' ).remove();

    send_request( api_base_url, "POST", {
	id: get_global_var( global_vars.USER_ID ),
	action: api_actions.FILLED_SURVEYS,
    }, function ( response ) {
	console.log( "Done" );
	console.log( response );
	var amount = 0;

	if ( isset( response.surveys ) ) {
	    $.each( response.surveys, function ( i, survey ) {
		var node = all.clone();
		$( '.link', node ).attr( "href", "pages/survey_view.html?id=" + survey.sid );
		$( '.item-title', node ).html( survey.stitle );
		$( '.item-after', node ).html( survey.published );
		$( '.item-text', node ).html( survey.brand );
		node.appendTo( $( '.filled-survey' ) );
		set_global_var( global_vars.SURVEY_DATA + "_" + survey.sid, JSON.stringify( survey ) );
		amount += 30;
	    } );
	} else {
	    var node = all.clone();
	    $( '.item-title', node ).html( "No survey available..." );
	    $( '.item-after, .item-text', node ).html( "" );
	    $( '.item-title-row', node ).removeClass( 'item-title-row' );
	    node.appendTo( $( '.filled-survey' ) );
	}

	$( '.earned-amount' ).html( amount );
	hide_indicator();
    } );
}

populate_filled_surveys();
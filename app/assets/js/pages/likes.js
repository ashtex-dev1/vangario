function populate_likes() {
//    console.log( "populate_likes()" );
//    log(get_global_var( global_vars.USER_DATA ));
    if ( isset( get_global_var( global_vars.USER_DATA ) ) && get_global_var( global_vars.USER_DATA ) !== null ) {
	var user_data = JSON.parse( get_global_var( global_vars.USER_DATA ) );
	var user_likes = "";
	var likes = { };
	if ( isset( user_data.likes.data ) )
	    likes = user_data.likes.data;
	else if ( isset( user_data.likes ) )
	    likes = user_data.likes;

	$.each( likes, function ( i, record ) {
	    if ( isset( record.name ) && record.name )
		user_likes += "<tr><td class='label-cell'>" + record.name + "</td></tr>";
	} );
	$( ".page.likes table tbody" ).html( user_likes );
    }
}

populate_likes();

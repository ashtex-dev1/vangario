function populate_surveys() {
    log( "populate_surveys()" );
    show_indicator();
    console.log(JSON.parse(get_global_var( global_vars.USER_FB_DATA)));

    var all = $( '.all-survey li:first' ).clone();
    // var recent = $( '.recent-survey li:first' ).clone();
    console.log( $( '.all-survey li:first' ).length );
    $( '.all-survey li:first, .recent-survey li' ).remove();
    console.log( $( '.all-survey li:first' ).length );

    send_request( api_base_url, "POST", {
	id: get_global_var( global_vars.USER_ID ),
	action: api_actions.SURVEYS,
    }, function ( response ) {
	log( "Done" );
	log( response );

	if ( isset( response.no_survey ) ) {
	    // show_alert( response.no_survey );
	    /*
	     main_app.addNotification( {
	     title: 'BrandVibe',
	     subtitle: 'Welcome',
	     message: response.no_survey,
	     media: '<i class="icon icon-f7"></i>'
	     } );
	     */
	}

	var count = 0;

	if ( isset( response.surveys ) && isset( response.surveys.all ) ) {
	    $.each( response.surveys.all, function ( i, survey ) {
		var node = all.clone();
		$( '.link', node ).attr( "href", "pages/survey.html?id=" + survey.sid );
		$( '.item-title', node ).html( survey.stitle );
		$( '.item-after', node ).html( survey.published );
		$( '.item-text', node ).html( survey.brand );
		node.appendTo( $( '.all-survey' ) );
		count++;
	    } );
	} else {
	    var node = all.clone();
	    $( '.item-title', node ).html( "No survey available..." );
	    $( '.item-after', node ).html( "" );
	    $( '.item-title-row', node ).removeClass( 'item-title-row' );
	    node.appendTo( $( '.all-survey' ) );
	}
/*
	if ( isset( response.surveys ) && isset( response.surveys.recent ) ) {
	    $.each( response.surveys.recent, function ( i, survey ) {
		var node = recent.clone();
		$( '.link', node ).attr( "href", "pages/survey.html?id=" + survey.sid );
		$( '.item-title', node ).html( survey.stitle );
		$( '.item-after', node ).html( survey.published );
		$( '.item-text', node ).html( survey.brand );
		node.appendTo( $( '.recent-survey' ) );
		count++;
	    } );
	} else {
	    var node = recent.clone();
	    $( '.item-title', node ).html( "No recent survey available..." );
	    $( '.item-after', node ).html( "" );
	    $( '.item-title-row', node ).removeClass( 'item-title-row' );
	    node.appendTo( $( '.recent-survey' ) );
	}
*/
	if ( !is_web ) {
        set_global_var( global_vars.SURVEY_COUNT, count );
	    cordova.plugins.notification.badge.set( count );
    }
	
	hide_indicator();
	if ( refresh_interests ) {
	    refresh_interests = false;
	    main_app.confirm( 'Do you want to restore your intrests?', 'BrandVibe', function () {
		if ( is_web ) {
		    get_user_fb_detail();
		} else {
		    fb_intrestes();
		}
	    } );
	}

    } );
}

populate_surveys();

/*
 * 
 * init
 */
function fb_init() {
    try {
	log( "fb_init()" );
	if ( is_web ) {
	    FB.init( {
		appId: '2000798346815610',
		autoLogAppEvents: true,
		status: true,
		xfbml: true,
		version: 'v2.9'
	    } );
	}
    } catch ( e ) {
	log( e );
    }
}

/*
 * Check login status
 */
function check_fb_login() {
    log( "check_fb_login()" );
    try {
	if ( is_web ) {
	    FB.getLoginStatus( function ( response ) {
		log( "check_fb_login()" );
		log( response );
		if ( response.status === 'connected' ) {
		    set_global_var( global_vars.FB_ID, response.authResponse.userID );
		    set_global_var( global_vars.FB_ACCESS_TOKEN, response.authResponse.accessToken );
		    get_user_fb_details();
		} else
		    fb_login();
	    } );
	} else {
	    // will do something here
	}
    } catch ( e ) {
	log( e );
    }
}


/*
 * Login
 */
function fb_login() {
    log( "fb_login()" );

    try {
	if ( is_web ) {
	    console.log( "--------" );
	    console.log( global_vars.FB_PERMISSIONS );
	    console.log( "--------" );
	    FB.login( function ( response ) {
		if ( response.authResponse ) {
		    check_fb_login();
		} else {
		    show_alert( "Unable to login. Please try again..." );
		}
	    }, {
		scope: global_vars.FB_PERMISSIONS,
	    } );
	} else {
	    facebookConnectPlugin.login( global_vars.FB_PERMISSIONS.split( ', ' ), function ( data ) {
		show_alert( JSON.stringify( data ) );
	    } );
	}
    } catch ( e ) {
	log( e );
    }
}

/*
 * Likes
 */
function get_user_fb_details() {
    log( "get_user_fb_details()" );

    try {
	if ( is_web ) {
	    show_indicator();
	    FB.api( '/me/?fields=' + global_vars.FB_FIELDS, function ( response ) {
		//log( response );
		var user_data = response;
		console.log( response );
		/*if ( isset( response.picture.data.url ) )
		 set_global_var( global_vars.FB_IMAGE, response.picture.data.url );
		 else
		 set_global_var( global_vars.FB_IMAGE, "assets/imgs/default-user.png" );*/
		response.action = api_actions.USER_UPDATE;
		if ( isset( response.likes.data ) ) {
		    likes = [ ];
		    
		    if ( 
			    ( isset( response.likes.paging ) && isset( response.likes.paging.next ) ) ||
			    ( isset( response.likes.paging ) && isset( response.likes.paging.previous ) )
		    ) {
			var url = ( isset( response.likes.paging ) && isset( response.likes.paging.previous ) ) ? response.likes.paging.previous : response.likes.paging.next;
			fetch_pages( url, "likes", function () {
			    var data = [ ];
			    $.each( likes, function ( i, entries ) {
				$.each( entries, function ( i, entry ) {
				    data.push( entry );
				} );
			    } );
			    response.likes = data;

			    send_user_data( response, user_data );
			} );
		    }
		} else {
		    send_user_data( response, user_data );
		}
	    } );
	}
    } catch ( e ) {
	alert( "catch" );
	log( e );
    }
}

function fetch_pages( page, variable, callback ) {
    try {
	$.get( page, function ( response ) {
	    if ( isset( response ) && isset( response.data ) ) {
		log( response.data )
		eval( variable ).push( response.data );
		if ( isset( response.paging ) && isset( response.paging.next ) )
		    fetch_pages( response.paging.next, variable, callback );
		else
		    callback();
	    } else
		callback();
	}, "json" );
    } catch ( e ) {
	log( 'fetch_pages()' );
	log( e );
    }
}

function send_user_data( data, user_data ) {
    set_global_var( global_vars.USER_FB_DATA, JSON.stringify( data ) );
    send_request( api_base_url, "POST", data, function ( response ) {
	log( "Done" );
	log( JSON.stringify( response ) );
	hide_indicator();

	set_global_var( global_vars.USER_ID, response.id );
	set_global_var( global_vars.USER_DATA, JSON.stringify( user_data ) );

	if ( isset( $_GET( 'id' ) ) && $_GET( 'id' ) ) {
	    var survey_id = $_GET( 'id' );
	    main_view.router.loadPage( 'pages/survey.html?id=' + survey_id );
	} else {
	    main_view.router.loadPage( "pages/dashboard.html" );
	}
    } );
}

/*
 * TESTING
 */

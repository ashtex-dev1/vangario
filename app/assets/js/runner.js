/*
 * All runner functions below
 */

function run_login() {
    login_action();
}

function run_survey() {
    populate_survey();
}

function run_profile() {
    populate_profile();
}

function run_likes() {
    populate_likes();
}

function run_dashboard() {
    populate_surveys();
}

function run_filled_surveys() {
    populate_filled_surveys();
}

function run_survey_view() {
    populate_survey_view();
}
function get_user_fb_detail() {
    get_user_fb_details();
}
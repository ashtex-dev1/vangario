/*
 * All common function below
 */

function log( message ) {
    if ( debug )
	console.log( JSON.stringify( message ) );
}

function get_device_id() {
    return isset( device.uuid ) ? device.uuid : 0; // (new Date())
}

function get_device_platform() {
    return isset( device.platform ) ? device.platform.toLowerCase() : '';
}

function hide_statusbar() {
    StatusBar.styleBlackTranslucent();
    StatusBar.hide();
}

function isset( value ) {
    return typeof value !== 'undefined';
}

function sanitize_value( value, default_value ) {
    return isset( value ) ? value : default_value;
}

function validate( value ) {
    return isset( value ) && value.length > 0 && value !== '' ? true : false;
}

function ucwords( str ) {
    return ( str + '' ).replace( /^([a-z])|\s+([a-z])/g, function ( $1 ) {
	return $1.toUpperCase();
    } );
}

function validate_email( email ) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test( email );
}

function is_numeric( num ) {
    return !isNaN( parseFloat( num ) ) && isFinite( num );
}

function hide_indicator() {
    main_app.hideIndicator();
}

function show_indicator() {
    main_app.showIndicator();
}

$.fn.serializeObject = function ()
{
    var o = { };
    var a = this.serializeArray();
    $.each( a, function () {
	if ( o[this.name] ) {
	    if ( !o[this.name].push ) {
		o[this.name] = [ o[this.name] ];
	    }
	    o[this.name].push( this.value || '' );
	} else {
	    o[this.name] = this.value || '';
	}
    } );
    return o;
};

function bind_clicks() {
    $( '.open-indicator' ).off( 'click' );
    $( '.open-indicator' ).on( 'click', function () {
	main_app.showIndicator();
    } );

    $( '.under-construction' ).on( 'click', function () {
	main_app.hideIndicator();
	show_alert( 'Deze functie zal binnenkort worden toegevoegd. Blijf kijken.' );
    } );

    $( document ).off( "click", ".logout" );
    $( document ).on( "click", ".logout", function () {
	try {
	    facebookConnectPlugin.logout( function () {
		localStorage.clear();
		main_view.router.loadPage( "pages/login.html" );
	    }, function () {
		show_alert( "Something went wrong. Please try again..." );
		hide_indicator();
	    } );
	} catch ( e ) {
	    log( e );
	    localStorage.clear();
	    main_view.router.loadPage( "pages/login.html" );
	    //show_alert( "Something went wrong. Please try again..." );
	    hide_indicator();
	}
    } );

    $( document ).undelegate( 'a[href^="tel:"]', 'click' );
    $( document ).delegate( 'a[href^="tel:"]', 'click', function () {
	window.open( $( this ).attr( 'href' ), '_system' );
    } );

    $( document ).undelegate( 'a[href^="mailto:"]', 'click' );
    $( document ).delegate( 'a[href^="mailto:"]', 'click', function () {
	window.open( $( this ).attr( 'href' ), '_system' );
    } );
}

function encode_data( data ) {
    return encodeURIComponent( data );
}

function decode_data( data ) {
    return decodeURIComponent( data );
}

function page_visited( page ) {
    var visited = false;
    for ( var i = 0; i < main_view.history.length; i++ ) {
	if ( main_view.history[i].indexOf( page ) > -1 ) {
	    visited = true;
	    break;
	}
    }

    return visited;
}

function show_alert( message ) {
    try {
	main_app.alert( message, 'BrandVibe' );
    } catch ( e ) {
	log( 'show_alert()' );
	log( e );
    }
}

function social_share( message, subject, image, link ) {
    window.plugins.socialsharing.share( message, subject, image, link );
}

// prepare the callback functions
function onSuccess( msg ) {
    log( "Success" );
    log( msg );
}
;
function onError( msg ) {
    log( "Error" );
    log( msg );
}
;
var servicename = "BrandVibe";

function get_global_var( key ) {
    try {
	var result = "";
	if ( !is_web && get_device_platform() == "ios" ) {
	    log( "Get from keychain" );
	    try {
		new Keychain().getForKey( function ( value ) {
		    set_global_var( key, value );
		}, function ( message ) {
		    log( "Failure in Get" );
		    log( message );
		}, key, servicename );
	    } catch ( e ) {
		log( "Exception: get_global_var()" );
		// will do something here
	    }
	}

	if ( isset( localStorage ) && isset( localStorage.getItem( key ) ) ) {
	    result = localStorage.getItem( key );
	}
    } catch ( e ) {
	log( 'set_global_var()' );
	log( e );
    }
    return result;
}

function set_global_var( key, value ) {
    try {
	if ( !is_web && get_device_platform() == "ios" ) {
	    try {
		new Keychain().setForKey( function ( data ) {
		    show_alert( "Set in Keychain" );
		    log( data );
		}, function ( data ) {
		    show_alert( "Failure in Set" );
		    log( data );
		}, key, servicename, value );
	    } catch ( e ) {
		log( "Exception: set_global_var()" );
		show_alert( "Exception: set_global_var()" );
		// will do something here
	    }
	}

	localStorage.setItem( key, value );
    } catch ( e ) {
	show_alert( "Outer Exception - In Set" );
	show_alert( e );
    }
}

function json_to_array( json ) {
    try {
	var array = new Array();
	for ( var i in json )
	    array[i] = json[i];

    } catch ( e ) {
	log( 'json_to_array()' );
	log( e );
    }

    return array;
}

$.fn.serializeObject = function () {
    var o = { };
    var a = this.serializeArray();
    $.each( a, function () {
	if ( o[this.name] ) {
	    if ( !o[this.name].push ) {
		o[this.name] = [ o[this.name] ];
	    }
	    o[this.name].push( this.value || '' );
	} else {
	    o[this.name] = this.value || '';
	}
    } );
    return o;
};

function print_map( node, longitude, latitude ) {
    try {
	log( node );
	var position = new google.maps.LatLng( latitude, longitude );
	var map = new google.maps.Map( node[0], { center: position, zoom: 10 } );
	var marker = new google.maps.Marker( {
	    position: position
	} );
	marker.setMap( map );
    } catch ( e ) {
	log( 'print_map()' );
	log( e );
    }
}

function get_app_settings() {
    try {
	var app_settings_data = { };
	if ( isset( get_global_var( global_vars.APP_SETTINGS_DATA ) ) && get_global_var( global_vars.APP_SETTINGS_DATA ) )
	    app_settings_data = JSON.parse( get_global_var( global_vars.APP_SETTINGS_DATA ) );
	else {
	    // will do something here
	}
	return app_settings_data;
    } catch ( e ) {
	log( 'get_app_settings()' );
	log( e );
    }
}

function get_user_id() {
    try {
	var id = "";
	if ( isset( get_global_var( global_vars.USER_ID ) ) && get_global_var( global_vars.USER_ID ) ) {
	    id = get_global_var( global_vars.USER_ID );
	} else {
	    set_global_var( global_vars.USER_ID, 0 );
	    id = 0;
	    /*
	     if ( !in_progress ) {
	     in_progress = true;
	     }
	     */
	}
    } catch ( e ) {
	log( 'get_user_id()' );
	log( e );
    }

    return id;
}

function asset_loaded( filename, filetype ) {
    try {
	var already_loaded = false;
	switch ( filetype ) {
	    case 'js':
		var assets = document.getElementsByTagName( "script" );
		break;
	    case 'css':
		var assets = document.getElementsByTagName( "link" );
		break;
	}

	for ( var i = 0; i < assets.length; i++ ) {
	    if ( assets[i].getAttribute( 'src' ) == filename ) {
		already_loaded = true;
		break;
	    } else if ( assets[i].getAttribute( 'href' ) == filename ) {
		already_loaded = true;
		break;
	    }
	}
    } catch ( e ) {
	log( 'asset_loaded()' );
	log( e );
    }

    return already_loaded;
}

function load_js_css_file( filename, filetype ) {
    try {
	if ( !asset_loaded( filename, filetype ) )
	    switch ( filetype ) {
		case 'js':
		    var fileref = document.createElement( 'script' );
		    fileref.setAttribute( "type", "text/javascript" );
		    fileref.setAttribute( "src", filename );
		    break;
		case 'css':
		    var fileref = document.createElement( "link" );
		    fileref.setAttribute( "rel", "stylesheet" );
		    fileref.setAttribute( "type", "text/css" );
		    fileref.setAttribute( "href", filename );
		    break;
	    }

	if ( isset( fileref ) && fileref !== "" && fileref !== null && fileref !== "null" )
	    document.getElementsByTagName( "head" )[0].appendChild( fileref )
    } catch ( e ) {
	log( 'loadjscssfile()' );
	log( e );
    }
}

function load_fragment( page ) {
    try {
	var z, i, elmnt, file, xhttp, type, script;
	z = document.getElementsByTagName( "div" );
	for ( i = 0; i < z.length; i++ ) {
	    elmnt = z[i];
	    // load fragment related scripts
	    type = elmnt.getAttribute( "data-type" );
	    script = elmnt.getAttribute( "data-script" );
	    if ( type === 'fragment' && isset( script ) && script.length ) {
		elmnt.removeAttribute( "data-type" );
		elmnt.removeAttribute( "data-script" );
		load_js_css_file( ( 'assets/js/pages/%script%.js' ).replace( '%script%', script ), 'js' );
	    }

	    // load fragments
	    file = elmnt.getAttribute( "data-load-fragment" );
	    if ( file ) {
		try {
		    xhttp = new XMLHttpRequest();
		    log( file )
		    xhttp.onreadystatechange = function () {
			elmnt.innerHTML = this.responseText;
			elmnt.removeAttribute( "data-load-fragment" );
			var script = $( page.container ).data( 'script' );
			if ( isset( script ) && script.length ) {
			    var script_path = ( 'assets/js/pages/%script%.js' ).replace( '%script%', script );
			    if ( asset_loaded( script_path, 'js' ) && isset( $( page.container ).data( 'runner' ) ) && $( page.container ).data( 'runner' ) ) {
				var run = eval( 'run_' + $( page.container ).data( 'runner' ) );
				run();
			    }
			}
			load_fragment( page );
		    }
		    xhttp.open( "GET", file, true );
		    xhttp.send();
		    return;
		} catch ( e ) {
		    log( e );
		}
	    }
	}
    } catch ( e ) {
	log( 'load_fragment()' );
	log( e );
    }
}

function send_request( action, request_type, data, callback ) {
    try {
	$.ajax( {
	    url: action,
	    type: request_type,
	    data: data,
	    crossDomain: true,
	    cache: false,
	    dataType: "json",
	    success: function ( data ) {
		callback( data );
	    }
	} );
    } catch ( e ) {
	log( "send_request()" );
	log( e );
	hide_indicator();
    }
}

function send_syncronize_request( action, request_type, data, callback ) {
    try {
	$.ajax( {
	    url: action,
	    type: request_type,
	    data: data,
	    async: false,
	    dataType: "json",
	    success: function ( data ) {
		callback( data );
	    }
	} );
    } catch ( e ) {
	log( "send_request()" );
	log( e );
    }
}

function $_GET( param ) {
    try {
	log( URL );
	var vars = { };
	URL.replace( location.hash, '' ).replace(
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function ( m, key, value ) { // callback
		    vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
	    return vars[param] ? vars[param] : null;
	}
    } catch ( e ) {
	log( "$_GET()" );
	log( e );
    }
    return vars;
}

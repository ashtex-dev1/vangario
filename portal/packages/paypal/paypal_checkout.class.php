<?php

class PaypalCheckout extends \Config\RSBase {

	//private variables
	protected $esc;

	public function __construct() {
		parent::__construct();
	}

	//escape function
	public function __call( $closure, $args ) {
		$f = Plusql::escape( $this->profile );
		return $f( $args[ 0 ] );
	}

	public function run() {
		try {
			$this->update_main_contents();
		} catch ( \Exception $e ) {
			return false;
		}
	}

	public function update_main_contents() {
		$this->process_to_paypal( $this->process_order() );
	}

	private function process_order() {
		$order_id = 0;
		try {
			if ( isset( $_POST ) ) {
				$corrupt = false;
				$school_id = $this->central->getargs( 'school_id', $_POST, $corrupt );
				if ( ! $corrupt ) {
					try {
						$school = Plusql::from( $this->profile )->school->select( '*' )->where( 'school_id=' . $school_id )->run()->school;
						Plusql::into( $this->profile )->orders( array( 'school_id' => $school_id, 'user_id' => $_SESSION[ 'user' ][ 'user_id' ], 'status' => 'pending', 'date' => date( 'Y-m-d' ) ) )->insert();
						$order_id = \Config\Central::get_accurate_last_id( 'orders', $this->profile );
						if ( isset( $_POST[ 'marketing_report' ] ) && $_POST[ 'marketing_report' ] ) {
							Plusql::into( $this->profile )->orders_details( array( 'orders_id' => $order_id, 'label' => 'marketing_report', 'price' => Config\Constants::MARKETING_REPORT_PRICE ) )->insert();
						}
						if ( isset( $_POST[ 'sms_service' ] ) && $_POST[ 'sms_service' ] ) {
							Plusql::into( $this->profile )->orders_details( array( 'orders_id' => $order_id, 'label' => 'sms_service', 'price' => Config\Constants::MARKETING_REPORT_PRICE ) )->insert();
						}
					} catch ( Exception $ex ) {
						
						$this->__redirect( '?r=DMErrorPage' );
					}
				} else
					$this->__redirect( '?r=DMErrorPage' );
			} else {
				$this->__redirect( '?r=DMErrorPage' );
			}
		} catch ( Exception $ex ) {
			$this->__redirect( '?r=DMErrorPage' );
		}
		return $order_id;
	}

	private function process_to_paypal( $order_id ) {
		try {
			$paypal_request = new Paypal();
			$paypal_request->setParam( 'cmd', '_cart' );
			$paypal_request->setParam( 'upload', '1' );
			$counter = 1;
			$orders_details = Plusql::from( $this->profile )->orders_details->select( '*' )->where( "orders_id = {$order_id} AND is_deleted <> 1" )->run()->orders_details;
			foreach ( $orders_details as $orders_detail ){
				if ( $orders_detail->price > 0 ) {
					$paypal_request->setParam( 'amount_' . $counter, $orders_detail->price );
					$paypal_request->setParam( 'quantity_' . $counter, 1 );
					$paypal_request->setParam( 'item_name_' . $counter, $orders_detail->label );
					$counter ++;
				}
			}
			$paypal_request->setCustom( $order_id );
			$paypal_request->setParam( 'return', $paypal_request->returnUrl() );
			$paypal_request->initiate();
		} catch ( Exception $e ) {
			return false;
		}
	}

}

?>

<?php

class Paypal
{

    public $params = array();

    const PAYPAL_CGI_LOCATION = '/cgi-bin/webscr';
    const PAYPAL_HOST = 'www.paypal.com';
    const PAYPAL_SANDBOX_HOST = 'www.sandbox.paypal.com';
    const PAYPAL_PORT = 80;
    const PAYPAL_SANDBOX_PORT = 443;
    //this must be the email address of our merchant paypal account
    const BUSINESS_EMAIL = 'joe@strategicsec.com';
    const SANDBOX_BUSINESS_EMAIL = 'ashtex4ga@gmail.com';
    const CURRENCY = 'USD';
    const PAYPAL_TESTING = 1;
    const RETURN_RUNNABLE = 'PaypalResponse';

    public function __construct()
    {
        $this->initParams();
    }

    public function initParams()
    {
        $this->params = array(
            'cmd' => '_xclick',
            'business' => $this->getBusinessEmailAddress(),
            'receiver_email' => $this->getBusinessEmailAddress(),
            //'item_name' => 'SOMETHING I SELL',
//            'amount' => 0.00,
            'currency_code' => self::CURRENCY,
            'return' => $this->returnUrl(),
            'custom' => '',
            'rm' => 2,
            'country' => 'AU',
            'email' => ''
        );
    }

    public function returnUrl()
    {
        if (self::PAYPAL_TESTING)
        {
            $surl = (isset($_SERVER['HTTPS'])) ? 'https://' : 'http://';
            $surl .= isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'localhost';
            $path = pathinfo($_SERVER['PHP_SELF']);
            if (!empty($path['dirname']))
                $surl .= $path['dirname'];
            $surl .= '/?r=' . self::RETURN_RUNNABLE;
            return $surl;
        }
        else
        {
            return 'http://www.dojomagic.net/portal/?r=' . self::RETURN_RUNNABLE;
        }
    }

    public function getBusinessEmailAddress()
    {
        if (self::PAYPAL_TESTING == 1)
            $ret = self::SANDBOX_BUSINESS_EMAIL;
        else
            $ret = self::BUSINESS_EMAIL;

        return $ret;
    }

    public function getPaypalFullUri()
    {
        $ret = false;

        if (self::PAYPAL_TESTING == 1)
            $ret = $this->getPaypalSandboxUri();
        else
            $ret = $this->getPaypalUri();

        return $ret;
    }

    public function getPaypalSandboxUri()
    {
        return 'https://' . self::PAYPAL_SANDBOX_HOST . $this->queryString();
    }

    public function getPaypalUri()
    {
        return 'https://' . self::PAYPAL_HOST . $this->queryString();
    }

    public function setParam($name, $value)
    {
        $ret = false;

        //if(array_key_exists($name, $this->params))
        {
            $this->params[$name] = trim($value);
            //echo print_r($this->params).'<br>';
            $ret = true;
        }

        return $ret;
    }

    public function setCustom($value)
    {
        return $this->setParam('custom', $value);
    }

    public function setAmount($value)
    {
        //return $this->setParam('amount', $value);
    }

    public function setBuyerEmail($value)
    {
        return $this->setParam('email', $value);
    }

    public function __redirect($url)
    {
        ?><html><body><script type="text/javascript">top.location.href = '<?php echo $url; ?>';</script></body></html><?php
        die();
    }

    public function initiate()
    {
        $uri = $this->getPaypalFullUri();
        header('location:' . $uri);
        $this->__redirect($uri);
        exit();
    }

    public function queryString()
    {
        $ret = self::PAYPAL_CGI_LOCATION . '?';
        $name_value_pairs = array();
        foreach ($this->params as $param_name => $param_value)
            $name_value_pairs[] = trim($param_name) . '=' . urlencode(trim($param_value));
        $ret .= implode('&', $name_value_pairs);
        return $ret;
    }

}
?>

<?php

use Config\Central;

class IPNReceiver implements \rocketsled\Runnable {

	//private variables
	private $central;
	private $esc;
	private $profile = "ipn_getter";

	public function __construct() {
		@session_start();
		$this->central = Central::instance();
		$this->central->set_alias_connection( $this->profile );
	}

	//escape function
	public function __call( $closure, $args ) {
		$f = Plusql::escape( $this->profile );
		return $f( $args[ 0 ] );
	}

	public function run() {
		$this->register_ipn( $_REQUEST );
		ob_start();
		print_r( $_POST );
		$data = ob_get_clean();
		$this->log( $data );
	}

	public function register_ipn( $post ) {
		if ( strlen( $post[ 'custom' ] ) && strcmp( $post[ 'payment_status' ], 'Completed' ) == 0 ) {//we have the order number available
			try {
				Plusql::on( $this->profile )->orders( array( "ipn" => "1" ) )->where( "orders_id={$post[ 'custom' ]}" )->update();
			} catch ( EmptySetException $e ) {
				// do nothing
			}
		}
	}

	public function log( $msg ) {
		$msg = '[' . date( 'Y-m-d H:i:s' ) . ']: ' . $msg . PHP_EOL;
		file_put_contents( self::logFileName(), $msg, FILE_APPEND );
	}

	public static function logFileName() {
		return PACKAGES_DIR . '/ipnlog/paypal_ipn_logger.log';
	}

}

<?php
class Param
{

    private static $configFile = 'data/config.ini';
    private static $configArray = false;

    public static function getVariable( $section, $variable )
    {
        if ( !self::$configArray ) self::$configArray = parse_ini_file( self::$configFile, true );
        if ( isset( self::$configArray[ $section ][ $variable ] ) )
        {
            return self::$configArray[ $section ][ $variable ];
        }
        else
        {
            return null;
        }
    }

}

?>

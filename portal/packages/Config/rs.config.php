<?php

if ( file_exists( PACKAGES_DIR . '/pheanstalk/pheanstalk_init.php' ) )
    require_once(PACKAGES_DIR . '/pheanstalk/pheanstalk_init.php');

if ( file_exists( PACKAGES_DIR . '/swiftmailer/lib/swift_required.php' ) )
    require_once(PACKAGES_DIR . '/swiftmailer/lib/swift_required.php');

if ( file_exists( PACKAGES_DIR . '/DOMTemplate/domtemplate.php' ) )
    require_once(PACKAGES_DIR . '/DOMTemplate/domtemplate.php');

if ( file_exists( PACKAGES_DIR . '/DOMTemplateGoodies/domtemplateapppaths.php' ) )
    require_once(PACKAGES_DIR . '/DOMTemplateGoodies/domtemplateapppaths.php');

if ( file_exists( PACKAGES_DIR . '/DOMTemplateGoodies/domtemplateimgpaths.php' ) )
    require_once(PACKAGES_DIR . '/DOMTemplateGoodies/domtemplateimgpaths.php');

if ( file_exists( PACKAGES_DIR . '/PHPExcel/Classes/PHPExcel.php' ) )
    require_once(PACKAGES_DIR . '/PHPExcel/Classes/PHPExcel.php');

if ( file_exists( PACKAGES_DIR . '/Facebook/autoload.php' ) )
    require_once(PACKAGES_DIR . '/Facebook/autoload.php');

if ( file_exists( PACKAGES_DIR . '/RSA/Crypt/RSA.php' ) )
    require_once(PACKAGES_DIR . '/RSA/Crypt/RSA.php');

if ( file_exists( PACKAGES_DIR . '/RSA/Math/BigInteger.php' ) )
    require_once(PACKAGES_DIR . '/RSA/Math/BigInteger.php');
?>

-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 23, 2017 at 08:23 PM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-11+deb.sury.org~xenial+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vangario`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `answer_id` int(11) NOT NULL,
  `response_id` int(11) DEFAULT NULL,
  `question` varchar(55) DEFAULT NULL,
  `Answer` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_name` varchar(55) NOT NULL,
  `company_email` varchar(55) NOT NULL,
  `company_info` varchar(200) NOT NULL,
  `image` varchar(300) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fb_check_ins`
--

CREATE TABLE `fb_check_ins` (
  `fb_check_ins_id` int(11) NOT NULL,
  `user_profile_id` int(11) DEFAULT NULL,
  `fb_check_ins` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fb_groups`
--

CREATE TABLE `fb_groups` (
  `fb_groups_id` int(11) NOT NULL,
  `user_profile_id` int(11) DEFAULT NULL,
  `fb_groups` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fb_likes`
--

CREATE TABLE `fb_likes` (
  `fb_likes_id` int(11) NOT NULL,
  `user_profile_id` int(11) DEFAULT NULL,
  `fb_likes` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invitation`
--

CREATE TABLE `invitation` (
  `invitaion_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `survey_form_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questionnaire`
--

CREATE TABLE `questionnaire` (
  `questionnaire_id` int(11) NOT NULL,
  `title` varchar(55) DEFAULT NULL,
  `data` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionnaire`
--

INSERT INTO `questionnaire` (`questionnaire_id`, `title`, `data`, `created_at`, `updated_at`, `deleted`) VALUES
(1, '0', '{"questions":[{"title":"Do you like the campaign?","type":"radio-button","options":[{"value":"Yes"},{"value":"No"},{"value":"Maybe"}]},{"title":"Was the campaign informative","type":"check-button","options":[{"value":"Agree"},{"value":"Disagree"}]},{"title":"qwe","type":"short-answer"},{"title":"Hey Testing ","type":"check-button","options":[{"value":"yeah"},{"value":"no"}]},{"title":"Mahham\'s Test Question - Vamp","type":"check-button","options":[{"value":"Title 1"},{"value":"Title "},{"value":"23453"}]}]}', '2017-06-06 08:28:59', '2017-06-21 06:44:20', 0),
(2, 'Survey  1', '{"questions":[{"title":"Do you like the campaign?","type":"radio-button","options":[{"value":"Yes"},{"value":"No"},{"value":"Maybe"}]},{"title":"Was the campaign informative","type":"check-button","options":[{"value":"Agree"},{"value":"Disagree"}]}]}', '2017-06-06 08:34:56', '2017-06-07 05:51:50', 0),
(3, '123123', '{"questions":[{"title":"123","type":"short-answer"}]}', '2017-06-06 08:46:46', '2017-06-06 08:46:55', 1),
(4, '123', '{"questions":[{"title":"123","type":"short-answer"}]}', '2017-06-06 03:48:55', '2017-06-06 03:48:55', 1),
(5, '123', '{"questions":[{"title":"123","type":"short-answer"}]}', '2017-06-06 05:37:56', '2017-06-06 05:37:56', 1),
(6, 'dsgdfgeegegd', '{"questions":[{"title":"asdfsdgasfdgsdfhfdghfghfdghdfg","type":"radio-button","options":[{"value":"asgsdfg"},{"value":"fgjkghffg"},{"value":"sgasdfdfs"}]}]}', '2017-06-08 03:43:24', '2017-06-14 06:24:02', 1),
(7, '0', '{"questions":[{"title":"qwe","type":"short-answer"}]}', '2017-06-20 07:33:07', '2017-06-20 07:33:07', 1),
(8, '0', '{"questions":[{"title":"Hey Testing ","type":"check-button","options":[{"value":"yeah"},{"value":"no"}]}]}', '2017-06-20 07:43:57', '2017-06-20 07:43:57', 0),
(9, '0', '{"questions":[{"title":"Mahham\'s Test Question - Vamp","type":"check-button","options":[{"value":"Title 1"},{"value":"Title "}]}]}', '2017-06-21 05:20:16', '2017-06-21 05:20:16', 0),
(10, '0', '{"questions":[{"title":"Testing123","type":"check-button","options":[{"value":"1"},{"value":"2"},{"value":"3"}]}]}', '2017-06-21 06:47:49', '2017-06-21 06:47:49', 0),
(11, '0', '{"questions":[{"title":"Testing123","type":"short-answer"}]}', '2017-06-21 06:48:13', '2017-06-21 06:48:13', 0),
(12, '0', '{"questions":[{"title":"Do you like the campaign?","type":"radio-button","options":[{"value":"Yes"},{"value":"No"},{"value":"Maybe"}]},{"title":"Was the campaign informative","type":"check-button","options":[{"value":"Agree"},{"value":"Disagree"}]},{"title":"qwe","type":"short-answer"},{"title":"Hey Testing ","type":"check-button","options":[{"value":"yeah"},{"value":"no"}]},{"title":"Mahham\'s Test Question - Vamp","type":"check-button","options":[{"value":"Title 1"},{"value":"Title "},{"value":"23453"}]},{"title":"Do you like the campaign?","type":"radio-button","options":[{"value":"Yes"},{"value":"No"},{"value":"Maybe"}]},{"title":"Was the campaign informative","type":"check-button","options":[{"value":"Agree"},{"value":"Disagree"}]},{"title":"qwe","type":"short-answer"},{"title":"Hey Testing ","type":"check-button","options":[{"value":"yeah"},{"value":"no"}]},{"title":"Mahham\'s Test Question - Vamp","type":"check-button","options":[{"value":"Title 1"},{"value":"Title "}]},{"title":"Testing123","type":"check-button","options":[{"value":"1"},{"value":"2"},{"value":"3"}]},{"title":"Testing123","type":"short-answer"},{"title":"Do you like the campaign?","type":"radio-button","options":[{"value":"Yes"},{"value":"No"},{"value":"Maybe"}]},{"title":"Was the campaign informative","type":"check-button","options":[{"value":"Agree"},{"value":"Disagree"}]},{"title":"qwe","type":"short-answer"},{"title":"Hey Testing ","type":"check-button","options":[{"value":"yeah"},{"value":"no"}]},{"title":"Mahham\'s Test Question - Vamp","type":"check-button","options":[{"value":"Title 1"},{"value":"Title "},{"value":"23453"}]},{"title":"Do you like the campaign?","type":"radio-button","options":[{"value":"Yes"},{"value":"No"},{"value":"Maybe"}]},{"title":"Was the campaign informative","type":"check-button","options":[{"value":"Agree"},{"value":"Disagree"}]},{"title":"qwe","type":"short-answer"},{"title":"Hey Testing ","type":"check-button","options":[{"value":"yeah"},{"value":"no"}]},{"title":"Mahham\'s Test Question - Vamp","type":"check-button","options":[{"value":"Title 1"},{"value":"Title "}]},{"title":"Testing123","type":"check-button","options":[{"value":"1"},{"value":"2"},{"value":"3"}]},{"title":"Testing123","type":"short-answer"},{"title":"Check Testingsas","type":"paragraph"}]}', '2017-06-21 06:48:45', '2017-06-22 07:31:41', 1),
(16, '0', '{"questions":[{"title":"321","type":"short-answer"}]}', '2017-06-22 08:23:46', '2017-06-22 08:23:46', 1),
(17, '0', '{"questions":[{"title":"New Testing 213","type":"short-answer"}]}', '2017-06-23 07:44:06', '2017-06-23 07:44:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `response`
--

CREATE TABLE `response` (
  `response_id` int(11) NOT NULL,
  `user_id` text,
  `survey_form_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `survey_form`
--

CREATE TABLE `survey_form` (
  `survey_form_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `name` text,
  `data` text,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `survey_form`
--

INSERT INTO `survey_form` (`survey_form_id`, `company_id`, `name`, `data`, `status`, `created_at`, `deleted`) VALUES
(1, 4, 'test23f', '{"questions":[{"title":"tester","type":"short-answer"}]}', 1, '2017-06-19 01:22:59', 0),
(2, 4, 'qweq', '{"questions":[{"title":"asd","type":"short-answer"}]}', 1, '2017-06-19 01:24:21', 0),
(3, 5, 'Test Survey', '{"questions":[{"title":"Do you like the campaign?","type":"check-button","options":[{"value":"yes"},{"value":"no"}]},{"title":"Do you like the campaign?","type":"check-button","options":[{"value":"yes"},{"value":"no"}]},{"title":"Do you like the campaign?","type":"check-button","options":[{"value":"yes"},{"value":"no"}]}]}', 0, '2017-06-20 05:17:14', 0),
(4, 5, 'Survey Test', '{"questions":[{"title":"Do you like the campaign","type":"radio-button","options":[{"value":""}]}]}', 0, '2017-06-20 10:25:14', 1),
(5, 5, 'Survey Test', '{"questions":[{"title":"Do you like the campaign","type":"radio-button","options":[{"value":""}]}]}', 0, '2017-06-20 10:25:16', 1),
(6, 5, 'Survey Test', '{"questions":[{"title":"Do you like the campaign","type":"radio-button","options":[{"value":""}]}]}', 0, '2017-06-20 10:25:16', 1),
(7, 5, 'Survey Test', '{"questions":[{"title":"Do you like the campaign","type":"radio-button","options":[{"value":""}]}]}', 0, '2017-06-20 10:25:17', 1),
(8, 5, 'Survey Test', '{"questions":[{"title":"Do you like the campaign","type":"radio-button","options":[{"value":""}]}]}', 0, '2017-06-20 10:25:17', 1),
(9, 5, 'Survey Test', '{"questions":[{"title":"Do you like the campaign","type":"check-button","options":[{"value":"yes"},{"value":"no"}]},{"title":"Do you like the campaign","type":"radio-button","options":[{"value":"yes"},{"value":"no"}]}]}', 1, '2017-06-20 10:25:18', 0),
(10, 5, 'Survey Test', '{"questions":[{"title":"Do you like the campaign","type":"radio-button","options":[{"value":""}]}]}', 0, '2017-06-20 10:25:18', 1),
(11, 5, 'Survey Test', '{"questions":[{"title":"Do you like the campaign","type":"radio-button","options":[{"value":""}]}]}', 0, '2017-06-20 10:25:18', 1),
(12, 5, 'Survey Test', '{"questions":[{"title":"Do you like the campaign","type":"radio-button","options":[{"value":""}]}]}', 0, '2017-06-20 10:25:29', 1),
(13, 5, 'Survey Test', '{"questions":[{"title":"Do you like the campaign","type":"radio-button","options":[{"value":""}]}]}', 0, '2017-06-20 10:25:33', 1),
(14, 3, 'Test 1', '{"questions":[{"title":"Q1","type":"check-button","options":[{"value":"1"},{"value":"2"},{"value":"3"}]},{"title":"Q2","type":"radio-button","options":[{"value":"sdfgs"},{"value":"sdfgdf"},{"value":"dfgsdf"},{"value":"hfdghd"}]}]}', 0, '2017-06-21 08:29:05', 0),
(15, 3, 'Test Survey', '{"questions":[{"title":"adfa","type":"paragraph"},{"title":"dfghfghdfg","type":"check-button","options":[{"value":"dfs"},{"value":"dsfgsdgh"},{"value":"fhfghdfg"}]}]}', 0, '2017-06-23 07:21:08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` text,
  `email` varchar(250) DEFAULT NULL,
  `password` text,
  `role` int(11) DEFAULT NULL,
  `status` text,
  `salt` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `email`, `password`, `role`, `status`, `salt`, `created_by`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'Usman Ashraf', 'usman4qcs@gmail.com', '$2a$12$Uvq5H04RS8PZ495VskOuHOSedaCE9B3Rrr2aqjnFcq0apg16H04lS', 2, 'active', 974605229, NULL, '2017-06-06 00:00:00', '2017-06-06 00:00:00', 0),
(2, 'test2', 'test@test.com', '$2a$12$DXdglGDmKRZ2DsOWifcoceesV/MTSetUhdRRODrGPIaXlTo2jLREm', 1, 'active', NULL, NULL, '2017-06-06 08:30:03', '2017-06-06 08:30:03', 1),
(3, 'Testing', 'info@testing.com', '$2a$12$kPJe5DUCvJthX6XKlr.07eKCkbUh/Eza38oD45xVLhys5Ru5pwWAK', 1, 'active', NULL, NULL, '2017-06-21 08:27:24', '2017-06-21 08:27:24', 0),
(4, 'company', 'company1@gmail.com', '$2a$12$g8sMAZHDaPXQ3vIqVVghiOuJWb3iPFFZ1F9QW9CkrGeXR7K6lvKIm', 1, 'active', 1102960039, NULL, '2017-06-20 06:48:37', '2017-06-20 06:48:37', 0),
(5, 'Test Company', 'testingashtex1@gmail.com', '$2a$12$EMW9Z8BwXtF48Oq2ih0IXuLRoJrgv6upigbDlN8ainxWymn6TgVyi', 1, 'active', 385370810, NULL, '2017-06-21 06:50:10', '2017-06-21 06:50:10', 0),
(31, 'test_company2', 'test_company@gmail.com', '$2a$12$DSBN1Cv1P9DrW3P.W/UXGustZDvnffV3QR1dUQjK5GC4dPiiMh09a', 1, 'active', 1172457791, NULL, '2017-06-23 11:23:02', '2017-06-23 11:23:02', 1),
(40, '0', '0', NULL, 3, 'active', NULL, NULL, '2017-06-23 05:47:26', '2017-06-23 05:47:26', 0),
(41, '0', '0', NULL, 3, 'active', NULL, NULL, '2017-06-23 05:48:31', '2017-06-23 05:48:31', 0),
(42, '0', '0', NULL, 3, 'active', NULL, NULL, '2017-06-23 06:47:49', '2017-06-23 06:47:49', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `user_profile_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `facebook_id` varchar(55) DEFAULT NULL,
  `contact` varchar(15) DEFAULT NULL,
  `basic_info` varchar(55) DEFAULT NULL,
  `work` varchar(55) DEFAULT NULL,
  `education` varchar(55) DEFAULT NULL,
  `lived_place` varchar(55) DEFAULT NULL,
  `image` varchar(55) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`user_profile_id`, `user_id`, `facebook_id`, `contact`, `basic_info`, `work`, `education`, `lived_place`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, '0', '', '', '', '', '', '5f9c219653a0bd56f955b26914e0a3e643194414.jpg', '2017-06-06 00:00:00', '2017-06-06 00:00:00'),
(2, 2, '0', '', 'test2', '', '', '', 'default.png', '2017-06-06 08:30:03', '2017-06-06 08:30:03'),
(3, 3, '0', '', 'farhan.sheikh@gmail.com', '', '', '', 'default.png', '2017-06-21 08:27:24', '2017-06-21 08:27:24'),
(4, 4, '0', '', 'none', '', '', '', '24edafc9d4214f892922dc5c5d81cb818baef592.jpg', '2017-06-20 06:48:37', '2017-06-20 06:48:37'),
(5, 5, '0', '', 'usman4qcs@gmail.com', '', '', '', 'default.png', '2017-06-21 06:50:10', '2017-06-21 06:50:10'),
(17, 31, NULL, '', 'test_company', '', '', '', 'default.png', '2017-06-23 11:23:02', '2017-06-23 11:23:02'),
(26, 40, '323089254791487', NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-23 05:47:26', '2017-06-23 05:47:26'),
(27, 41, '323089254791487', NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-23 05:48:31', '2017-06-23 05:48:31'),
(28, 42, '323089254791487', NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-23 06:47:49', '2017-06-23 06:47:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `fb_check_ins`
--
ALTER TABLE `fb_check_ins`
  ADD PRIMARY KEY (`fb_check_ins_id`);

--
-- Indexes for table `fb_groups`
--
ALTER TABLE `fb_groups`
  ADD PRIMARY KEY (`fb_groups_id`);

--
-- Indexes for table `fb_likes`
--
ALTER TABLE `fb_likes`
  ADD PRIMARY KEY (`fb_likes_id`);

--
-- Indexes for table `invitation`
--
ALTER TABLE `invitation`
  ADD PRIMARY KEY (`invitaion_id`);

--
-- Indexes for table `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD PRIMARY KEY (`questionnaire_id`);

--
-- Indexes for table `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`response_id`);

--
-- Indexes for table `survey_form`
--
ALTER TABLE `survey_form`
  ADD PRIMARY KEY (`survey_form_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`user_profile_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fb_check_ins`
--
ALTER TABLE `fb_check_ins`
  MODIFY `fb_check_ins_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fb_groups`
--
ALTER TABLE `fb_groups`
  MODIFY `fb_groups_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fb_likes`
--
ALTER TABLE `fb_likes`
  MODIFY `fb_likes_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invitation`
--
ALTER TABLE `invitation`
  MODIFY `invitaion_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questionnaire`
--
ALTER TABLE `questionnaire`
  MODIFY `questionnaire_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `response`
--
ALTER TABLE `response`
  MODIFY `response_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `survey_form`
--
ALTER TABLE `survey_form`
  MODIFY `survey_form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `user_profile_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

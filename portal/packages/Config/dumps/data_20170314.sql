-- MySQL dump 10.13  Distrib 5.6.34, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: pos
-- ------------------------------------------------------
-- Server version	5.6.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES UTF8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand`
--

LOCK TABLES `brand` WRITE;
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` VALUES (1,1,1,'VVF ( India ) Ltd.'),(2,1,1,'Cinthol'),(3,1,1,'Vatika'),(4,1,1,'Cream'),(5,1,1,'Face Wash'),(6,1,1,'Shampoo'),(7,1,1,'Soap'),(8,1,1,'Paste'),(9,1,1,'Body Spray'),(10,1,1,'Lotion'),(11,1,1,'Kajal');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `office_no` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,1,'Farhan','Ishaaq','City Marketing Services','',1,'03078841851','','Mandi Bahaudin','Punjab','Pakistan','City Marketing Services Mandi Bahaudin New Share Goods',''),(2,1,'Rafeeq Center','Gojra','RCG','',1,'02334343421','','Gojra','Punjab','Pak','LAhore',''),(3,1,'Rafeeq','Gojra','RCG','',1,'-99082392','','Lahore','Punjab','Pak','Lhaore',''),(4,1,'Ahmed','Ali','a','',1,'34','','sdf','asdasd','sadasd','asdsad','');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturer`
--

LOCK TABLES `manufacturer` WRITE;
/*!40000 ALTER TABLE `manufacturer` DISABLE KEYS */;
INSERT INTO `manufacturer` VALUES (1,1,'Indian');
/*!40000 ALTER TABLE `manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `payments_id` int(11) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`payments_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (0,1,2,14,NULL,'2017-03-08 08:35:30');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `product_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `uom_id` int(11) NOT NULL,
  `tax` varchar(255) NOT NULL,
  PRIMARY KEY (`product_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` VALUES (1,1,'Paste',' ',1,'0'),(2,1,'Oil ',' ',2,'0'),(3,1,'Cream',' ',1,'0'),(4,1,'Face Wash',' ',1,'0'),(5,1,'Shampoo',' ',2,'0'),(6,1,'Body Spray',' ',2,'0'),(7,1,'Lotion',' ',2,'0');
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category_brand`
--

DROP TABLE IF EXISTS `product_category_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category_brand` (
  `product_category_brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  PRIMARY KEY (`product_category_brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category_brand`
--

LOCK TABLES `product_category_brand` WRITE;
/*!40000 ALTER TABLE `product_category_brand` DISABLE KEYS */;
INSERT INTO `product_category_brand` VALUES (3,2,3),(4,3,4),(5,4,5),(6,5,6),(8,1,8),(9,6,9),(10,7,10);
/*!40000 ALTER TABLE `product_category_brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `product_category_brand_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(100) NOT NULL,
  `stock` varchar(255) NOT NULL,
  `initial_cost` varchar(255) NOT NULL,
  `reorder_level` int(11) NOT NULL,
  `description` text NOT NULL,
  `selling_price` varchar(255) NOT NULL,
  `cost_price` varchar(255) NOT NULL,
  `account` varchar(500) DEFAULT NULL,
  `purchase_date` date NOT NULL,
  `sales_description` text,
  `purchase_description` text NOT NULL,
  PRIMARY KEY (`products_id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (3,1,0,3,'Vatika Oil 200ml','vatika oil','400','164',0,' ','170','164',NULL,'2016-12-31',NULL,' '),(4,1,0,3,'Vatika Oil 100ml','vatika oil','0','98',0,' ','100','98',NULL,'2016-12-31',NULL,' '),(5,1,0,3,'Bajaj Almond Oil 100ml','bajaj 100ml','500','123',0,' ','190','98',NULL,'2016-12-31',NULL,' '),(6,1,0,3,'Bajaj Almond Oil 200ml','bajaj 200ml','7430','765',0,' ','8769','6656',NULL,'2016-12-31',NULL,' '),(7,1,0,3,'Bajaj Almond Oil 50ml','bajaj 50ml','765','986',0,' ','8676','8976',NULL,'2016-12-31',NULL,' '),(8,1,0,3,'Livon Oil 100ml','Livon 100ml','57734','231',0,' ','2341','43124',NULL,'2016-12-31',NULL,' '),(9,1,0,3,'Livon Oil 50ml','livon 50ml','243222','1243',0,' ','1443','1234',NULL,'2016-12-30',NULL,' '),(10,1,0,3,'Meera Oil 200ml','Meera','3244','32334',0,' ','2344','324',NULL,'2016-12-30',NULL,' '),(11,1,0,3,'Navratna Oil 200ml','Navratna L','3444','3435',0,' ','190','6656',NULL,'2016-12-31',NULL,' '),(12,1,0,3,'Navratna Oil 100ml','Navratna S','7778','123',0,' ','100','43124',NULL,'2016-12-31',NULL,' '),(13,1,0,3,'Vaseline Hair Tonic 300ml','Vaseline Tonic ','1000','400',0,' ','100','1234',NULL,'2016-12-31',NULL,' '),(14,1,0,3,'Vaseline Hair Tonic 200ml','Vaseline Tonic M','1000','63',0,' ','67','43124',NULL,'2016-12-30',NULL,' '),(15,1,0,3,'Vaseline Hair Tonic 100ml','Vaseline Tonic S','1000','34',0,' ','6765','43124',NULL,'2016-12-30',NULL,' '),(16,1,0,3,'Vatika Coconut Oil 300ml','Vatika Coconut 300ml','7778','98',0,' ','67','98',NULL,'2016-12-30',NULL,' '),(17,1,0,3,'Vatika Coconut Oil 150ml','Vatika Coconut 150ml','1000','123',0,' ','190','6656',NULL,'2016-12-12',NULL,' '),(18,1,0,3,'Vatika Coconut Oil 75ml','Vatika Coconut 75ml','1000','765',0,' ','8769','40',NULL,'2016-11-01',NULL,' '),(19,1,0,3,'Kesh King Oil 120ml','Kesh King Oil','993','98',0,' ','100','6656',NULL,'2016-12-12',NULL,' '),(20,1,0,3,'Dabur Lal Tail 100ml','Dabur Lal 100ml','1000','123',0,' ','67','43124',NULL,'2016-12-12',NULL,' '),(21,1,0,3,'Dabur Lal Tail 200ml','Dabur Lal 200ml','1000','123',0,' ','8769','98',NULL,'2016-12-22',NULL,' '),(22,1,0,3,'Sandhi Sudha Oil','Sandhi Sudha','1000','6500',0,' ','7500','6500',NULL,'2016-01-16',NULL,' '),(23,1,0,3,'Nurament Oil 100ml','Nurament','1000','98',0,' ','190','6656',NULL,'2016-01-16',NULL,' '),(24,1,0,3,'Livon Hair Gain Tonic','Livon Hair Gain','972','123',0,' ','100','43124',NULL,'2016-01-11',NULL,' '),(25,1,0,4,'F&L MultiVitamin 80g','F&L 80g','940','123',0,' ','100','43124',NULL,'2016-01-18',NULL,' '),(26,1,0,3,'F&L MultiVitamin 50g','F&L 50g','1000','123',0,' ','67','6656',NULL,'2017-01-17',NULL,' '),(27,1,0,4,'F&L MultiVitamin 25g','F&L 25g','923','98',0,' ','67','43124',NULL,'2017-12-12',NULL,' '),(28,1,0,4,'F&L Max Fairness 25g','F&L Max 25g','864','123',0,' ','100','6656',NULL,'2017-11-01',NULL,' '),(29,1,0,4,'F&L Max Fairness 50g','F&L Max 50g','985','123',0,' ','67','6656',NULL,'2017-12-12',NULL,' '),(30,1,0,4,'Lifebuoy Hand Wash','LifeBoy Hand Wash','1000','63',0,' ','190','6656',NULL,'2017-01-13',NULL,' '),(31,1,0,4,'Ring Guard 20g','Ring Guard','1000','123',0,' ','67','6656',NULL,'2017-12-12',NULL,' '),(32,1,0,4,'Itch Guard 15g','Itch Guard','1000','765',0,' ','100','43124',NULL,'2017-01-15',NULL,' '),(33,1,0,4,'Moov 25g','moov 25g','1000','45',0,' ','45','7',NULL,'2017-01-09',NULL,' '),(34,1,0,4,'Garnier Wrinkle Lift 18g','wrinkle lift 18g','1000','98',0,' ','67','98',NULL,'2017-01-19',NULL,' '),(35,1,0,4,'Garnier White Complete 18g','garnier WC 18g','976','63',0,' ','190','98',NULL,'2017-01-19',NULL,' '),(36,1,0,4,'Garnier White Complete 40g','garnier WC 40g','1000','98',0,' ','8769','6656',NULL,'2017-12-30',NULL,' '),(37,1,0,4,'Garnier Sun Block','g. sun block','1000','123',0,' ','100','98',NULL,'2017-01-04',NULL,' '),(38,1,0,4,'F&L BB Cream 9g','F&L BB 9g','788','98',0,' ','67','98',NULL,'2017-01-03',NULL,' '),(39,1,0,4,'F&L BB Cream 18g','F&L BB 18g','1000','98',0,' ','67','98',NULL,'2017-01-10',NULL,' '),(40,1,0,4,'F&L BB Cream 40g','F&L BB 40g','1000','98',0,' ','100','40',NULL,'2017-01-11',NULL,' '),(41,1,0,4,'Nixoderm (S)','nixoderm (s)','1000','63',0,' ','8676','40',NULL,'2017-11-01',NULL,' '),(42,1,0,4,'Nixoderm (L)','nixoderm (l)','1000','63',0,' ','67','98',NULL,'2017-01-09',NULL,' '),(43,1,0,4,'Pond\'s White Beauty 25g','Ponds WB 25g','1000','98',0,' ','67','98',NULL,'2017-01-13',NULL,' '),(44,1,0,4,'Pond\'s White Beauty 35g','Ponds WB 35g','1000','98',0,' ','100','98',NULL,'2017-01-07',NULL,' '),(45,1,0,4,'Pond\'s White Beauty 50g','Ponds WB 50g','1000','123',0,' ','67','6656',NULL,'2017-01-22',NULL,' '),(46,1,0,4,'Pond\'s Age Miracle (Day) 50g','ponds age miracle','1000','725',0,' ','760','760',NULL,'2017-01-22',NULL,' '),(47,1,0,4,'Lakme CC 9to5','lakme cc','1000','98',0,' ','67','98',NULL,'2017-01-03',NULL,' '),(48,1,0,4,'Vaseline Petroleum Jelly','petroleum jelly','1000','98',0,' ','67','98',NULL,'2017-12-12',NULL,' '),(49,1,0,4,'Olay 7in1 Anti-Ageing','olay 7in1','1000','98',0,' ','67','98',NULL,'2017-12-12',NULL,' '),(50,1,0,4,'Bio Beauty Heel Repair','Biobeauty heel repair','1000','98',0,' ','67','98',NULL,'2017-12-12',NULL,' '),(51,1,0,4,'Fair & Handsome Cream 50g','F&H 50g ','1000','98',0,' ','100','98',NULL,'2017-01-16',NULL,' '),(52,1,0,4,'Fair & Handsome Cream 80g','F&H 80g ','940','98',0,' ','67','98',NULL,'2017-01-12',NULL,' '),(53,1,0,4,'F&L Blue Lens 50g','F&L BL 50g','1000','98',0,' ','67','98',NULL,'2017-12-30',NULL,' '),(54,1,0,4,'Vi John Hand Care 50g','vi john hand cream','1000','98',0,' ','67','98',NULL,'2017-11-01',NULL,' '),(55,1,0,4,'F&L Anti Marks 50g','F&L AM 50g','997','98',0,' ','67','98',NULL,'2017-01-16',NULL,' '),(56,1,0,4,'Garnier BB 30g','garnier bb 30g','966','98',0,' ','67','6656',NULL,'2017-01-10',NULL,' '),(57,1,0,4,'Garnier BB 18g','garnier bb 18g ','990','98',0,' ','67','98',NULL,'2017-01-31',NULL,' '),(58,1,0,4,'Pond\'s BB 18g','ponds bb','976','123',0,' ','100','98',NULL,'2017-01-11',NULL,' '),(59,1,0,4,'Garnier Fair Miracle 40g','garnier fair','1000','123',0,' ','100','6656',NULL,'2017-11-01',NULL,' '),(60,1,0,4,'Eraser Anti Marks ','eraser','1000','98',0,' ','67','6656',NULL,'2017-11-01',NULL,' '),(61,1,0,4,'Bio Beauty Bleach Cream','Bio Beauty Bleach Cream','1000','98',0,' ','100','40',NULL,'2017-01-16',NULL,' '),(62,1,0,4,'F&L Winter Fairness','f&l winter','1000','98',0,' ','67','43124',NULL,'2017-11-01',NULL,' '),(63,1,0,4,'Vicco Turmeric Cream','vicco','1000','98',0,' ','67','43124',NULL,'2017-12-31',NULL,' '),(64,1,0,4,'Vi-John Feather Touch','vi john feather  ','1000','98',0,' ','190','98',NULL,'2017-01-02',NULL,' '),(65,1,0,4,'Vi John Flower Cream','vi john flower','1000','98',0,' ','100','40',NULL,'2017-01-19',NULL,' '),(66,1,0,4,'F&L Anti Marks 25g','F&L AM 25g','1000','98',0,' ','45','40',NULL,'2017-12-30',NULL,' '),(67,1,0,5,'C&C F/W 50%Extra ','cc wash','1000','98',0,' ','45','98',NULL,'2017-01-12',NULL,' '),(68,1,0,5,'C&C Wash 100ml (M)','cc wash','994','98',0,' ','67','43124',NULL,'2017-01-31',NULL,' '),(69,1,0,5,'C&C Wash 50ml (S)','cc wash','1000','98',0,' ','67','40',NULL,'2017-01-26',NULL,' '),(70,1,0,5,'F&L Pimple Off Wash 50g','f&l pimple','1000','98',0,' ','67','98',NULL,'2017-12-12',NULL,' '),(71,1,0,5,'F&L Face Wash 50g','f&l wash','904','98',0,' ','67','40',NULL,'2017-01-16',NULL,' '),(72,1,0,5,'F&L Face Wash 100g','f&l wash','1000','98',0,' ','67','98',NULL,'2017-01-16',NULL,' '),(73,1,0,5,'Boroplus Face Wash 50ml','boroplus fw','1000','98',0,' ','100','98',NULL,'2017-01-03',NULL,' '),(74,1,0,5,'Garnier AcnoFight+Cream','garnier set','1000','98',0,' ','67','98',NULL,'2017-01-23',NULL,' '),(75,1,0,5,'Garnier Men Power Light F/W','garnier men pw','1000','98',0,' ','67','98',NULL,'2017-01-17',NULL,' '),(76,1,0,5,'Pond\'s W/B F/W 100g','ponds fw','1000','98',0,' ','67','98',NULL,'2017-01-18',NULL,' '),(77,1,0,5,'Pond\'s W/B F/W 50g','ponds fw','1000','98',0,' ','67','98',NULL,'2017-01-26',NULL,' '),(78,1,0,5,'Dove F/W 100g','dove wash','1000','98',0,' ','67','98',NULL,'2017-01-10',NULL,' '),(79,1,0,5,'Dove F/W 50g','dove wash','1000','123',0,' ','67','98',NULL,'2017-01-17',NULL,' '),(80,1,0,5,'C&C Wash Daily Scrub 80g','cc scrub','1000','98',0,' ','67','98',NULL,'2017-12-30',NULL,' '),(81,1,0,5,'F&L Men Oil Control F/W','f&l men wash','7778','123',0,' ','67','98',NULL,'2017-01-10',NULL,' '),(82,1,0,5,'Garnier Men Double Action F/W','garnier men da','1000','98',0,' ','67','98',NULL,'2017-01-10',NULL,' '),(83,1,0,5,'Pond\'s Men F/W 100g','ponds fw','1000','98',0,' ','100','98',NULL,'2017-01-03',NULL,' '),(84,1,0,5,'Garnier Men Oil Clear F/W','garnier men oc','1000','123',0,' ','67','98',NULL,'2017-01-05',NULL,' '),(85,1,0,5,'Garnier Men Acno Fight F/W','garnier men af','1000','98',0,' ','67','6656',NULL,'2017-12-12',NULL,' '),(86,1,0,5,'Fair & Handsome F/W ','f&h wash ','999','98',0,' ','67','6656',NULL,'2017-11-01',NULL,' '),(87,1,0,5,'Garnier W/C 3in1 F/W 50g','garnier wc 3in1','1000','98',0,' ','67','98',NULL,'2017-01-16',NULL,' '),(88,1,0,5,'Garnier W/C 3in1 F/W 100g','garnier wc 3in1','1000','98',0,' ','67','40',NULL,'2017-01-26',NULL,' '),(89,1,0,5,'Olay F/W 50g','olay 50g','1000','98',0,' ','67','98',NULL,'2017-01-20',NULL,' '),(90,1,0,5,'Himalaya F/W 50g','himalaya ','850','98',0,' ','67','98',NULL,'2017-12-31',NULL,' '),(91,1,0,5,'Himalaya F/W 100g','himalaya ','904','98',0,' ','100','98',NULL,'2017-12-12',NULL,' '),(92,1,0,6,'Nyle Shampoo 400ml','nyle','1000','98',0,' ','100','98',NULL,'2017-01-22',NULL,' '),(93,1,0,6,'Nyle Shampoo 180ml','nyle','1000','98',0,' ','100','98',NULL,'2017-01-21',NULL,' '),(94,1,0,6,'Garnier Fructis 2in1 175ml','garnier 2in1','1000','98',0,' ','67','98',NULL,'2017-01-14',NULL,' '),(95,1,0,6,'Clinic Plus 340ml','clinic +','1000','98',0,' ','67','98',NULL,'2017-01-22',NULL,' '),(96,1,0,6,'Clinic Plus 175ml','clinic +','1000','98',0,' ','67','98',NULL,'2017-01-09',NULL,' '),(97,1,0,6,'Kesh King Shampoo 120ml','Kesh King shampoo','1000','98',0,' ','67','98',NULL,'2017-01-09',NULL,' '),(98,1,0,6,'Comex Shampoo','comex','1000','98',0,' ','100','98',NULL,'2017-11-01',NULL,' '),(100,1,0,6,'Garnier Fructics 340ml','garnier fructis','1000','98',0,' ','100','98',NULL,'2017-11-01',NULL,' '),(101,1,0,6,'Denon Shampoo 100ml','denon shampoo','1000','98',0,' ','100','98',NULL,'2017-01-17',NULL,' '),(102,1,0,6,'Loreal Shampoo+Conditioner ','loreal set','1000','98',0,' ','100','98',NULL,'2017-01-17',NULL,' '),(103,1,0,6,'Beuton Kala Shampoo 200ml','beuton kala','1000','98',0,' ','100','98',NULL,'2017-01-17',NULL,' '),(104,1,0,6,'Vatika Shampoo','vatika shampoo','1000','98',0,' ','100','98',NULL,'2017-01-17',NULL,' '),(105,1,0,6,'Dabur Almond Shampoo','almond shampoo','1000','98',0,' ','100','98',NULL,'2017-01-17',NULL,' '),(106,1,0,8,'Dabur Red Paste 100g','dabur red','1000','98',0,' ','67','98',NULL,'2017-01-18',NULL,' '),(107,1,0,8,'Dabur Red Paste 200g','dabur red','1000','98',0,' ','67','98',NULL,'2017-01-18',NULL,' '),(108,1,0,8,'Colgate Visible White 100g','colgate','1000','98',0,' ','67','98',NULL,'2017-01-18',NULL,' '),(109,1,0,8,'Vicco Vajradanti 100g','vicco vajradanti','1000','98',0,' ','67','98',NULL,'2017-01-18',NULL,' '),(110,1,0,9,'Engage B/S Women 150ml','Engage','1000','98',0,' ','67','40',NULL,'2017-01-10',NULL,' '),(111,1,0,9,'Engage B/S Men 150ml','engage','1000','98',0,' ','67','40',NULL,'2017-01-10',NULL,' '),(112,1,0,9,'Zatak Body Spray 150ml','zatak','1000','98',0,' ','100','98',NULL,'2017-01-09',NULL,' '),(113,1,0,9,'HE Perfume','he','1000','98',0,' ','100','98',NULL,'2017-01-09',NULL,' '),(114,1,0,9,'Fogg B/S 120ml','fog','1000','98',0,' ','100','98',NULL,'2017-01-09',NULL,' '),(115,1,0,9,'18+ Body Spray','18+','1000','98',0,' ','100','98',NULL,'2017-01-16',NULL,' '),(116,1,0,9,'Layer\'s WottaGirl','layer\'s wottagirl','1000','98',0,' ','100','98',NULL,'2017-01-16',NULL,' '),(117,1,0,9,'Wild Stone Perfume 120ml','wild stone','1000','98',0,' ','100','98',NULL,'2017-01-16',NULL,' '),(118,1,0,9,'Set Wet Body Spray','set wet','998','98',0,' ','100','98',NULL,'2017-01-16',NULL,' '),(119,1,0,9,'Cinthol Body Spray','cinthol','1000','98',0,' ','100','98',NULL,'2017-01-16',NULL,' '),(120,1,0,9,'Axe Signature','axe','1000','98',0,' ','100','98',NULL,'2017-01-16',NULL,' '),(121,1,0,9,'Fogg Scent','fog scent','1000','98',0,' ','100','98',NULL,'2017-01-16',NULL,' '),(122,1,0,9,'Nufeel Facial Spray','nufeel','1000','98',0,' ','100','98',NULL,'2017-01-16',NULL,' '),(123,1,0,4,'Garnier Eye Roll On 15ml','eye roll on','1000','98',0,' ','100','98',NULL,'2017-01-16',NULL,' '),(124,1,0,10,'Vaseline Lotion 200ml','vaseline lotion','1000','98',0,' ','67','98',NULL,'2017-01-23',NULL,' '),(125,1,0,10,'Vaseline Lotion 100ml','vaseline lotion','1000','98',0,' ','67','98',NULL,'2017-01-23',NULL,' '),(126,1,0,10,'Dove Lotion','dove lotion','1000','98',0,' ','67','98',NULL,'2017-01-23',NULL,' '),(127,1,0,10,'Boroplus Lotion','boroplus Lotion','1000','98',0,' ','67','98',NULL,'2017-01-23',NULL,' ');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order`
--

DROP TABLE IF EXISTS `sales_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order` (
  `sales_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `sales_order_number` varchar(100) NOT NULL,
  `sales_order_date` datetime NOT NULL,
  `shipment_date` datetime NOT NULL,
  `delivery_method` varchar(100) NOT NULL,
  `sales_person` varchar(255) NOT NULL,
  `total_amount` varchar(100) NOT NULL,
  `shipping_charges` varchar(100) NOT NULL,
  `adjustment_amount` varchar(100) NOT NULL,
  `notes` text NOT NULL,
  `creation_date` datetime NOT NULL,
  `amount_paid` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sales_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order`
--

LOCK TABLES `sales_order` WRITE;
/*!40000 ALTER TABLE `sales_order` DISABLE KEYS */;
INSERT INTO `sales_order` VALUES (1,1,1,0,'SO-00000001','2016-11-27 20:16:00','2016-11-27 20:16:00','1','Abrar Munir','450.00','','',' ','2016-11-27 06:38:15','400','partial'),(2,1,1,0,'SO-00000002','2016-11-27 20:16:00','2016-11-27 20:16:00','1','Abrar Munir','516.00','','',' ','2016-11-27 07:47:15','500','partial'),(3,1,1,0,'SO-00000003','2017-01-01 20:17:00','2017-01-01 20:17:00','1','Abrar Munir','18615.00','','',' ','2017-01-01 14:59:45','18000','partial'),(4,1,1,0,'SO-00000004','2017-01-01 20:17:00','2017-01-01 20:17:00','1','Abrar Munir','1800.00','','',' ','2017-01-01 15:05:22','0','pending'),(5,1,1,0,'SO-00000005','2017-01-01 20:17:00','2017-01-01 20:17:00','1','Abrar Munir','2278.00','','',' ','2017-01-01 21:50:42','0','pending'),(6,1,1,0,'SO-00000006','2017-01-13 20:17:00','2017-01-13 20:17:00','1','Abrar Munir','21752.00','','',' ','2017-01-13 01:04:21','21750','partial'),(7,1,1,0,'SO-00000007','2017-01-16 20:17:00','2017-01-16 20:17:00','1','Abrar Munir','7455.00','150','2305','','2017-01-16 12:23:39','0','pending'),(8,1,1,1,'SO-00000008','2017-01-16 20:17:00','2017-01-16 20:17:00','1','Abrar Munir','35808.00','','',' ','2017-01-16 12:32:55','35000','partial'),(9,1,1,0,'SO-00000009','2017-01-17 20:17:00','2017-01-17 20:17:00','1','Abrar Munir','22947.00','','',' ','2017-01-17 09:37:50','0','pending'),(10,1,1,1,'SO-00000010','2017-01-17 20:17:00','2017-01-17 20:17:00','1','Abrar Munir','14160.00','','',' ','2017-01-17 09:40:18','0','pending'),(11,1,1,0,'SO-00000011','2017-01-17 20:17:00','2017-01-17 20:17:00','1','Abrar Munir','468.00','','',' ','2017-01-17 10:29:32','0','pending'),(12,1,1,0,'SO-00000012','2017-01-17 20:17:00','2017-01-17 20:17:00','1','Abrar Munir','27504.00','','',' ','2017-01-18 01:03:59','30000','paid'),(13,1,1,0,'SO-00000013','2017-03-08 20:17:00','2017-03-08 20:17:00','1','Abrar Munir','3988.00','','',' ','2017-03-08 08:25:05',NULL,'pending'),(14,1,1,2,'SO-00000014','2017-03-08 20:17:00','2017-03-08 20:17:00','1','Abrar Munir','34500.00','','',' ','2017-03-08 08:50:49',NULL,'pending');
/*!40000 ALTER TABLE `sales_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_order_details`
--

DROP TABLE IF EXISTS `sales_order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_order_details` (
  `sales_order_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_order_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `discount_method` varchar(100) NOT NULL,
  `discount_amount` varchar(100) NOT NULL,
  `tax` varchar(100) NOT NULL,
  `amount` varchar(255) NOT NULL,
  PRIMARY KEY (`sales_order_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_order_details`
--

LOCK TABLES `sales_order_details` WRITE;
/*!40000 ALTER TABLE `sales_order_details` DISABLE KEYS */;
INSERT INTO `sales_order_details` VALUES (34,14,6,300,'115','pkr','','0','34500.00');
/*!40000 ALTER TABLE `sales_order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `logo` text,
  `tag_line` varchar(500) NOT NULL,
  `office_no` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` VALUES (1,1,'Abdullah Cometics','Shop # 4, Seth Plaza, Shah Alam Market, Lahore',NULL,'It is better to be worst','0423-7658057 / 03348134235','');
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `office_no` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (1,1,'Rashid','Minhas','Rashid & Sons','',1,'123456789','','Lahore','Punjab','Pakistan','Office # 123','');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax`
--

DROP TABLE IF EXISTS `tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax` (
  `tax_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`tax_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax`
--

LOCK TABLES `tax` WRITE;
/*!40000 ALTER TABLE `tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uom`
--

DROP TABLE IF EXISTS `uom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uom` (
  `uom_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY (`uom_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uom`
--

LOCK TABLES `uom` WRITE;
/*!40000 ALTER TABLE `uom` DISABLE KEYS */;
INSERT INTO `uom` VALUES (1,1,'Grams'),(2,1,'ml');
/*!40000 ALTER TABLE `uom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `users_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Abrar Munir','ab.ibrar27@gmail.com','$2a$12$fVvW6v2jKF/6D/3u.9SDCeYnpFkT1sOOxj2/TuLvbQvFv3gdZ59Hu','admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-14  9:28:37

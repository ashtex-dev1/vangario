<?php

namespace Config;

use Config\Central;
use Config\Constants;
use Config\URLs;

abstract class RSBase implements \Rocketsled\Runnable
{

    //--private members
    protected $template;
    protected $central;
    protected $profile = 'pos';
    protected $esc;
    private $allowed_pages = array(
	Constants::USER_ROLE_ADMIN => array(
	    URLs::COMPANIES,
	    URLs::QUESTIONS,
	    URLs::QUESTIONNAIRE,
	),
	Constants::USER_ROLE_COMPANY => array(
	    URLs::SURVEY,
	    URLs::REPORTS,
	),
	'general' => array(
	    URLs::LOGIN,
	    URLs::PROFILE,
	    URLs::DASHBOARD,
	    URLs::PAGE_NOT_FOUND,
	),
    );
    private $info = array();
    private $redirect;

    //--constructor
    public function __construct()
    {
	try
	{
	    $this->session_time();

	    // Load DB credentials and supported classes
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    //render function
    public function render( $display = 1 )
    {
	try
	{
	    try
	    {
		if (
			$this->central->check_user_login_status( $this->profile ) &&
			$this->page_allowed()
		)
		{
		    $this->prepare_template();
		    $this->update_main_contents();

		    if ( $display && !is_null( $this->template ) )
		    {
			$this->template->remove( '.static-entry' );
			$this->central->render( $this->template );
		    }
		}
		else
		{
		    $this->__redirect( URLs::LOGIN );
		}
	    }
	    catch ( Exception $e )
	    {
		$this->__redirect( URLs::LOGIN );
	    }
	}
	catch ( Exception $e )
	{
	    $this->__redirect( URLs::LOGIN );
	}
    }

    public function page_allowed()
    {
	if (
		!in_array( $_GET[ 'active' ], $this->allowed_pages[ Central::get_user_role() ] ) &&
		!in_array( $_GET[ 'active' ], $this->allowed_pages[ 'general' ] )
	)
	{
	    $this->__redirect( URLs::PAGE_NOT_FOUND );
	}

	return TRUE;
    }

    public function prepare_template()
    {
	if ( is_null( $this->template ) )
	    return;

	$this->template->setValue( '.user-name', ucwords( $_SESSION[ 'user' ][ 'name' ] ), 1 );
	$this->template->setValue( '#user-registration-date', date( "M, Y", strtotime( $_SESSION[ 'user' ][ 'registration' ] ) ) );
	$this->template->setValue( 'a.signout@href', URLs::LOGIN . $this->central->encode_url_param( URLs::LOGOUT ) );
	$this->template->setValue( 'a.profile@href', URLs::PROFILE );
	$this->template->setValue( 'li.dashboard/a@href', URLs::DASHBOARD );
	$this->template->setValue( 'li.questionnaire/a@href', URLs::QUESTIONS );
	$this->template->setValue( 'li.view-company/a@href', URLs::COMPANIES );
	$this->template->setValue( 'li.add-company/a@href', URLs::COMPANIES . $this->central->encode_url_param( URLs::ADD_VIEW ) );
	$this->template->setValue( 'li.view-survey/a@href', URLs::SURVEY );
	$this->template->setValue( 'li.add-survey/a@href', URLs::SURVEY . $this->central->encode_url_param( URLs::ADD_VIEW ) );
	$this->template->setValue( 'li.general-report/a@href', URLs::REPORTS );
	$this->set_active_menu();
	$this->setup_menu_as_per_role();
    }

    public function setup_menu_as_per_role()
    {
	switch ( Central::get_user_role() )
	{
	    case Constants::USER_ROLE_ADMIN:
		$this->template->remove( 'li.survey' );
		$this->template->remove( 'li.reports' );
		break;
	    case Constants::USER_ROLE_COMPANY:
		$this->template->remove( 'li.questionnaire' );
		$this->template->remove( 'li.companies' );
		break;
	    default:
		Central::display_error_page();
		break;
	}
    }

    public function set_active_menu()
    {
	switch ( $_GET[ 'active' ] )
	{
	    case URLs::PROFILE:
		$this->template->query( 'li.user-menu' )->item( 0 )->setAttribute( 'class', 'active dropdown user user-menu' );
		break;
	    case URLs::COMPANIES:
		$this->template->query( 'li.companies' )->item( 0 )->setAttribute( 'class', 'active dropdown user companies' );
		break;
	    case URLs::QUESTIONS:
		$this->template->query( 'li.questionnaire' )->item( 0 )->setAttribute( 'class', 'active dropdown user questionnaire' );
		break;
	    case URLs::SURVEY:
		$this->template->query( 'li.survey' )->item( 0 )->setAttribute( 'class', 'active dropdown user servey' );
		break;
	    case URLs::REPORTS:
		$this->template->query( 'li.reports' )->item( 0 )->setAttribute( 'class', 'active dropdown user reports' );
		break;
	    default:
		$this->template->query( 'li.dashboard' )->item( 0 )->setAttribute( 'class', 'active dashboard' );
		break;
	}
    }

    protected function print_messages( $data = array() )
    {
	if ( $data )
	{
	    if (
		    ( isset( $data[ "success" ] ) && $data[ "success" ] ) ||
		    ( isset( $data[ "error" ] ) || $data[ "error" ] )
	    )
	    {
		$this->template->setValue( ".alert_close/span", "X" );
		if ( isset( $data[ "success" ] ) && $data[ "success" ] )
		{
		    $this->template->remove( "#messages/.alert-danger" );
		    $this->template->setValue( "#messages/.alert-success/span", $data[ "message" ] );
		}
		else if ( isset( $data[ "error" ] ) && $data[ "error" ] )
		    $this->template->remove( "#messages/.alert-success" );
		else
		    $this->template->remove( "#messages" );

		unset( $_SESSION[ "success" ] );
		unset( $_SESSION[ "error" ] );
	    }
	    else
	    {
		$this->template->remove( "#messages" );
	    }
	}
	else
	{
	    $this->template->remove( "#messages" );
	}
    }

    public function get_template()
    {
	return $this->template;
    }

    public function run( $verbose = true )
    {
	try
	{
	    if ( $verbose )
	    {
		$this->render( true );
	    }
	}
	catch ( Exception $e )
	{
	    if ( $verbose )
	    {
		// echo $e->getMessage();
	    }
	    else
	    {
		throw $e; // for the Murphy
	    }
	}
    }

    public function session_time()
    {
	@ini_set( 'session.gc_maxlifetime', '43200' );
	@session_set_cookie_params( '43200' );
	@session_start();
    }

    protected function __redirect( $url )
    {
	?><html><body><script type="text/javascript">top.location.href = '<?php echo $url; ?>';</script></body></html><?php
	die();
    }

    // abstract function
    abstract protected function update_main_contents();
}
?>

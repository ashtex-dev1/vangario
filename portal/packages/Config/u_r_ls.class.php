<?php

/**
 * @Package  Config\\ URLs
 * @author Bugs - Ashtex
 * The constant define class for the package
 */

namespace Config;

class URLs
{

    const HOME = "/home";
    const LOGIN = "/login";
    const REGISTER = "/register";
    const FORGET_PASSWORD = "/forget-password";
    const RESET_PASSWORD = "/reset-password";
    const DASHBOARD = "/dashboard";
    const COMPANIES = "/companies";
    const QUESTIONS = "/questions";
    const SURVEY = "/survey";
    const REPORTS = "/reports";
    const QUESTIONNAIRE = "/questionnaire";
    const GENERATE_EXCEL = "/generate-excel";
    const PROFILE = "/profile";
    const LOGOUT = "/logout-yes";
    const PAGE_NOT_FOUND = "/error-404";
    const ADD_VIEW = "/view_type-add";
    const DETAIL_VIEW = "/view_type-detail";
    const ID = "/id-%ID%";
    const METHOD = "/method-%ID%";
    const ACTION = "/action";

}

<?php

use Config\Constants;
use Config\Central;
use Config\URLs;

class Statistics
{

    public function get_columns( $profile, $survey_id )
    {
	try
	{
	    $columns = array( '#', 'name', 'age', 'gender', 'home town', 'relationship status',
		array(
		    'title' => 'education',
		    'data' => '',
		    'table' => 'user_education'
		),
		array(
		    'title' => 'interests',
		    'data' => '',
		    'table' => 'user_interests'
		),
	    );
	    $questions = Plusql::from( $profile )->question->question_options->select( '*, question.title as qtitle' )->where( "survey_id = {$survey_id} AND question.deleted <> 1" )->run()->question;
	    $count = 0;
	    foreach ( $questions as $question )
	    {
		$columns[] = array(
		    'title' => "Q # " . ( $count + 1 ),
		    'data' => $question,
		    'table' => 'question',
		);
		$count++;
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $columns;
    }

    public function get_respondents( $profile, $survey_id )
    {
	try
	{
	    $return = array();
	    $sql = "
		    SELECT 
			u.user_id AS uid, u.fb_id, ud.name, ud.age, ud.gender, 
			ud.home_town AS 'home town', ud.relationship_status AS 'relationship status'
		    FROM 
			    user AS u
			INNER JOIN
			    user_details AS ud
			ON
			    u.user_id = ud.user_id
			INNER JOIN
			    survey_response AS sr
			ON
			    ud.user_id = sr.user_id
			" . self::get_filters_table() . "
		    WHERE
			sr.survey_id = {$survey_id} " . self::get_filters() . "
		";

	    $return = Plusql::against( $profile )->run( $sql );
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $return;
    }

    public function get_respondents_education( $profile, $survey_id, $institutes )
    {
	try
	{
	    $sql = "
	      SELECT
	      u.user_id, ue.institute, ue.type
	      FROM
	      user AS u
	      INNER JOIN
	      user_details AS ud
	      ON
	      u.user_id = ud.user_id
	      INNER JOIN
	      user_education AS ue
	      ON
	      u.user_id = ue.user_id
	      INNER JOIN
	      survey_response AS sr
	      ON
	      ud.user_id = sr.user_id
	      WHERE
	      sr.survey_id = {$survey_id} AND ue.institute IN ( {$institutes} )
	      ";
	    $return = Plusql::against( $profile )->run( $sql );
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $return;
    }

    public function get_respondents_locations( $profile, $survey_id, $locations )
    {
	try
	{
	    $sql = "
		    SELECT 
			u.user_id, ud.home_town
		    FROM 
			    user AS u
			INNER JOIN
			    user_details AS ud
			ON
			    u.user_id = ud.user_id
			INNER JOIN
			    survey_response AS sr
			ON
			    ud.user_id = sr.user_id
			" . self::get_filters_table() . "
		    WHERE
			sr.survey_id = {$survey_id} AND ud.home_town IN ( {$locations} ) " . self::get_filters() . "
		";

	    $return = Plusql::against( $profile )->run( $sql );
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $return;
    }

    public function get_respondents_relationships( $profile, $survey_id, $relationships )
    {
	try
	{
	    $sql = "
		    SELECT 
			u.user_id, ud.relationship_status
		    FROM 
			    user AS u
			INNER JOIN
			    user_details AS ud
			ON
			    u.user_id = ud.user_id
			INNER JOIN
			    survey_response AS sr
			ON
			    ud.user_id = sr.user_id
			" . self::get_filters_table() . "
		    WHERE
			sr.survey_id = {$survey_id} AND ud.relationship_status IN ( {$relationships} ) " . self::get_filters() . "
		";

	    $return = Plusql::against( $profile )->run( $sql );
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $return;
    }

    public function get_respondents_answers_count( $profile, $question_options_id )
    {
	try
	{
	    $total = 0;
	    $sql = "
		    SELECT 
			COUNT(*) AS total
		    FROM 
			    user AS u
			INNER JOIN
			    user_details AS ud
			ON
			    u.user_id = ud.user_id
			INNER JOIN
			    survey_response AS sr
			ON
			    ud.user_id = sr.user_id
			INNER JOIN
			    survey_response_answers AS sra
			ON
			    sr.survey_response_id = sra.survey_response_id
			" . self::get_filters_table() . "
		    WHERE
			sra.question_options_id = {$question_options_id} " . self::get_filters() . "
		    LIMIT 0, 1
		";

	    $result = Plusql::against( $profile )->run( $sql );
	    while ( $row = $result->nextRow() )
	    {
		$total = $row[ 'total' ];
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $total;
    }

    public function get_respondents_age( $profile, $survey_id, $where )
    {
	try
	{
	    $sql = "
		    SELECT 
			DISTINCT(u.user_id), ud.age, ud.gender
		    FROM 
			    user AS u
			INNER JOIN
			    user_details AS ud
			ON
			    u.user_id = ud.user_id
			INNER JOIN
			    survey_response AS sr
			ON
			    ud.user_id = sr.user_id
			" . self::get_filters_table() . "
		    WHERE
			sr.survey_id = {$survey_id} AND {$where} " . self::get_filters() . "
		";
	    $return = Plusql::against( $profile )->run( $sql );
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $return;
    }

    public function get_user_education( $profile, $user_id, $xls = false )
    {
	try
	{
	    $return = "";
	    $types = array();
	    $user_education = Plusql::from( $profile )->user_education->select( "*" )->where( "user_id = {$user_id}" )->run()->user_education;
	    foreach ( $user_education as $education )
	    {
		$return .= htmlentities( Central::string_formating(ucwords( $education->institute )) ) . ( $xls ? "\n" : " <br />" );
	    }

	    $return = rtrim( $return, "\n" );
	    $return = rtrim( $return, "<br />" );
	}
	catch ( Exception $ex )
	{
	    $return = " - ";
	}

	return $return;
    }

    public function get_user_interests( $profile, $user_id, $survey_id, $xls = false )
    {
	try
	{
	    $return = "";

	    $sql = "
		    SELECT 
			*
		    FROM 
			user_interests AS ui 
			INNER JOIN fb_pages AS pg ON ui.fb_pages_id = pg.fb_pages_id
			INNER JOIN publish_category AS pc ON pg.fb_pages_id = pc.fb_pages_id
			INNER JOIN publish AS p ON pc.publish_id = p.publish_id
		    WHERE 
			ui.user_id = {$user_id} AND p.survey_id = {$survey_id}
		";

	    $results = Plusql::against( $profile )->run( $sql );

	    while ( $row = $results->nextRow() )
	    {
		$return .= htmlentities( Central::string_formating(ucwords( $row[ 'title' ] )) ) . ( $xls ? "\n" : " <br />" );
	    }

	    if ( strlen( $return ) )
	    {
		$return = rtrim( $return, "\n" );
		$return = rtrim( $return, "<br />" );
	    }
	    else
		$return = " - ";
	}
	catch ( Exception $ex )
	{
	    $return = " - ";
	}

	return $return;
    }

    public function get_user_answers( $profile, $survey_id, $user_id, $question_id, $xls = false )
    {
	try
	{
	    $return = "";
	    $user_answers = Plusql::from( $profile )->survey_response->survey_response_answers->select( "*" )->where( "survey_response.survey_id = {$survey_id} AND survey_response.user_id = {$user_id} AND question_id = {$question_id}" )->run()->survey_response_answers;
	    foreach ( $user_answers as $user_answer )
	    {
		$return .= htmlentities( Central::string_formating(ucwords( $user_answer->answer )) ) . ( $xls ? "\n" : " <br />" );
	    }

	    $return = rtrim( $return, "\n" );
	    $return = rtrim( $return, "<br />" );
	}
	catch ( Exception $ex )
	{
	    $return = " - ";
	}

	return $return;
    }

    public static function get_filters_table()
    {
	try
	{
	    $join = "";
	    $education = Central::getargs( "education", $_POST, $corrupt, 0 );
	    if ( $education )
		$join .= " INNER JOIN user_education ON u.user_id = ue.user_id";
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $join;
    }

    public static function get_filters()
    {
	try
	{
	    $where = "";
	    $age = Central::getargs( "age", $_POST, $corrupt, 0 );
	    if ( $age )
	    {
		if ( strpos( $age, "-" ) )
		{
		    list($min, $max) = explode( "-", $age );
		    $where .= " AND ud.age BEtWEEN {$min} AND {$max}";
		}
		else if ( strpos( $age, "+" ) )
		    $where .= " AND ud.age >= " . explode( "+", $age )[ 0 ];
	    }

	    $gender = Central::getargs( "gender", $_POST, $corrupt, 0 );
	    if ( $gender )
		$where .= " AND ud.gender = '{$gender}'";

	    $location = Central::getargs( "location", $_POST, $corrupt, 0 );
	    if ( $location )
		$where .= " AND ud.home_town = '{$location}'";

	    $education = Central::getargs( "education", $_POST, $corrupt, 0 );
	    if ( $education )
		$where .= " AND ue.institute = '{$education}'";

	    $relationship = Central::getargs( "relationship", $_POST, $corrupt, 0 );
	    if ( $relationship )
		$where .= " AND ud.relationship_status = '{$relationship}'";
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $where;
    }

}

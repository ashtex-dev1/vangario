<?php

define( 'FACEBOOK_APP_ID', '2000798346815610' );
define( 'FACEBOOK_APP_SECRET', 'fb753e7d974ef9b293572baf90a68596' );

// -------
require_once(__DIR__ . '/../../Facebook/autoload.php');

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\GraphPage;

FacebookSession::setDefaultApplication( FACEBOOK_APP_ID, FACEBOOK_APP_SECRET );

function prepare_pages_list()
{
    if ( isset( $_POST[ 'id' ] ) )
    {
	$pages = array();
	if ( isset( $_POST[ 'work' ] ) )
	{
	    foreach ( $_POST[ 'work' ] as $item )
	    {
		$pages[] = array(
		    'id' => $item[ 'employer' ][ 'id' ],
		    'title' => $item[ 'employer' ][ 'name' ],
		);
	    }
	}

	if ( isset( $_POST[ 'likes' ] ) )
	{
	    $likes = $_POST[ 'likes' ];
	    if ( isset( $likes[ 'data' ] ) )
		$likes = $likes[ 'data' ];

	    foreach ( $likes as $item )
	    {
		if ( isset( $item[ 'data' ] ) )
		    $item = $item[ 'data' ];

		if ( isset( $item[ 'id' ] ) && $item[ 'name' ] )
		{
		    $pages[] = array(
			'id' => $item[ 'id' ],
			'title' => $item[ 'name' ],
		    );
		}
	    }
	}

	return $pages;
    }
}

function get_facebook_id()
{
    try
    {
	$user_facebook_id = 0;
	$user = new Facebook\FacebookJavaScriptLoginHelper();
	$user_facebook_id = $user->getUserId();
    }
    catch ( Exception $ex )
    {
	$user_facebook_id = 0;
    }
    return $user_facebook_id;
}

function get_facebook_page_details( $pages )
{
    try
    {
	$page_details = array();
	foreach ( $pages as $k => $page )
	{
	    try
	    {
		$session = FacebookSession::newAppSession( FACEBOOK_APP_ID, FACEBOOK_APP_SECRET );
		$request = new FacebookRequest( $session, "GET", "/{$page[ 'id' ]}/?fields=category,category_list" );
		$page_details[ $k ] = $request->execute()->getGraphObject()->AsArray();
		$page_details[ $k ][ 'title' ] = $page[ 'title' ];
		$page_details[ $k ][ 'page_id' ] = $page[ 'id' ];
		echo "Page Fetched: {$page[ 'title' ]} ( {$page[ 'id' ]} )" . PHP_EOL;
		sleep( rand( 0, 3 ) );
	    }
	    catch ( FacebookRequestException $e )
	    {
		$page_details[ $k ] = array(
		    'id' => 0,
		    'category' => 'Uncategorized'
		);
		// echo '*******'. $page[ 'id' ]. '********';
		$page_details[ $k ][ 'title' ] = $page[ 'title' ];
		$page_details[ $k ][ 'page_id' ] = $page[ 'id' ];
		echo "FacebookRequestException: {$e->getMessage()}" . PHP_EOL;
		// do nothing
	    }
	    catch ( Exception $ex )
	    {
		$page_details[ $k ] = array(
		    'id' => 0,
		    'category' => 'Uncategorized'
		);
		$page_details[ $k ][ 'title' ] = $page[ 'title' ];
		$page_details[ $k ][ 'page_id' ] = $page[ 'id' ];
		echo "Exception: {$e->getMessage()}" . PHP_EOL;
		// do nothing
	    }
	}
    }
    catch ( Exception $ex )
    {
	echo "Exception: {$e->getMessage()}" . PHP_EOL;
	$page_details = array();
    }

    return $page_details;
}

function get_facebook_page_id( $artist )
{
    try
    {
	$page_id = 0;
	$session = FacebookSession::newAppSession( FACEBOOK_APP_ID, FACEBOOK_APP_SECRET );
	$fb_obj = new Facebook\FacebookRequest( $session, 'GET', '/search?q=' . $artist . '&type=page&limit=100' );
	$pages = $fb_obj->execute()->getGraphObject( GraphPage::className() )->getPropertyAsArray( "data", GraphPage::className() );
	foreach ( $pages as $page )
	{
	    if (
		    strcmp( strtolower( $page->getName() ), slug_decode( $artist ) ) === 0 &&
		    strcmp( strtolower( $page->getCategory() ), "musician/band" ) === 0
	    )
	    {
		$request = new FacebookRequest( $session, 'GET', '/' . $page->getId() );
		$page_details = $request->execute()->getGraphObject()->AsArray();

		if ( $page_details[ "is_community_page" ] )
		    continue;
		else
		{
		    $page_id = $page->getId();
		    break;
		}
	    }
	}
    }
    catch ( FacebookRequestException $e )
    {
	$page_id = 0;
    }
    catch ( Exception $ex )
    {
	$page_id = 0;
    }

    return $page_id;
}

function slug_decode( $input )
{
    $unwanted_array = array(
	'Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A',
	'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E',
	'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
	'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y',
	'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a',
	'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i',
	'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
	'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'ü' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b',
	'ÿ' => 'y'
    );
    return strtolower( strtr( $input, $unwanted_array ) );
}

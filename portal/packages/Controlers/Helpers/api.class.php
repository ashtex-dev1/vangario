<?php

@header( "Access-Control-Allow-Origin: *" );

use Config\Constants;
use Config\Central;
use Config\URLs;

class Api implements \RocketSled\Runnable
{

    // actions
    const USER_UPDATE = 1;
    const SURVEY_DETAILS = 2;
    const SURVEY_RESPONSE = 3;
    const USER_DETAILS = 4;
    const SURVEYS = 5;
    const FILLED_SURVEYS = 6;
    const PUBLIC_URL = 7;
    // methods
    const SUBMITTED = 1;
    const ABORTED = 0;
    const GET = 1;
    const UPDATE = 2;

    // private members
    private $profile = "bv";
    private $central;
    private $fb_helper;

    // constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	    $this->pheanstalk_client = new Pheanstalk_Pheanstalk( Constants::BEANSTALKD_HOST, Constants::BEANSTALKD_PORT );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function __call( $closure, $argv )
    {
	$escape = Plusql::escape( $this->profile );
	return $escape( $argv[ 0 ] );
    }

    public function run()
    {
	try
	{
	    $data = $this->update_main_contents();
	    die( json_encode( $data ) );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{

	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $method = $this->central->getargs( 'action', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		switch ( $method )
		{
		    case self::USER_UPDATE:
			$return = $this->add_update_user_data();
			break;
		    case self::SURVEY_DETAILS:
			$return = $this->get_survey_data();
			break;
		    case self::SURVEY_RESPONSE:
			$return = $this->save_survey_results();
			break;
		    case self::USER_DETAILS:
			$return = $this->get_user_data();
			break;
		    case self::SURVEYS:
			$return = $this->get_surveys_data();
			break;
		    case self::FILLED_SURVEYS:
			$return = $this->get_filled_surveys();
			break;
		    case self::PUBLIC_URL:
			$return = $this->get_public_url();
			break;
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    public function add_update_user_data()
    {
	try
	{
	    $corrupt = false;
	    $user_id = 0;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    $user_email = $this->esc( $this->central->getargs( 'email', $_POST, $corrupt ));
	    if ( $user_email == ""  )
	    {
		$return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
            }
	    if ( !$corrupt )
	    {
		$data = array(
		    'password' => '',
		    'deleted' => '0',
		    'fb_id' => $id,
		    'user_name' => $this->esc( $this->central->getargs( 'name', $_POST, $corrupt ) ),
		    'email' => $user_email,
		    'device_id' => $this->central->getargs( 'device_id', $_POST, $corrupt ),
		    'data' => $this->esc( serialize( $_POST ) ),
		    'role' => Constants::USER_ROLE_USER,
		    'status' => Constants::STATUS_ACTIVE,
		    'updated_at' => date( "Y-m-d H:i:s" ),
		);

		$user = $this->central->check_existance( $this->profile, "user", "fb_id = '{$id}'" );
		if ( !$user )
		{
		    $data[ 'created_at' ] = date( "Y-m-d H:i:s" );
		    if ( Plusql::into( $this->profile )->user( $data )->insert() )
			$user_id = $this->central->get_accurate_last_id( "user", $this->profile );
		}
		else
		{
		    $user_id = $user->user_id;
		    Plusql::on( $this->profile )->user( $data )->where( "user_id = {$user_id}" )->update();
		}

		if ( $user_id )
		{

		    $this->update_user_education( $user_id );
		    $this->update_user_interests( $this->profile, $user_id );
		    // $this->add_update_user_details( $user_id, $_POST, false );
		    // push to queue
		    $_POST[ 'user_id' ] = $user_id;
		    $this->pheanstalk_client->useTube( Constants::BEANSTALKD_LIKES_TUBE )->put( serialize( $_POST ) );
		    $return = array(
			"success" => 1,
			"error" => 0,
			"message" => 0,
			"inserted" => $this->add_update_user_details( $user_id, $_POST, false ),
			"id" => $user_id,
		    );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    private function update_user_education( $user_id )
    {
	try
	{
	    $corrupt = false;
	    $education = $this->central->getargs( 'education', $_POST, $corrupt );
	    if ( $education )
	    {
		foreach ( $education as $institute )
		{
		    $data = array(
			'user_id' => $user_id,
			'institute' => $this->esc( $institute[ 'school' ][ 'name' ] ),
			'fb_id' => $institute[ 'school' ][ 'id' ],
			'type' => $institute[ 'type' ],
			'year' => isset( $institute[ 'year' ][ 'name' ] ) ? $institute[ 'year' ][ 'name' ] : "",
		    );

		    $school = $this->central->check_existance( $this->profile, "user_education", "user_id = {$user_id} AND institute = '" . $this->esc( $data[ 'institute' ] ) . "' AND type = '" . $this->esc( $data[ 'type' ] ) . "'" );
		    if ( !$school )
			Plusql::into( $this->profile )->user_education( $data )->insert();
		    else
			Plusql::on( $this->profile )->user_education( $data )->where( "user_id = {$user_id} AND institute = '" . $this->esc( $data[ 'institute' ] ) . "' AND type = '" . $this->esc( $data[ 'type' ] ) . "'" )->update();

		    // $this->add_education_category( $institute[ 'type' ], $institute[ 'school' ] );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

    private function add_education_category( $category, $page )
    {
	try
	{
	    $survey_categories = $this->central->check_existance( $this->profile, "survey_categories", "title = '{$category}'" );
	    if ( !$survey_categories )
	    {
		Plusql::into( $this->profile )->survey_categories( array( 'title' => $this->esc( $category ) ) )->insert();
		$survey_categories_id = $this->central->get_accurate_last_id( "survey_categories", $this->profile );
	    }
	    else
	    {
		$survey_categories_id = $survey_categories->survey_categories_id;
		Plusql::on( $this->profile )->survey_categories( array( 'title' => $this->esc( $category ) ) )->where( "survey_categories_id = {$survey_categories_id}" )->update();
	    }

	    $fb_page = $this->central->check_existance( $this->profile, "fb_pages", "title = '" . $this->esc( $page[ 'name' ] ) . "'" );
	    if ( !$fb_page )
	    {
		Plusql::into( $this->profile )->fb_pages( array(
		    'title' => $this->esc( $page[ 'name' ] ),
		    'fb_id' => $page[ 'id' ],
		    'survey_categories_id' => $survey_categories_id,
		) )->insert();
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

    public static function update_user_interests( $profile, $user_id, $is_daemon = false )
    {
	try
	{
	    $corrupt = false;
	    $likes = Central::getargs( 'likes', $_POST, $corrupt );
	    if ( $likes )
	    {
		foreach ( $likes as $like )
		{
		    if ( $is_daemon )
		    echo PHP_EOL . "*** title = '{$like[ 'name' ]}' AND fb_id = '{$like[ 'id' ]}' - - - ";
		    $fb_page = Central::check_existance( $profile, "fb_pages", 'fb_id = "' . $like[ 'id' ] . '"' );
		    if ( $fb_page )
		    {
			$user_interest = Central::check_existance( $profile, "user_interests", "fb_pages_id = '{$fb_page->fb_pages_id}' AND user_id = '{$user_id}'" );
			if ( !$user_interest )
			{
			    Plusql::into( $profile )->user_interests( array(
				'user_id' => $user_id,
				'survey_categories_id' => $fb_page->survey_categories_id,
				'fb_pages_id' => $fb_page->fb_pages_id,
			    ) )->insert();
			}
		    }
		}
	    }
	}
	catch ( Exception $ex )
	{
		echo $ex->getMessage().PHP_EOL;
	    // do nothing
	}
    }

    private function get_survey_data()
    {
	try
	{

	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => "A: " . Constants::ERROR_MESSAGE );
	    $survey_id = $this->central->getargs( 'id', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$data = array();
		$survey = PluSQL::from( $this->profile )->survey->question->question_options->select( "*, survey.title AS survey_title" )->where( "survey.survey_id = {$survey_id} AND survey.status = '" . Constants::STATUS_PUBLISHED . "' AND survey.deleted <> 1 AND question.deleted <> 1 AND question_options.deleted <> 1" )->run()->survey;
		$data[ 'survey' ][ 'title' ] = $survey->survey_title;
		foreach ( $survey->question as $k => $question )
		{
		    $data[ 'survey' ][ 'questions' ][ $k ][ 'title' ] = $this->central->string_formating( $question->title );
		    $data[ 'survey' ][ 'questions' ][ $k ][ 'type' ] = $question->type;
		    $data[ 'survey' ][ 'questions' ][ $k ][ 'id' ] = $question->question_id;
		    if ( in_array( $question->type, array( "radio", "checkbox", "multi-image", "single-image", "rating-image" ) ) )
		    {
			foreach ( $question->question_options as $option )
			{
			    try
			    {
				if ( $question->type == "single-image" || $question->type == "multi-image" || $question->type == "rating-image" )
				{
				    $count = 0;
				    $images_survey = PluSQL::from( $this->profile )->survey_option_image->select( "*" )->where( "question_options_id = {$option->question_options_id}" )->run()->survey_option_image;
				    foreach ( $images_survey as $image_survey )
				    {
					$data[ 'survey' ][ 'questions' ][ $k ][ 'count' ] = $count++;

					$data[ 'survey' ][ 'questions' ][ $k ][ 'options' ][] = array(
					    "id" => $image_survey->question_options_id,
					    "value" => $this->central->string_formating( $option->value ),
					    "label" => $this->central->string_formating( $option->label ),
					    "image_url" => $image_survey->image_url,
					    "survey_option_image_id" => $image_survey->survey_option_image_id,
					);
				    }
				}
				else
				{
				    $data[ 'survey' ][ 'questions' ][ $k ][ 'options' ][] = array(
					"value" => $this->central->string_formating( $option->value ),
					"label" => $this->central->string_formating( $option->label ),
					"id" => $option->question_options_id,
				    );
				}
			    }
			    catch ( Exception $ex )
			    {
				$data[ 'survey' ][ 'questions' ][ $k ][ 'options' ][] = array(
				    "value" => $this->central->string_formating( $option->value ),
				    "label" => $this->central->string_formating( $option->label ),
				    "id" => $option->question_options_id,
				);
			    }
			}
		    }
		    else
		    {
			$data[ 'survey' ][ 'questions' ][ $k ][ 'options' ][] = array(
			    "value" => $this->central->string_formating( $question->question_options->value ),
			    "label" => $this->central->string_formating( $question->question_options->label ),
			    "id" => $question->question_options->question_options_id,
			);
		    }
		}

		$data[ 'survey' ][ 'questions' ] = $this->adjust_questions( $data[ 'survey' ][ 'questions' ], $this->get_extra_questions() );

		$return = array_merge( $data, array(
		    'success' => 1,
		    'error' => 0,
		    'message' => '',
			) );
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $return;
    }

    private function adjust_questions( $data, $extra_questions )
    {
	try
	{
	    $questions = array();
	    $count = 0;
	    foreach ( $data as $k => $question )
	    {
		$questions[] = $question;
		if (
			$k % 3 == 0 && isset( $extra_questions[ $count ] ) &&
			$extra_questions[ $count ] && $count < 6
		)
		{
		    $questions[] = $extra_questions[ $count ];
		    $count++;
		}
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $questions;
    }

    private function get_extra_questions()
    {
	try
	{
	    $data = array();
	    $questions = PluSQL::from( $this->profile )->question->question_options->select( "*" )->where( "question.survey_id = 0 AND question.compulsory = 1 AND question.deleted <> 1 AND question_options.deleted <> 1" )->run()->question;
	    foreach ( $questions as $k => $question )
	    {
		$data[ $k ][ 'title' ] = $this->central->string_formating( $question->title );
		$data[ $k ][ 'type' ] = $question->type;
		$data[ $k ][ 'id' ] = $question->question_id;
		if ( in_array( $question->type, array( "radio", "checkbox" ) ) )
		{
		    foreach ( $question->question_options as $option )
		    {
			$data[ $k ][ 'options' ][] = array(
			    "value" => $this->central->string_formating( $option->value ),
			    "label" => $this->central->string_formating( $option->label ),
			    "id" => $option->question_options_id,
			);
		    }
		}
		else
		{
		    $data[ $k ][ 'options' ][] = array(
			"value" => $this->central->string_formating( $question->question_options->value ),
			"label" => $this->central->string_formating( $question->question_options->label ),
			"id" => $question->question_options->question_options_id,
		    );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	@shuffle( $data );
	return $data;
    }

    private function save_survey_results()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => "A: " . Constants::ERROR_MESSAGE );
	    $method = $this->central->getargs( 'method', $_POST, $corrupt );
	    $user_id = $this->central->getargs( 'user', $_POST, $corrupt );
	    $survey_id = $this->central->getargs( 'survey', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		switch ( $method )
		{
		    case self::SUBMITTED:
			$survey_response = $this->central->check_existance( $this->profile, "survey_response", "survey_id = {$survey_id} AND user_id = {$user_id}" );
			if ( !$survey_response )
			{
			    $user = $this->central->check_existance( $this->profile, "user", "user_id = '{$user_id}'" );
			    if ( $user )
			    {
				$data = array(
				    'survey_id' => $survey_id,
				    'user_id' => $user_id,
				    'status' => self::SUBMITTED,
				    'created_at' => date( "Y-m-d H:i:s" ),
				);
				if ( Plusql::into( $this->profile )->survey_response( $data )->insert() )
				{
				    $survey_response_id = $this->central->get_accurate_last_id( "survey_response", $this->profile );
				    $questions = $this->central->getargs( 'questions', $_POST, $corrupt );
				    $question_ids = 0;
				    $answers = array();
				    if ( !$corrupt )
				    {
					foreach ( $questions as $question )
					{
					    $question_ids .= ", {$question[ 'id' ]}";
					    foreach ( $question[ 'options' ] as $option )
					    {
						$data = array(
						    'question_id' => $question[ 'id' ],
						    'question_options_id' => $option[ 'id' ],
						    'survey_response_id' => $survey_response_id,
						    'answer' => $this->esc( $option[ 'value' ] ),
						    'is_correct' => $this->check_option_correctness( $option ),
						);
						$answers[ $data[ 'question_id' ] ] = $data[ 'is_correct' ];
						Plusql::into( $this->profile )->survey_response_answers( $data )->insert();
					    }
					}
				    }

				    $this->update_user_warning( $question_ids, $answers, $user );

				    $return = array(
					'success' => 1,
					'error' => 0,
					'message' => "Thank you for filling out this survey. RS. 30 has been added into your account.",
				    );
				}
			    }
			}
			else
			{
			    $return[ "message" ] = "You have already attempted this survey.";
			}
			break;
		    case self::ABORTED:
			$data = array(
			    'survey_id' => $survey_id,
			    'user_id' => $user_id,
			    'status' => self::ABORTED,
			    'created_at' => date( "Y-m-d H:i:s" ),
			);
			if ( Plusql::into( $this->profile )->survey_response( $data )->insert() )
			{
			    $return = array(
				'success' => 1,
				'error' => 0,
				'message' => "",
			    );
			}
			break;
		}
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $return;
    }

    private function check_option_correctness( $option )
    {
	try
	{
	    $return = false;
	    $options = Plusql::from( $this->profile )->question->question_options->select( "*" )->where( "question_options_id = {$option[ 'id' ]}" )->limit( "0, 1" )->run()->question_options;
	    if ( in_array( $options->question->type, array( "radio", "checkbox" ) ) )
	    {
		$return = $options->answer;
	    }
	    else
	    {
		$answers = explode( ",", $options->answer );
		$return = in_array( $this->esc( $option[ 'value' ] ), $answers );
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $return;
    }

    public function get_surveys_data()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $user_id = 0;
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    $survey_ids = array();
	    $surveys = array();
	    if ( !$corrupt )
	    {
		$print = 0;
		$users = $this->central->check_existance( $this->profile, "user", "user_id = '{$id}'" );
		$user = $this->central->check_existance( $this->profile, "user_details", "user_id = '{$id}'" );
		if ( $users && $user && $users->warnings <= 3 )
		{
		    $attempted_surveys = $this->get_user_attempted_surveys( $id );
		    $user->home_town = $this->central->string_formating( strlen( $user->home_town ) ) ? $this->central->string_formating( $user->home_town ) : "lahore";

		    // based on interests & location & gender & relationship_status
		    $sql = "
			SELECT 
			    DISTINCT( s.survey_id ) AS sid, s.title AS stitle, 
			    s.published_at AS published, b.user_name AS brand
			FROM 
			    ((((( user AS u 
				INNER JOIN user_details AS ud
			    ON u.user_id = ud.user_id
			    	INNER JOIN user_interests AS ui
			    ON ui.user_id = ud.user_id )
			    	INNER JOIN publish_category AS pc
			    ON ui.fb_pages_id = pc.fb_pages_id)
				INNER JOIN publish AS p
			    ON p.publish_id = pc.publish_id)
				INNER JOIN survey AS s
			    ON s.survey_id = p.survey_id ) 
				INNER JOIN user AS b 
			    ON s.user_id = b.user_id )
			WHERE
			    ui.user_id = {$id} AND s.survey_id NOT IN ( {$attempted_surveys} ) AND p.deleted <> 1 AND
			    u.created_at <= s.published_at AND p.locations LIKE CONCAT( '%', CONCAT(ud.home_town, '%') ) 
			    AND p.relationship_types LIKE CONCAT( '%', CONCAT(ud.relationship_status, '%') ) AND ( p.gender = 'all' OR p.gender = ud.gender )
		    ";
		    $results = Plusql::against( $this->profile )->run( $sql );
		    while ( $row = $results->nextRow() )
		    {
			if ( !in_array( $row[ 'sid' ], $survey_ids ) )
			{
			    $survey_ids[] = $row[ 'sid' ];
			    $surveys[ 'all' ][] = $row;
			    if ( $this->get_days_diff( $row[ 'published' ] ) < 3 )
				$surveys[ 'recent' ][] = $row;
			}
		    }
		    if ( $print )
		    {
			echo "<h1>based on interests & location & gender & relationship_status</h1>";
			echo "<h2>" . count( $survey_ids ) . "</h2>";
			echo "<pre>" . print_r( $survey_ids, 1 ) . "</pre>";
			echo "<pre>" . $sql . "</pre>";
		    }

		    if ( $survey_ids )
		    {
			// based on age
			$sql = "
			SELECT
			    DISTINCT( s.survey_id ) AS sid, s.title AS stitle,
			    s.published_at AS published, u.user_name AS brand,
			    p.age AS age
			FROM
			    user AS u
				INNER JOIN survey AS s
			    ON u.user_id = s.user_id
				INNER JOIN publish AS p
			    ON s.survey_id = p.survey_id
				INNER JOIN publish_category AS pc
			    ON p.publish_id = pc.publish_id
			WHERE
			    s.survey_id NOT IN ( {$attempted_surveys} ) AND p.deleted <> 1 AND
			    u.created_at <= s.published_at
		      ";

			$sql .= strlen( join( ",", $survey_ids ) ) ? " AND s.survey_id IN ( " . join( ",", $survey_ids ) . " )" : "";

			$results = Plusql::against( $this->profile )->run( $sql );
			$survey_ids = array();
			$surveys = array( 'all' => array(), 'recent' => array() );
			while ( $row = $results->nextRow() )
			{
			    foreach ( explode( ",", $row[ 'age' ] ) as $age )
			    {
				list($min, $max) = explode( "-", $age );
				if ( $user->age >= $min && $user->age < $max )
				{
				    if ( !in_array( $row[ 'sid' ], $survey_ids ) )
				    {
					$survey_ids[] = $row[ 'sid' ];
					$surveys[ 'all' ][] = $row;
					if ( $this->get_days_diff( $row[ 'published' ] ) < 3 )
					    $surveys[ 'recent' ][] = $row;
				    }
				}
			    }
			}
			if ( $print )
			{
			    echo "<h1>based on age</h1>";
			    echo "<h2>" . count( $survey_ids ) . "</h2>";
			    echo "<pre>" . print_r( $survey_ids, 1 ) . "</pre>";
			    echo "<pre>" . $sql . "</pre>";
			}
			// based on education
			try
			{
			    $user_education = Plusql::from( $this->profile )->user_education->select( "*" )->where( "user_id = '{$id}'" )->run()->user_education;
			    $institutes = " ( ";
			    foreach ( $user_education as $education )
				$institutes .= " OR p.education LIKE '%" . $this->central->string_formating( $education->institute ) . "%'";

			    $institutes = " ) ";

			    $sql = "
			    SELECT
				DISTINCT( s.survey_id ) AS sid, s.title AS stitle,
				s.published_at AS published, u.user_name AS brand
			    FROM 
				user AS u
				    INNER JOIN survey AS s
				ON u.user_id = s.user_id
				    INNER JOIN publish AS p
				ON s.survey_id = p.survey_id
			    WHERE
				s.survey_id NOT IN ( {$attempted_surveys} ) AND p.deleted <> 1 AND
				( 1=1 {$institutes} AND
				u.created_at <= s.published_at
			  ";

			    $sql .= strlen( join( ",", $survey_ids ) ) ? " AND s.survey_id IN ( " . join( ",", $survey_ids ) . " )" : "";
			    $results = Plusql::against( $this->profile )->run( $sql );
			    $survey_ids = array();
			    $surveys = array( 'all' => array(), 'recent' => array() );
			    while ( $row = $results->nextRow() )
			    {
				if ( !in_array( $row[ 'sid' ], $survey_ids ) )
				{
				    $survey_ids[] = $row[ 'sid' ];
				    $surveys[ 'all' ][] = $row;
				    if ( $this->get_days_diff( $row[ 'published' ] ) < 3 )
					$surveys[ 'recent' ][] = $row;
				}
			    }
			}
			catch ( Exception $ex )
			{
			    $return[ 'message' ] = $ex->getMessage();
			    // do nothing
			}
			if ( $print )
			{
			    echo "<h1>based on education</h1>";
			    echo "<h2>" . count( $survey_ids ) . "</h2>";
			    echo "<pre>" . print_r( $survey_ids, 1 ) . "</pre>";
			    echo "<pre>" . $sql . "</pre>";
			}
		    }

		    $return = array(
			'success' => 1,
			'error' => 0,
			'message' => '',
			'surveys' => $surveys,
		    );

		    if ( !$surveys )
		    {
			$return[ 'no_survey' ] = "We are finding surveys according to your interests. Thank you for being patient.";
		    }
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    private function get_public_url()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$sql = "
			SELECT
			    *
			FROM 
			    ( survey AS s
			    INNER JOIN
			    publish AS p ON s.survey_id = p.survey_id ) 
			    INNER JOIN
			    survey_short_link AS sl ON s.survey_id = sl.survey_id
			WHERE
			    valid >= '" . date( "Y-m-d" ) . "'
		    ";

		$survey = Plusql::from( $this->profile )->survey->survey_short_link->select( '*' )->where( "survey.status = '" . Constants::STATUS_PUBLISHED . "' AND valid >= '" . date( "Y-m-d" ) . "'" )->run()->survey_short_link;
		$return = array( 'success' => 1, 'error' => 0, 'message' => '' );
		/*
		  $survey = Plusql::against( $this->profile )->run( $sql );
		  while ( $row = $survey->nextRow() )
		  $return = array( 'success' => 1, 'error' => 0, 'message' => '' );
		 */
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    private function get_filled_surveys()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $user_id = 0;
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    $survey_ids = array();
	    $surveys = array();
	    if ( !$corrupt )
	    {
		$user = $this->central->check_existance( $this->profile, "user_details", "user_id = '{$id}'" );
		if ( $user )
		{
		    $attempted_surveys = $this->get_user_attempted_surveys( $id );
		    $brands = Plusql::from( $this->profile )->user->survey->survey_response->select( "*" )->where( "survey.survey_id IN ({$attempted_surveys}) AND survey_response.status = 1 " )->run()->user;
		    foreach ( $brands as $brand )
		    {
			foreach ( $brand->survey as $survey )
			{
			    if ( !array_key_exists( $survey->survey_id, $return[ 'surveys' ] ) )
				$return[ 'surveys' ][ $survey->survey_id ] = array(
				    'sid' => $survey->survey_id,
				    'stitle' => $this->central->string_formating( $survey->title ),
				    'published' => $survey->published_at,
				    'brand' => $this->central->string_formating( $brand->user_name ),
				    'attempted' => date( "Y-m-d H:i:s", strtotime( $brand->survey_response->created_at ) ),
				);
			}
		    }

		    $return[ 'success' ] = 1;
		    $return[ 'error' ] = 0;
		    $return[ 'message' ] = '';
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    public function get_user_data()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $user_id = 0;
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    $method = $this->central->getargs( 'method', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$user_details = $this->central->check_existance( $this->profile, "user_details", "user_id = '{$id}'" );
		switch ( $method )
		{
		    case self::GET:
			if ( $user_details )
			    $return = array(
				'success' => 1,
				'error' => 0,
				'message' => '',
				'details' => array(
				    'name' => $this->central->string_formating( $user_details->name ),
				    'email' => $this->central->string_formating( $user_details->email ),
				    'cnic' => $user_details->cnic,
				    'age' => $user_details->age,
				    'hometown' => $this->central->string_formating( $user_details->home_town ),
				    'gender' => $user_details->gender,
				    'dob' => date( "Y-m-d", strtotime( $user_details->dob ) ),
				    'relationship_status' => $this->central->string_formating( $user_details->relationship_status ),
				),
			    );
			break;
		    case self::UPDATE :
			if ( $this->add_update_user_details( $id, $_POST ) )
			{
			    $return = array(
				'success' => 1,
				'error' => 0,
				'message' => 'Data',
			    );
			}
			break;
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->
		    getMessage();
	}

	return $return;
    }

    private function add_update_user_details( $user_id, $data, $update = true )
    {
	try
	{
	    $all_ok = true;
	    $corrupt = false;
	    $user_details = $user = $this->central->check_existance( $this->profile, "user_details", "user_id = '{$user_id}'" );
	    $details = array(
		'user_id' => $user_id, 'name' => $this->central->getargs( "name", $data, $corrupt, $this->central->getargs( "email", $data, $corrupt ) ), 'email' => $this->central->getargs( "email", $data, $corrupt ),
		'cnic' => strtolower( $this->central->getargs( "cnic", $data, $corrupt, "" ) ),
		'gender' => strtolower( $this->central->getargs( "gender", $data, $corrupt, "male" ) ),
		'age' => $this->calculate_age( $this->central->getargs( "birthday", $data, $corrupt, date( "Y-m-d" ) ) ),
		'dob' => date( "Y-m-d", strtotime( $this->central->getargs( "birthday", $data, $corrupt, date( "Y-m-d" ) ) ) ),
		'relationship_status' => $this->esc( strtolower( $this->central->getargs( "relationship_status", $data, $corrupt, "single" ) ) ),
	    );

	    if ( isset( $data[ 'hometown' ] ) )
	    {
		$details[ 'home_town' ] = $this->central->getargs( "hometown", $data, $corrupt, array( "name" => "lahore" ) );
		$details[ 'home_town' ] = $this->esc( $details[ 'home_town' ][ 'name' ] ) ? $this->esc( $details[ 'home_town' ][ 'name' ] ) : 'lahore';
	    }
	    else if ( isset( $data[ 'location' ] ) )
	    {
		$details[ 'home_town' ] = $this->central->getargs( "location", $data, $corrupt, array( "name" => "lahore" ) );
		$details[ 'home_town' ] = $this->esc( $details[ 'home_town' ][ 'name' ] ) ? $this->esc( $details[ 'home_town' ][ 'name' ] ) : 'lahore';
	    }
	    else
	    {
		$details[ 'home_town' ] = "lahore";
            }
	    if ( isset( $data[ 'age' ] ) )
		$details[ 'age' ] = $this->central->getargs( "age", $data, $corrupt, "" );

	    if ( !$user_details )
	    {
		$all_ok = Plusql::into( $this->profile )->user_details( $details )->insert();
	    }
	    else if ( $update )
	    {
		$all_ok = Plusql::on( $this->profile )->user_details( $details )->where( "user_details_id = {$user_details->user_details_id}" )->update();
	    }

	}
	catch ( Exception $ex )
	{
	    $all_ok = $ex->getMessage();
	}

	// 

	return $all_ok;
    }

    private function get_user_attempted_surveys( $user_id )
    {
	try
	{
	    $in = 0;
	    $survey_responses = Plusql::from( $this->profile )->survey_response->select( "*" )->where( "user_id = {$user_id}" )->run()->survey_response;
	    foreach ( $survey_responses as $survey_respons )
	    {
		$in .= ", {$survey_respons->survey_id}";
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $in;
    }

    private function calculate_age( $dob )
    {
	$from = new DateTime( date( 'Y-m-d', strtotime( $dob ) ) );
	$to = new DateTime( 'today'
	);
	return $from->diff( $to )->y;
    }

    private function get_days_diff( $date )
    {
	$from = new DateTime( date( 'Y-m-d', strtotime( $date ) ) );
	$to = new DateTime( 'today' );
	return $from->diff( $to )->d;
    }

    private function update_user_warning( $question_ids, $answers, $user )
    {
	try
	{
	    $questions = Plusql::from( $this->profile )->question->select( "*" )->where( "question_id IN ( {$question_ids} ) AND compulsory = 1" )->run()->question;
	    $total_questions = 0;
	    $correct_answers = 0;
	    foreach ( $questions as $question )
	    {
		if (
			isset( $answers[ $question->question_id ] ) &&
			$answers[ $question->question_id ]
		)
		    $correct_answers++;

		$total_questions++;
	    }

	    $ratio = ( $correct_answers / $total_questions ) * 100;
	    if ( $ratio < 50 )
	    {
		Plusql::on( $this->profile )->user( array( 'warnings' => ($user->warnings + 1) ) )->where( "user_id = {$user->user_id}" )->update();
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

}

/*
for ( $i = 0; $i < 25; $i++ )
{
$surveys[ 'all' ][] = array(
"sid" => 0,
"stitle" => "Dummy",
"published" => date( "Y-m-d" ),
"brand" => "X brand",
);
}
*/

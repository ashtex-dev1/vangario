<?php

@session_start();

use Config\Constants;
use Config\Central;

class ProfileAction implements RocketSled\Runnable
{

    const ADD = 1;
    const UPDATE = 2;

    //--private members
    private $profile = "user";
    private $central;

    //--constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function run()
    {
	try
	{
	    $data = $this->update_main_contents();
	    @header( 'location: ' . $_SERVER[ 'HTTP_REFERER' ] );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $user_id = $this->central->get_logged_in_user_id( $this->profile );
	    if ( $user_id )
	    {
		$data = array(
		    'user_name' => $this->central->getargs( 'name', $_POST, $corrupt ),
		    'email' => $this->central->getargs( 'email', $_POST, $corrupt ),
		);
		$password = $this->central->getargs( 'password', $_POST, $corrupt );

		if ( strlen( $password ) )
		{
		    $bcrypt = new \Config\Bcrypt();
		    if ( !$bcrypt->verify( $password, $user->password ) )
			$data[ 'password' ] = $bcrypt->hash( $password );
		}

		try
		{
		    $store = Plusql::from( $this->profile )->user->select( "*" )->where( "user_id = {$user_id}" )->limit( "0, 1" )->run()->user;
		    if ( Plusql::on( $this->profile )->user( $data )->where( 'user_id = ' . $user_id )->update() )
			$return = array(
			    'success' => 1,
			    'error' => 0,
			    'message' => 'Operation performed successfully.',
			);
		}
		catch ( EmptySetException $ex )
		{
		    if ( Plusql::into( $this->profile )->user( $data )->insert() )
			$return = array(
			    'success' => 1,
			    'error' => 0,
			    'message' => 'Operation performed successfully.',
			);
		}
		catch ( Exception $ex )
		{
		    $return[ 'message' ] = $ex->getMessage();
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

}

?>

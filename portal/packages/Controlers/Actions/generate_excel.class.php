<?php

use Config\Central;
use Config\Constants;

class GenerateExcel implements \RocketSled\Runnable
{

    private $profile = "BV";

    //--constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    // will do something here
	}
    }

    public function run()
    {
	$this->generate_excel();
    }

    private function generate_excel( $return = false )
    {
	$survey_id = $_GET[ 'id' ];
	$excel_obj = new PHPExcel();
	$columns = Statistics::get_columns( $this->profile, $survey_id );
	if ( isset( $_POST[ "report" ] ) || TRUE )
	{
	    try
	    {
		$excel_obj->getProperties()->setCreator( "BrandVibe" );
		$this->set_basic_formatting( $excel_obj );
		$last_index = $this->populate_questions( $excel_obj, $columns );
		$last_index = $this->add_user_heading_columns( $last_index, $excel_obj, $columns );
		$this->add_user_data( $last_index, $excel_obj, $survey_id, $columns );
	    }
	    catch ( Exception $e )
	    {
		// will do something here
	    }
	}
	else
	{
	    // will do something here
	}
	@header( 'Content-type: application/vnd.ms-excel' );
	@header( 'Content-Disposition: attachment; filename="Report-' . date( "Y-m-d" ) . '.xls"' );
	@header( 'Pragma: no-cache' );
	@header( 'Expires: 0' );
	$writer_obj = PHPExcel_IOFactory::createWriter( $excel_obj, 'Excel5' );
	ob_clean();
	$writer_obj->save( 'php://output' );
	exit();
    }

    private function set_basic_formatting( $excel_obj )
    {
	try
	{
	    $excel_obj->getDefaultStyle()->getAlignment()->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_LEFT );
	    $excel_obj->setActiveSheetIndex( 0 )->getStyle( "A1:N1" )->getFont()->setSize( 20 )->setBold( true );
	    $excel_obj->setActiveSheetIndex( 0 )->mergeCells( "A1:N1" )->setCellValue( "A1", "Analysis" );
	    $excel_obj->setActiveSheetIndex( 0 )->getStyle( "A2" )->getFont()->setSize( 12 )->setBold( true );
	    $excel_obj->setActiveSheetIndex( 0 )->setCellValue( 'A2', 'Questions' );
	}
	catch ( Exception $ex )
	{
	    // will do nothing
	}
    }

    private function populate_questions( $excel_obj, $columns )
    {
	try
	{
	    $index = 3;
	    $question_count = 1;
	    foreach ( $columns as $column )
	    {
		if (
			is_array( $column ) && isset( $column[ 'table' ] ) &&
			strcmp( $column[ 'table' ], 'question' ) === 0 && $column[ 'data' ]
		)
		{
		    $excel_obj->getActiveSheet()->getStyle( "A{$index}" )->getFont()->setBold( true );
		    $excel_obj->getActiveSheet()->setCellValue( "A{$index}", "Q #{$question_count}" );
		    $excel_obj->getActiveSheet()->setCellValue( "B{$index}", strlen( $column[ 'data' ]->qtitle ) ? $column[ 'data' ]->qtitle : "-"  );
		    $index++;
		    $question_count++;
		    $option_count = 1;
		    if (
			    strcmp( $column[ 'data' ]->type, "short" ) !== 0 &&
			    strcmp( $column[ 'data' ]->type, "long" ) !== 0
		    )
		    {
			foreach ( $column[ 'data' ]->question_options as $option )
			{
			    $excel_obj->getActiveSheet()->setCellValue( "B{$index}", "#{$option_count}" );
			    $excel_obj->getActiveSheet()->setCellValue( "C{$index}", $option->label );
			    $option_count++;
			    $index++;
			}
		    }
		}
	    }
	}
	catch ( Exception $ex )
	{
	    // will do something
	}

	return $index;
    }

    private function add_user_heading_columns( $index, $excel_obj, $columns )
    {
	try
	{
	    $index++;
	    $sheet_column = "A";
	    foreach ( $columns as $column )
	    {
		if ( strcmp( $column, "#" ) !== 0 )
		{
		    $excel_obj->getActiveSheet()->getStyle( "{$sheet_column}{$index}" )->getFont()->setBold( true );
		    $excel_obj->getActiveSheet()->setCellValue( "{$sheet_column}{$index}", is_array( $column ) ? ucwords( $column[ 'title' ] ) : ucwords( $column )  );
		    $sheet_column++;
		}
	    }

	    $index++;
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $index;
    }

    private function add_user_data( $index, $excel_obj, $survey_id, $columns )
    {
	try
	{
	    $count = 1;
	    $results = Statistics::get_respondents( $this->profile, $survey_id );
	    while ( $row = $results->nextRow() )
	    {
		$sheet_column = "A";
		foreach ( $columns as $column )
		{
		    if ( strcmp( $column, "#" ) !== 0 )
		    {
			if ( !is_array( $column ) )
			{
			    $excel_obj->getActiveSheet()->setCellValue( "{$sheet_column}{$index}", strlen( $row[ $column ] ) ? ucwords( $row[ $column ] ) : "-"  );
			}
			else
			{
			    switch ( $column[ 'table' ] )
			    {
				case 'user_education':
				    $excel_obj->getActiveSheet()->setCellValue( "{$sheet_column}{$index}", Statistics::get_user_education( $this->profile, $row[ 'uid' ], 1 ), 1 );
				    break;
				case 'user_interests':
				    $excel_obj->getActiveSheet()->setCellValue( "{$sheet_column}{$index}", Statistics::get_user_interests( $this->profile, $row[ 'uid' ], $survey_id , 1), 1 );
				    break;
				case 'question':
				    $excel_obj->getActiveSheet()->setCellValue( "{$sheet_column}{$index}", Statistics::get_user_answers( $this->profile, $survey_id, $row[ 'uid' ], $column[ 'data' ]->question_id, 1 ), 1 );
				    break;
			    }
			}
			$sheet_column++;
		    }
		}
		$index++;
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

}

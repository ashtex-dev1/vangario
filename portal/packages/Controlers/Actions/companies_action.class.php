<?php

@session_start();

use Config\Constants;
use Config\Central;

class CompaniesAction implements RocketSled\Runnable
{

    const ADD = 1;
    const UPDATE = 2;
    const REMOVE = 3;

    //--private members
    private $profile = "supplier";
    private $central;

    //--constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function run()
    {
	try
	{
	    $data = $this->update_main_contents();
	    die( json_encode( $data ) );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{

	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $method = $this->central->getargs( 'action', $_POST, $corrupt );
	    switch ( $method )
	    {
		case self::ADD:
		case self::UPDATE:
		    $return = $this->add_update_data();
		    break;
		case self::REMOVE:
		    $return = $this->remove_data();
		    break;
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    private function add_update_data()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $action = $this->central->getargs( 'action', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$data = array(
		    'user_name' => $this->central->getargs( 'company_name', $_POST, $corrupt ),
		    'email' => $this->central->getargs( 'email', $_POST, $corrupt ),
		    'status' => $this->central->getargs( 'status', $_POST, $corrupt ),
		    'data' => serialize( $_POST ),
		    'role' => Constants::USER_ROLE_COMPANY,
		    'updated_at' => date( "Y-m-d H:i:s" ),
		    'deleted' => 0,
		);

		if ( isset( $_POST[ 'password' ] ) && strlen( $_POST[ 'password' ] ) )
		{
		    $bcypt = new Config\Bcrypt();
		    $data[ 'password' ] = $bcypt->hash( $_POST[ 'password' ] );
		}

		switch ( $action )
		{
		    case self::ADD:
			$data[ 'created_at' ] = date( "Y-m-d H:i:s" );
			$where = "user_name = '{$data[ 'first_name' ]}' AND role = '" . Constants::USER_ROLE_COMPANY . "'";
			if (
				!$this->central->check_existance( $this->profile, 'user', $where ) &&
				Plusql::into( $this->profile )->user( $data )->insert()
			)
			    $return = array(
				'success' => 1,
				'error' => 0,
				'active' => strcmp( $data[ 'status' ], Constants::STATUS_ACTIVE ) === 0,
				'message' => 'Operation performed successfully.',
				'company_id' => $this->central->get_accurate_last_id( 'user', $this->profile ),
				'action' => self::UPDATE,
			    );
			else
			    $return[ 'message' ] = 'This data already exists';
			break;
		    case self::UPDATE:
			$corrupt = false;
			$company_id = $this->central->getargs( 'company_id', $_POST, $corrupt );
			if ( !$corrupt && Plusql::on( $this->profile )->user( $data )->where( 'user_id = ' . $company_id )->update() )
			    $return = array(
				'success' => 1,
				'error' => 0,
				'active' => strcmp( $data[ 'status' ], Constants::STATUS_ACTIVE ) === 0,
				'message' => 'Operation performed successfully.',
				'company_id' => $company_id,
				'action' => self::UPDATE,
			    );
			break;
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    private function remove_data()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $action = $this->central->getargs( 'action', $_POST, $corrupt );
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		if ( Plusql::against( $this->profile )->run( "DELETE FROM user WHERE user_id = {$id}" ) )
		{
//		    Plusql::against( $this->profile )->run( "DELETE FROM user_profile WHERE user_id = {$id}" );
		    $return = array( 'success' => 1, 'error' => 0, 'message' => "Removed Successfuly" );
		}else {
		    $return = array( 'success' => 1, 'error' => 0, 'message' => "Deleted Successfuly" );
		}
		/*
		  try
		  {
		  $category = Plusql::from( $this->profile )->user->select( "*" )->where( "user_id = {$id}" )->run()->user;
		  $return = array( 'success' => 0, 'error' => 1, 'message' => "A category is linked to this UOM you cannot delete this." );
		  }
		  catch ( Exception $ex )
		  {
		  if ( Plusql::against( $this->profile )->run( "DELETE FROM uom WHERE uom_id = {$id}" ) )
		  $return = array( 'success' => 1, 'error' => 0, 'message' => "" );
		  }
		 */
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ "message" ] = $ex->getMessage();
	}

	return $return;
    }

}

?>

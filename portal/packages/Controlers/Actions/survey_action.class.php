<?php

@session_start();

use Config\Constants;
use Config\Central;
use Config\URLs;

class SurveyAction implements RocketSled\Runnable
{

    const ADD = 1;
    const UPDATE = 2;
    const REMOVE = 3;
    const PREVIEW = 4;
    const PUBLISH = 5;
    const UNPUBLISH = 6;
    const CLONE_SURVEY = 7;
    const USER_COUNT = 8;
    const SHORT_LINK = 9;

//--private members
    private $profile = "supplier";
    private $central;
    private $image_index = 0;

//--constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    $this->central->set_alias_connection( $this->profile );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function run()
    {
	try
	{
	    $data = $this->update_main_contents();
	    die( json_encode( $data ) );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $method = $this->central->getargs( 'action', $_POST, $corrupt );
	    switch ( $method )
	    {
		case self::ADD:
		case self::UPDATE:
		    $return = $this->add_update_data();
		    break;
		case self::REMOVE:
		    $return = $this->remove_data();
		    break;
		case self::PREVIEW:
		    $return = $this->survey_preview();
		    break;
		case self::PUBLISH:
		    $return = $this->publish_survey();
		    break;
		case self::UNPUBLISH:
		    $return = $this->unpublish_survey();
		    break;
		case self::CLONE_SURVEY:
		    $return = $this->clone_survey();
		    break;
		case self::USER_COUNT:
		    $return = $this->get_user_count();
		    break;
		case self::SHORT_LINK:
		    $return = $this->create_short_link();
		    break;
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    private function add_update_data()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => "A" . Constants::ERROR_MESSAGE );
	    $action = $this->central->getargs( 'action', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$data = array(
		    'title' => $this->central->getargs( 'title', $_POST, $corrupt ),
		    'user_id' => $this->central->get_logged_in_user_id( $this->profile ),
		    'survey_id' => $this->central->getargs( 'survey_id', $_POST, $corrupt ),
		    'deleted' => 0,
		    'status' => Constants::STATUS_DRAFT,
		    'updated_at' => date( "Y-m-d H:i:s" ),
		);

		switch ( $action )
		{
		    case self::ADD:
			$data[ 'created_at' ] = date( "Y-m-d H:i:s" );
			$where = "title = '{$data[ 'title' ]}' AND survey_id = '{$data[ 'survey_id' ]}'";
			if (
				!$this->central->check_existance( $this->profile, 'question', $where ) &&
				Plusql::into( $this->profile )->survey( $data )->insert()
			)
			{
			    $survey_id = $this->central->get_accurate_last_id( 'survey', $this->profile );
			    if ( $survey_id )
			    {
				$return = array(
				    'success' => 1,
				    'error' => 0,
				    'message' => 'Operation performed successfully.',
				    'survey_id' => $survey_id,
				    'questions' => $this->add_update_questions( $survey_id ),
				    'action' => self::UPDATE,
				    'target' => URLs::SURVEY . $this->central->encode_url_param( URLs::ADD_VIEW ) . $this->central->encode_url_param( ( str_replace( "%ID%", $survey_id, URLs::ID ) ) ) . $this->central->encode_url_param( "publish-1" ),
				);
			    }
			}
			else
			    $return[ 'message' ] = 'This data already exists';
			break;
		    case self::UPDATE:
			$corrupt = false;
			$survey_id = $this->central->getargs( 'survey_id', $_POST, $corrupt );
			if ( !$corrupt && Plusql::on( $this->profile )->survey( $data )->where( 'survey_id = ' . $survey_id )->update() )
			    $return = array(
				'success' => 1,
				'error' => 0,
				'message' => 'Operation performed successfully.',
				'survey_id' => $survey_id,
				'questions' => $this->add_update_questions( $survey_id ),
				'action' => self::UPDATE,
				'target' => URLs::SURVEY . $this->central->encode_url_param( URLs::ADD_VIEW ) . $this->central->encode_url_param( ( str_replace( "%ID%", $survey_id, URLs::ID ) ) ),
			    );
			break;
		}
	    }
	}
	catch ( Exception $ex )
	{
// do nothing
	}

	// die( '<pre>' . print_r( $_FILES, 1 ) );
	return $return;
    }

    private function add_update_questions( $survey_id )
    {
	try
	{
	    $return = array();
	    $questions = $this->central->getargs( 'questions', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$this->remove_existing_questions( $survey_id );
		foreach ( $questions as $k => $question_data )
		{
		    $id = $question_index = 0;
		    $data = array(
			'title' => $question_data[ 'title' ],
			'type' => $question_data[ 'type' ],
			'user_id' => $this->central->get_logged_in_user_id( $this->profile ),
			'survey_id' => $survey_id,
			'compulsory' => 0,
			'deleted' => 0,
			'updated_at' => date( "Y-m-d H:i:s" ),
		    );
		    $where = "question_id = {$question_data[ 'id' ]} AND survey_id = {$survey_id}";
		    if (
			    !$this->central->check_existance( $this->profile, 'question', $where )
		    )
		    {
			if ( $question_data[ 'id' ] )
			    $data[ 'question_id' ] = $question_data[ 'id' ];

			$data[ 'created_at' ] = date( "Y-m-d H:i:s" );
			if ( Plusql::into( $this->profile )->question( $data )->insert() )
			{
			    if ( !$data[ 'question_id' ] )
				$question_id = $this->central->get_accurate_last_id( 'question', $this->profile );
			    else
				$question_index = $question_id = $data[ 'question_id' ];

			    $return[] = array(
				'id' => $question_id,
				'title' => $question_data[ 'title' ],
				'options' => $this->insert_update_options( $data[ 'type' ], $question_id, $question_data[ 'options' ], $question_data[ 'answer' ], $question_index ),
			    );
			}
		    }
		    else
		    {
			if ( Plusql::on( $this->profile )->question( $data )->where( "question_id = {$question_data[ 'id' ]}" )->update() )
			    $return[] = array(
				'id' => $question_data[ 'id' ],
				'title' => $question_data[ 'title' ],
				'options' => $this->insert_update_options( $data[ 'type' ], $question_data[ 'id' ], $question_data[ 'options' ], $question_data[ 'answer' ], $question_data[ 'id' ] ),
			    );
		    }
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ 'message' ] = $ex->getMessage();
	}

	return $return;
    }

    private function insert_update_options( $type, $question_id, $options, $answers, $question_index = 0 )
    {
	try
	{
	    $return = array();
	    $existing_images = $this->get_existing_files( $question_id );
	    $this->remove_existing_options( $question_id );
	    switch ( $type )
	    {
		case 'short':
		case 'long':
		    $return[ $question_id ][] = array(
			'id' => $this->add_update_record( $question_id, $options[ 0 ], $answers ),
			'label' => '',
		    );
		    break;
		case 'radio':
		    foreach ( $options as $option )
		    {
			$answer = strcmp( $option[ 'value' ], $answers ) === 0 ? 1 : 0;
			$return[ $question_id ][] = array(
			    'id' => $this->add_update_record( $question_id, $option, $answer ),
			    'label' => $option[ 'value' ],
			);
		    }
		    break;
		case 'checkbox':
		    $answers = explode( ',', $answers );
		    foreach ( $options as $option )
		    {

			$answer = in_array( $option[ 'value' ], $answers ) ? 1 : 0;
			$return[ $question_id ][] = array(
			    'id' => $this->add_update_record( $question_id, $option, $answer ),
			    'label' => $option[ 'value' ],
			);
		    }
		    break;
		case 'single-image':
		case 'multi-image':
		case 'rating-image':
		    $answers = explode( ',', $answers );
		    foreach ( $options as $k => $option )
		    {
			$option_index = (!$question_index || $question_index < 1) ? $this->image_index++ : $k;
			$answer = in_array( $option[ 'value' ], $answers ) ? 1 : 0;
			$return[ $question_id ][] = array(
			    'id' => $this->add_update_record( $question_id, $option, $answer, $type, $option_index, $question_index, $existing_images ),
			    'label' => $option[ 'value' ],
			    'value' => $option[ 'value' ],
			);
		    }
		    break;
	    }
	}
	catch ( Exception $ex )
	{
// do nothing
	}

	return $return;
    }

    private function add_update_record( $question_id, $option, $answer, $type, $option_index = 0, $question_index = 0, $existing_images = 0 )
    {
	try
	{
	    $id = 0;
	    $data = array(
		'question_id' => $question_id,
		'label' => $option[ 'value' ],
		'value' => $option[ 'value' ],
		'answer' => $answer,
		'updated_at' => date( "Y-m-d H:i:s" ),
	    );
	    $where = "question_id = {$question_id} AND question_options_id = {$option[ 'id' ]}";
	    if (
		    !$this->central->check_existance( $this->profile, 'question_options', $where )
	    )
	    {
		if ( $option[ 'id' ] )
		    $data[ 'question_options_id' ] = $option[ 'id' ];

		$data[ 'created_at' ] = date( "Y-m-d H:i:s" );
		if ( Plusql::into( $this->profile )->question_options( $data )->insert() )
		{
		    if ( $option[ 'id' ] )
			$option_index = $id = $option[ 'id' ];
		    else
		    {
			$option_index = $option_index - $existing_images;
			$id = $this->central->get_accurate_last_id( 'question_options', $this->profile );
		    }
		}
	    }
	    else
	    {
		if ( Plusql::on( $this->profile )->question_options( $data )->where( "question_id = {$question_id} AND question_options_id = {$option[ 'id' ]}" )->update() )
		    $option_index = $id = $option[ 'id' ];
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	if (
		( $type == 'single-image' || $type == 'multi-image' || $type == 'rating-image' ) &&
		isset( $_FILES )
	)
	{
	    $this->upload_files_to_server( $question_index, $id, $option_index, $type, $question_id );
	}

	return $id;
    }

    private function upload_files_to_server( $question_id, $option_id, $image_index = 0, $type, $questions_id )
    {
	try
	{
	    // echo "[ $question_id ][ $image_index ][ $type ]<br>";
	    $files = $_FILES[ 'file' ];
	    if (
		    isset( $files[ 'error' ][ $question_id ][ $image_index ][ $type ] ) &&
		    !$files[ 'error' ][ $question_id ][ $image_index ][ $type ]
	    )
	    {
		$file = $_FILES[ 'file' ][ 'name' ][ $question_id ][ $image_index ][ $type ];
		$tmp_name = $files[ 'tmp_name' ][ $question_id ][ $image_index ][ $type ];
		$file_name = md5( uniqid( 'img_' . $file ) );
		if ( move_uploaded_file( $tmp_name, __DIR__ . "/../../files/" . $file_name ) )
		{
		    $this->remove_existing_pictures( $option_id );
		    $option_data = array(
			'question_options_id' => $option_id,
			'question_id' => $questions_id,
			'image_name' => $file_name,
			'image_url' => Central::get_server_path( TRUE ) . "/packages/files/" . $file_name,
			'deleted' => 0
		    );
		    {
			Plusql::into( $this->profile )->survey_option_image( $option_data )->insert();
		    }
		}
	    }
	}
	catch ( Exception $ex )
	{
	    Central::pr( $ex->getMessage() );
	    return false;
	}
    }

    private function get_existing_files( $question_id = 0 )
    {
	try
	{
	    $return = 0;
	    $sql = "
		    SELECT 
			*
		    FROM
			question_options
			    INNER JOIN
			survey_option_image ON question_options.question_options_id = survey_option_image.question_options_id
		    WHERE
			question_options.question_id = {$question_id}
		";

	    $result = Plusql::against( $this->profile )->run( $sql );
	    while ( $row = $result->nextRow() )
	    {
		$return++;
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}

	return $return;
    }

    private function remove_existing_questions( $survey_id )
    {
	try
	{
	    Plusql::against( $this->profile )->run( "DELETE FROM question WHERE survey_id = {$survey_id}" );
	}
	catch ( Exception $ex )
	{
// do nothing
	}
    }

    private function remove_existing_options( $question_id )
    {
	try
	{
	    Plusql::against( $this->profile )->run( "DELETE FROM question_options WHERE question_id = {$question_id}" );
	}
	catch ( Exception $ex )
	{
// do nothing
	}
    }

    private function remove_existing_pictures( $option_id )
    {
	try
	{
	    Plusql::against( $this->profile )->run( "DELETE FROM survey_option_image WHERE question_options_id = {$option_id}" );
	}
	catch ( Exception $ex )
	{
// do nothing
	}
    }

    private function remove_data()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => "B" . Constants::ERROR_MESSAGE );
	    $action = $this->central->getargs( 'action', $_POST, $corrupt );
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		if ( Plusql::against( $this->profile )->run( "DELETE FROM survey WHERE survey_id = {$id}" ) )
		{
		    $questions = Plusql::from( $this->profile )->question->select( "*" )->where( "survey_id = {$id}" )->run()->question;
		    Plusql::against( $this->profile )->run( "DELETE FROM question WHERE survey_id = {$id}" );
		    foreach ( $questions as $question )
			Plusql::against( $this->profile )->run( "DELETE FROM question_options WHERE question_id = {$question->question_id}" );
		    $return = array( 'success' => 1, 'error' => 0, 'message' => "" );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ "message" ] = $ex->getMessage();
	}

	return $return;
    }

    private function publish_survey()
    {
	try
	{

	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $id = $this->central->getargs( 'survey_id', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$where = "survey_id = {$id}";
		$survey = $this->central->check_existance( $this->profile, 'survey', $where );
		if ( $survey )
		{
		    $this->remove_publish_survey( $id );
		    $data = array(
			'survey_id' => $id,
			'user_id' => $this->central->get_logged_in_user_id( $this->profile ),
			'data' => serialize( $_POST ),
			'age' => $this->central->getargs( 'age', $_POST, $corrupt ),
			'gender' => $this->central->getargs( 'grender', $_POST, $corrupt ),
			'education' => $this->central->getargs( 'education', $_POST, $corrupt ),
			'relationship_types' => $this->central->getargs( 'relationship', $_POST, $corrupt ),
			'custom_audiences' => $this->central->getargs( 'custom_audiences', $_POST, $corrupt ),
			'locations' => $this->central->getargs( 'location', $_POST, $corrupt ),
			'languages' => $this->central->getargs( 'languages', $_POST, $corrupt ),
			'zero_audience' => $this->central->getargs( 'zero-audience', $_POST, $corrupt ),
			'created_at' => date( "Y-m-d H:i:s" ),
			'updated_at' => date( "Y-m-d H:i:s" ),
		    );

		    if ( is_array( $data[ 'age' ] ) )
			$data[ 'age' ] = implode( ",", $data[ 'age' ] );

		    if ( is_array( $data[ 'education' ] ) )
			$data[ 'education' ] = implode( ",", $data[ 'education' ] );

		    if ( is_array( $data[ 'relationship_types' ] ) )
			$data[ 'relationship_types' ] = implode( ",", $data[ 'relationship_types' ] );

		    if ( is_array( $data[ 'locations' ] ) )
			$data[ 'locations' ] = implode( "~", $data[ 'locations' ] );

		    if ( is_array( $data[ 'custom_audiences' ] ) )
			$data[ 'custom_audiences' ] = implode( ",", $data[ 'custom_audiences' ] );

		    if (
			    Plusql::into( $this->profile )->publish( $data )->insert() &&
			    Plusql::on( $this->profile )->survey( array(
				'status' => Constants::STATUS_PUBLISHED,
				'published_at' => date( "Y-m-d H:i:s" ),
			    ) )->where( $where )->update()
		    )
		    {
			$publish = $this->central->check_existance( $this->profile, 'publish', $where );
			$demographics = $this->central->getargs( 'demographics', $_POST, $corrupt );
			if ( !is_array( $demographics ) )
			{
			    Plusql::into( $this->profile )->publish_category( array(
				"publish_id" => $publish->publish_id,
				"fb_pages_id" => $demographics,
			    ) )->insert();
			}
			else
			{
			    foreach ( $demographics as $page )
			    {
				Plusql::into( $this->profile )->publish_category( array(
				    "publish_id" => $publish->publish_id,
				    "fb_pages_id" => $page,
				) )->insert();
			    }
			}

			$this->send_notification( array(
			    'title' => 'New Survey',
			    'message' => "'{$survey->title}' is available.",
			    'published_at' => $data[ 'created_at' ],
			) );
			$return = array( 'success' => 1, 'error' => 0, 'message' => "" );
		    }
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ "message" ] = $ex->getMessage();
	}

	return $return;
    }

    private function clone_survey()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$where = "survey_id = {$id}";
		$survey = $this->central->check_existance( $this->profile, 'survey', $where );
		if ( $survey )
		{
		    try
		    {
			if (
				Plusql::into( $this->profile )->survey( array(
				    'title' => $survey->title . " ( copy )",
				    'user_id' => $survey->user_id,
				    'deleted' => 0,
				    'status' => Constants::STATUS_DRAFT,
				    'created_at' => date( "Y-m-d H:i:s" ),
				    'updated_at' => date( "Y-m-d H:i:s" ),
				) )->insert()
			)
			{
			    $survey_id = $this->central->get_accurate_last_id( 'survey', $this->profile );
			    $questions = Plusql::from( $this->profile )->question->question_options->select( "*, question.title AS qtitle" )->where( $where )->run()->question;
			    foreach ( $questions as $question )
			    {
				if ( Plusql::into( $this->profile )->question( array(
					    'title' => $question->title,
					    'user_id' => $question->user_id,
					    'survey_id' => $survey_id,
					    'type' => $question->type,
					    'compulsory' => 0,
					    'deleted' => $question->deleted,
					    'created_at' => date( "Y-m-d H:i:s" ),
					    'updated_at' => date( "Y-m-d H:i:s" ),
					) )->insert() )
				{
				    $question_id = $this->central->get_accurate_last_id( 'question', $this->profile );
				    foreach ( $question->question_options as $option )
				    {
					Plusql::into( $this->profile )->question_options( array(
					    'question_id' => $question_id,
					    'label' => $option->label,
					    'value' => $option->value,
					    'answer' => $option->answer,
					    'deleted' => $option->deleted,
					    'created_at' => date( "Y-m-d H:i:s" ),
					    'updated_at' => date( "Y-m-d H:i:s" ),
					) )->insert();
				    }
				}
			    }

			    $return = array( 'success' => 1, 'error' => 0, 'message' => "Clone created..." );
			}
		    }
		    catch ( Exception $ex )
		    {
			die( $ex->getMessage() );
		    }
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ "message" ] = $ex->getMessage();
	}

	return $return;
    }

    private function get_user_count( $return_users = false )
    {
	try
	{
	    $corrupt = false;
	    $where = $join = $edu_where = $interest_where = "";
	    $details_count = $edu_count = $interest_count = array();
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $gender = $this->central->getargs( "grender", $_POST, $corrupt, 0 );
	    if ( $gender && strcmp( $gender, "all" ) !== 0 )
		$where .= " AND ( ud.gender = '{$gender}' ) ";
	    else if ( strcmp( $gender, "all" ) === 0 )
		$where .= " AND ( ud.gender != '' OR ud.gender IS NOT NULL ) ";

	    $locations = $this->central->getargs( "location", $_POST, $corrupt, '' );
	    if ( $locations )
	    {
		$where .= " AND ( ";
		if ( is_array( $locations ) )
		{
		    foreach ( $locations as $location )
		    {
			if ( strcmp( strtolower( $location ), 'not-specified' ) !== 0 )
			    $where .= "ud.home_town = '{$location}' OR ";
			else
			    $where .= "( ud.home_town IS NULL OR ud.home_town = '' ) OR ";
		    }

		    $where = rtrim( $where, "OR " ) . " )";
		}
		else
		{
		    if ( strcmp( strtolower( $locations ), 'not-specified' ) !== 0 )
			$where .= "ud.home_town = '{$locations}' ) ";
		    else
			$where .= "ud.home_town IS NULL OR ud.home_town = '' ) ";
		}
	    }

	    $relationships = $this->central->getargs( "relationship", $_POST, $corrupt, 0 );
	    if ( $relationships )
	    {
		$where .= " AND ( ";
		if ( is_array( $relationships ) )
		{
		    foreach ( $relationships as $relationship )
			$where .= "ud.relationship_status = '{$relationship}' OR ";

		    $where = rtrim( $where, "OR " ) . " )";
		}
		else
		{
		    $where .= "ud.relationship_status = '{$relationships}' ) ";
		}
	    }

	    $age_ranges = $this->central->getargs( "age", $_POST, $corrupt, 0 );
	    if ( $age_ranges )
	    {
		$where .= " AND ( ";
		if ( is_array( $age_ranges ) )
		{
		    foreach ( $age_ranges as $age )
		    {
			if ( strpos( $age, "-" ) )
			{
			    list($min, $max) = explode( "-", $age );
			    $where .= "ud.age BETWEEN {$min} AND {$max} OR ";
			}
			else if ( strpos( $age, "+" ) )
			    $where .= "ud.age > " . str_replace( "+", "", $age ) . " OR ";
		    }

		    $where = rtrim( $where, "OR " ) . " )";
		}
		else
		{
		    if ( strpos( $age_ranges, "-" ) )
		    {
			list($min, $max) = explode( "-", $age_ranges );
			$where .= "ud.age BETWEEN {$min} AND {$max} )";
		    }
		    else if ( strpos( $age_ranges, "+" ) )
			$where .= "ud.age > " . str_replace( "+", "", $age_ranges ) . " ) ";
		}
	    }

	    $institutes = $this->central->getargs( "education", $_POST, $corrupt, 0 );
	    if ( $institutes )
	    {
		$edu_where = " AND ( ";
		if ( is_array( $institutes ) )
		{
		    foreach ( $institutes as $institute )
			if ( strlen( $institute ) )
			    $edu_where .= "ue.institute = '{$institute}' OR ";

		    $edu_where = rtrim( $edu_where, "OR " ) . " )";
		}
		else
		    $edu_where .= "ue.institute = '{$institutes}' ) ";
	    }

	    $interests = $this->central->getargs( "demographics", $_POST, $corrupt, 0 );
	    if ( $interests )
	    {
		$interest_where = " AND ( ";
		if ( is_array( $interests ) )
		{
		    foreach ( $interests as $interest )
			$interest_where .= "ui.fb_pages_id = {$interest} OR ";

		    $interest_where = rtrim( $interest_where, "OR " ) . " )";
		}
		else
		    $interest_where .= "ui.fb_pages_id = {$interests} )";
	    }

	    $where = trim( $where, " AND" );
	    $details_sql = "
		    SELECT 
			u.user_id
		    FROM 
			    user AS u
			INNER JOIN
			    user_details AS ud
			ON
			    u.user_id = ud.user_id
		";

	    $details_sql = $where ? ( $details_sql . " WHERE {$where}" ) : $details_sql;

	    $in = array( 0 );
	    $details_result = Plusql::against( $this->profile )->run( $details_sql );
	    while ( $row = $details_result->nextRow() )
	    {
		if ( !in_array( $row[ 'user_id' ], $in ) )
		    $in[] = $row[ 'user_id' ];
	    }

	    if ( $edu_where )
	    {

		$edu_sql = " 
			SELECT
			    ue.user_id
			FROM
			    user_education AS ue
			WHERE
			    ue.user_id IN ( " . join( ",", $in ) . " ) {$edu_where}
			";

		$edu_result = Plusql::against( $this->profile )->run( $edu_sql );
		$in = array( 0 );
		while ( $row = $edu_result->nextRow() )
		{
		    if ( !in_array( $row[ 'user_id' ], $in ) )
		    {
			$in[] = $row[ 'user_id' ];
			$edu_count[] = $row[ 'user_id' ];
		    }
		}
	    }

	    if ( $interest_where )
	    {
		$interest_sql = " 
			SELECT
			    ui.user_id
			FROM
			    user_interests AS ui
			WHERE
			    ui.user_id IN ( " . join( ",", $in ) . " ) {$interest_where}
			";

		$interest_result = Plusql::against( $this->profile )->run( $interest_sql );
		$in = array( 0 );
		while ( $row = $interest_result->nextRow() )
		{
		    if ( !in_array( $row[ 'user_id' ], $in ) )
		    {
			$in[] = $row[ 'user_id' ];
			$interest_count[] = $row[ 'user_id' ];
		    }
		}
	    }

	    unset( $in[ 0 ] );

	    $return = array(
		"success" => 1,
		"error" => 0,
		"count" => count( $in ), // ( count( $in ) + count( $edu_count ) + count( $interest_count ) ),
		"query" => $details_sql,
	    );

	    if ( $return_users )
		$return[ 'users' ] = array_merge( $in, array_merge( $interest_count, $edu_count ) );
	}
	catch ( Exception $ex )
	{
	    $return[ "message" ] = $ex->getMessage();
	}

	return $return;
    }

    private function unpublish_survey()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$where = "survey_id = {$id}";
		$publish = $this->central->check_existance( $this->profile, 'publish', $where );
		if (
			$this->central->check_existance( $this->profile, 'survey', $where ) && $publish
		)
		{

		    if (
			    Plusql::on( $this->profile )->publish( array(
				'deleted' => 1,
				'updated_at' => date( "Y-m-d H:i:s" ),
			    ) )->where( $where )->update() &&
			    Plusql::on( $this->profile )->survey( array(
				'status' => Constants::STATUS_UNPUBLISHED,
				'valid_till' => date( "Y-m-d H:i:s" ),
			    ) )->where( $where )->update()
		    )
		    {
			$return = array( 'success' => 1, 'error' => 0, 'message' => "" );
		    }
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ "message" ] = $ex->getMessage();
	}

	return $return;
    }

// will use this at some point
    private function remove_publish_survey( $id )
    {
	try
	{
	    $where = "survey_id = {$id}";
	    $publish = $this->central->check_existance( $this->profile, 'publish', $where );
	    if ( $publish )
	    {
		Plusql::against( $this->profile )->run( "
				DELETE FROM publish WHERE survey_id = {$id}
			    " );
		Plusql::against( $this->profile )->run( "
				DELETE FROM publish_category WHERE publish_id = {$publish->publish_id}
			    " );
	    }
	}
	catch ( Exception $ex )
	{
// do nothing
	}
    }

    private function survey_preview()
    {
	try
	{
	    $corrupt = false;
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $action = $this->central->getargs( 'action', $_POST, $corrupt );
	    $id = $this->central->getargs( 'id', $_POST, $corrupt );
	    if ( !$corrupt )
	    {
		$template = $this->central->load_normal( "pages/company/preview_survey.html" );
		$questions = PluSQL::from( $this->profile )->question->question_options->select( "*" )->where( "question.deleted <> 1 AND question_options.deleted <> 1 AND user_id = {$this->central->get_logged_in_user_id( $this->profile )} AND survey_id = {$id}" )->orderBy( 'question.question_id ASC' )->run()->question;
		$item = $template->repeat( '.questions' );
		foreach ( $questions as $question )
		{
		    $item->setValue( "#title", $question->title );
		    $item->setValue( ".{$question->type}-section@style", "" );
		    if ( in_array( $question->type, array( "radio", "checkbox", "single-image", "multi-image", "rating-image" ) ) )
		    {
			$sub_item = $item->repeat( ".{$question->type}-section/.option" );
			foreach ( $question->question_options as $option )
			{
			    if ( $question->type == "multi-image" || $question->type == "single-image" || $question->type == "rating-image" )
			    {
				try
				{
				    $image_urls = PluSQL::from( $this->profile )->survey_option_image->select( "*" )->where( "survey_option_image.deleted <> 1 AND question_options_id={$option->question_options_id}" )->run()->survey_option_image;
				    foreach ( $image_urls as $image_url )
				    {
					$sub_item->setValue( "#preview-{$question->type}@src", $image_url->image_url );
				    }
				}
				catch ( Exception $ex )
				{
				    $sub_item->setValue( "#preview-{$question->type}@src", '/assets/dist/img/no-available.jpg' );
				    //do nothing
				}
				$sub_item->setValue( "#label", $option->label );
				$sub_item->setValue( "#value", $option->value );
			    }
			    $sub_item->setValue( "#label", $option->label );
			    $sub_item->setValue( "#value", $option->value );
			    if ( strcmp( $question->type, "radio" ) === 0 )
				$sub_item->query( 'i' )->item( 0 )->setAttribute( "class", $option->answer ? "ion-android-radio-button-on" : "ion-android-radio-button-off"  );
			    else
				$sub_item->query( 'i' )->item( 0 )->setAttribute( "class", $option->answer ? "ion-android-checkbox-outline" : "ion-android-checkbox-outline-blank"  );
			    $sub_item->next();
			}
			Central::remove_last_repeating_element( $sub_item, '.', 1, 0, 0 );
		    }
		    $item->next();
		}
		Central::remove_last_repeating_element( $item, '.', 1, 0, 0 );
		$return = array(
		    'success' => 1,
		    'error' => 0,
		    'message' => '',
		    'content' => $template->html(),
		);
	    }
	}
	catch ( Exception $ex )
	{
	    $return[ "message" ] = $ex->getMessage();
	}

	return $return;
    }

    private function create_short_link()
    {
	try
	{
	    $return = array( 'success' => 0, 'error' => 1, 'message' => Constants::ERROR_MESSAGE );
	    $survey_id = $this->central->getargs( 'survey_id', $_POST, $corrupt );
	    $ajax = $this->central->getargs( 'ajax', $_POST, $corrupt );
	    if ( $ajax != '' )
	    {
		$survey_date = PluSQL::from( $this->profile )->survey_short_link->select( "*" )->where( "survey_id={$survey_id}" )->run()->survey_short_link;
		$data = array(
		    'short_link' => $survey_date->short_link,
		    'valid_date' => $survey_date->valid
		);
	    }
	    else
	    {
		$date = $this->central->getargs( 'date', $_POST, $corrupt );
		$short_link = $this->get_short_url( "http://app.brandvibe.io/?survey=" . $this->central->base64url_encode( $survey_id ) );
		$where = "survey_id = {$survey_id}";
		if (
			!$this->central->check_existance( $this->profile, 'survey_short_link', $where )
		)
		{
		    $data = array(
			'survey_id' => $survey_id,
			'short_link' => $short_link,
			'valid' => $date
		    );
		    if ( Plusql::into( $this->profile )->survey_short_link( $data )->insert() )
		    {
			$id = $this->central->get_accurate_last_id( 'survey_short_link', $this->profile );
		    }
		}
		else if ( $survey_id )
		{
		    $data = array(
			'short_link' => $short_link,
			'valid' => $date
		    );
		    Plusql::on( $this->profile )->survey_short_link( $data )->where( "survey_id = {$survey_id}" )->update();
		}
	    }
	    $return = array( 'success' => 1, 'error' => 0, 'data' => $data, 'message' => 'Link Generated' );
	}
	catch ( Exception $ex )
	{
	    
	}

	return $return;
    }

    private function get_short_url( $url = 'http://brandvive.io' )
    {
	$apiKey = 'AIzaSyCo-UVWvw3xrPkt1jJ2praurnhY6wWRE9Q';
	$postData = array( 'longUrl' => $url, 'key' => $apiKey );
	$jsonData = json_encode( $postData );
	$curlObj = curl_init();
	curl_setopt( $curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key=' . $apiKey );
	curl_setopt( $curlObj, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $curlObj, CURLOPT_SSL_VERIFYPEER, 0 );
	curl_setopt( $curlObj, CURLOPT_HEADER, 0 );
	curl_setopt( $curlObj, CURLOPT_HTTPHEADER, array( 'Content-type:application/json' ) );
	curl_setopt( $curlObj, CURLOPT_POST, 1 );
	curl_setopt( $curlObj, CURLOPT_POSTFIELDS, $jsonData );
	$response = curl_exec( $curlObj );
	$json = json_decode( $response );
	curl_close( $curlObj );

	return $json->id;
    }

    private function send_notification( $data = array() )
    {
	try
	{
	    if ( $data )
	    {
		$fields = array(
		    'Android' => array(
			'data' => array(
			    'title' => $data[ 'title' ],
			    'message' => $data[ 'message' ],
			),
		    ),
		);

		$filters = array(
		    'CreatedAt' => array(
			'$lte' => date( 'M j, Y, H:i A', strtotime( $data[ 'published_at' ] ) )
		    ),
		    'HardwareID' => array(
			'$elemMatch' => $this->get_devices()
		    )
		);
// print_r($this->get_devices())  . "----";
		$fields[ 'Filter' ] = $filters;
// print_r($filters) . "***";
//if ( $filters[ 'HardwareID' ][ '$elemMatch' ] )
		{
		    $ch = curl_init();
		    curl_setopt( $ch, CURLOPT_URL, 'http://api.everlive.com/v1/5m8cj6apa1k6kqgt/Push/Notifications' );
		    curl_setopt( $ch, CURLOPT_POST, true );
		    curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer=0kzzzhun63vvfokkk0zktjli2prnm3fw',
			//'X-Everlive-Filter: ' . json_encode( $filters ),
			'Content-Type: application/json'
		    ) );
		    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
		    $result = curl_exec( $ch );
		    curl_close( $ch );


//                    die(print_r($result,1));
		}
	    }
	}
	catch ( Exception $ex )
	{
// do nothing
	}
    }

    private function get_devices()
    {
	try
	{
	    $users = $this->get_user_count( true );
	    $devices = array();
	    $result = Plusql::from( $this->profile )->user->select( "*" )->where( "user_id IN ( " . join( ",", $users[ 'users' ] ) . ")" )->run()->user;
	    foreach ( $result as $row )
	    {
		if ( strlen( $row->device_id ) && !in_array( $row->device_id, $devices ) )
		    $devices[] = $row->device_id;
	    }
	}
	catch ( Exception $ex )
	{
// do nothing
	}

	return $devices;
    }

}

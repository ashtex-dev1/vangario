<?php

use \Config\Constants;
use \Config\Central;
use Config\URLs;

class Survey extends Config\RSBase
{

    // private members
    private $view = "";
    private $question_types = array(
	'short' => 'Short Answer',
	'long' => 'Paragraph',
	'checkbox' => 'Checkboxes',
	'radio' => 'Radios',
	'multi-image' => 'Multiple Select Image',
	'single-image' => 'Single Select Image',
	'rating-image' => 'Rating Image'
    );

    // constructor
    public function __construct()
    {
	try
	{
	    parent::__construct();
	    $this->view = $view = ( isset( $_GET[ 'view_type' ] ) && $_GET[ 'view_type' ] ) ? $_GET[ 'view_type' ] : 'view';
	    switch ( $view )
	    {
		case 'add':
		    $this->template = $this->central->load_normal( "pages/company/add_survey.html" );
		    $this->template->query( 'li.add-survey' )->item( 0 )->setAttribute( 'class', 'active add-survey' );
		    break;
		default :
		    $this->template = $this->central->load_normal( "pages/company/view_survey.html" );
		    $this->template->query( 'li.view-survey' )->item( 0 )->setAttribute( 'class', 'active view-survey' );
		    break;
	    }
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function update_main_contents()
    {
	try
	{
//	    phpinfo();
	    switch ( $this->view )
	    {
		case 'add':
		    $this->add_survey_view();
		    break;
		default:
		    $this->survey_view();
		    break;
	    }

	    $this->template->setValue( '#publish_survey_form@action', URLs::SURVEY . URLs::ACTION );
	    $this->template->setValue( '#publish_survey_form/div/#action@value', SurveyAction::PUBLISH );
	    $this->populate_dropdowns();
	}
	catch ( Exception $ex )
	{
	    // will do something here
	}
    }

    private function add_survey_view()
    {
	try
	{
	    $corrupt = false;
	    $survey_id = $this->central->getargs( "id", $_GET, $corrupt );
	    if ( !$corrupt )
	    {
		try
		{
		    $survey = PluSQL::from( $this->profile )->survey->question->question_options->select( "*, survey.title AS survey_title" )->where( "survey.survey_id = {$survey_id} AND survey.user_id = '" . $this->central->get_logged_in_user_id( $this->profile ) . "' AND survey.deleted <> 1 AND question.deleted <> 1 AND question_options.deleted <> 1" )->orderBy( 'question.question_id ASC' )->run()->survey;
		    $this->template->setValue( "#survey_id@value", $survey->survey->survey_id );
		    $this->template->setValue( "#survey-title@value", $this->central->string_formating( $survey->survey_title ) );
		    $this->setup_publish_actions( $this->template, $survey );
		    $item = $this->template->repeat( '.survey-question' );
		    foreach ( $survey->question as $question )
		    {
			$item->setValue( ".question_id@value", $question->question_id );
			$item->setValue( ".question-title@value", $this->central->string_formating( $question->title ) );
			$this->central->repeat_select_options( $item, '#question-type', $this->question_types, $question->type );
			$item->setValue( ".{$question->type}-section@style", "" );
			if ( in_array( $question->type, array( "radio", "checkbox", "single-image", "multi-image", "rating-image" ) ) )
			{
			    $sub_item = $item->repeat( ".{$question->type}-section/.multiple" );
			    foreach ( $question->question_options as $option )
			    {

				if ( $question->type == "single-image" || $question->type == "multi-image" || $question->type == "rating-image" )
				{
				    try
				    {
					$image_urls = PluSQL::from( $this->profile )->survey_option_image->select( "*" )->where( "survey_option_image.deleted <> 1 AND question_options_id={$option->question_options_id}" )->limit( "0, 1" )->run()->survey_option_image;
					foreach ( $image_urls as $image_url )
					    $sub_item->setValue( "#picture@src", $image_url->image_url );
				    }
				    catch ( Exception $ex )
				    {
					// do nothing
				    }
				    $sub_item->setValue( "#image-title@value", $this->central->string_formating( $option->label ) );
				    $sub_item->setValue( "#{$question->type}-option-title@value", $this->central->string_formating( $option->value ) );
				    $sub_item->setValue( "#{$question->type}-option-title@data-id", $option->question_options_id );
				}
				$sub_item->setValue( "#label", $option->label );
				$sub_item->setValue( "#{$question->type}@value", $this->central->string_formating( $option->value ) );
				$sub_item->setValue( "#{$question->type}@data-id", $option->question_options_id );
				$sub_item->setValue( "#{$question->type}@data-answer", $this->central->string_formating( $option->answer ) );
				$sub_item->next();
			    }
			    Central::remove_last_repeating_element( $sub_item, '.', 1, 0, 0 );
			}
			else
			{
			    $item->setValue( "#{$question->type}@data-id", $question->question_options->question_options_id );
			    $item->setValue( "#{$question->type}@data-answer", $question->question_options->answer );
			}
			if ( $question->status == "published" )
			{
			    $item->remove( ".add-more-button" );
			    $item->remove( ".remove-question" );
			    $this->template->remove( "#add-srv-question" );
			    $this->template->remove( "#save-survey" );
			}
			$item->next();
		    }
		    Central::remove_last_repeating_element( $item, '.', 1, 0, 0 );
		    $this->template->setValue( "#action@value", SurveyAction::UPDATE );
		}
		catch ( EmptySetException $ex )
		{
		    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
		}
		catch ( Exception $ex )
		{
		    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
		}
	    }
	    else
	    {
		$this->template->setValue( "#action@value", CompaniesAction::ADD );
		$this->template->remove( '#un-publish' );
		$this->template->remove( '#publish' );
		$this->central->repeat_select_options( $this->template, '#question-type', $this->question_types );
	    }
	    $this->template->setValue( "#survey_form@action", URLs::SURVEY . URLs::ACTION );
	}
	catch ( Exception $ex )
	{
	    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
	}
    }

    private function survey_view()
    {
	try
	{
	    $surveys = PluSQL::from( $this->profile )->survey->select( "*" )->where( "user_id = '" . $this->central->get_logged_in_user_id( $this->profile ) . "' AND deleted <> 1" )->run()->survey;
	    $this->template->remove( '.question-model' );
	    $item = $this->template->repeat( '.rep-surveys' );
	    foreach ( $surveys as $k => $survey )
	    {
		$item->setValue( '#title', $this->central->string_formating( $survey->title ) );
		$item->setValue( '#cnt', ( $k + 1 ) );
		$item->setValue( '#audience', $this->get_target_audience( $survey->survey_id ) );

		$item->setValue( "#preview@data-action", URLs::SURVEY . URLs::ACTION );
		$item->setValue( "#preview@data-method", SurveyAction::PREVIEW );
		$item->setValue( "#preview@data-id", $survey->survey_id );

		$item->setValue( "#short-link@data-action", URLs::SURVEY . URLs::ACTION );
		$item->setValue( "#short-link@data-method", SurveyAction::SHORT_LINK );
		$item->setValue( "#short-link@data-id", $survey->survey_id );

		$item->setValue( "#remove@data-id", $survey->survey_id );
		$item->setValue( "#remove@data-action", URLs::SURVEY . URLs::ACTION );
		$item->setValue( "#remove@data-method", SurveyAction::REMOVE );

		$item->setValue( "#clone@data-id", $survey->survey_id );
		$item->setValue( "#clone@data-action", URLs::SURVEY . URLs::ACTION );
		$item->setValue( "#clone@data-method", SurveyAction::CLONE_SURVEY );

		$item->setValue( '#edit@href', URLs::SURVEY . $this->central->encode_url_param( URLs::ADD_VIEW ) . $this->central->encode_url_param( ( str_replace( "%ID%", $survey->survey_id, URLs::ID ) ) ) );

		if ( $survey->status != 'published' )
		{
		    $item->remove( "#short-link" );
		}

		$this->setup_publish_actions( $item, $survey );
		$item->next();
	    }
	    Central::remove_last_repeating_element( $item, '.', 1, 0, 0 );
	}
	catch ( Exception $ex )
	{
	    $this->template->setValue( '.rep-surveys', '<td> No Record Found... </td><td></td><td></td><td></td><td></td>', 1 );
	}
    }

    private function setup_publish_actions( $item, $survey )
    {
	try
	{
	    $item->setValue( "#publish@data-id", $survey->survey_id );
	    $item->setValue( "#un-publish@data-id", $survey->survey_id );
	    $item->setValue( "#un-publish@data-action", URLs::SURVEY . URLs::ACTION );
	    $item->setValue( "#un-publish@data-method", SurveyAction::UNPUBLISH );
	    switch ( $survey->status )
	    {
		case Constants::STATUS_DRAFT:
		    $item->setValue( '#status', '<span class="label label-warning">Draft</span>', 1 );
		    $item->remove( '#un-publish' );
		    break;
		case Constants::STATUS_PUBLISHED:
		    $item->setValue( '#status', '<span class="label label-success">Published</span>', 1 );
		    $item->remove( '#publish' );
		    break;
		case Constants::STATUS_UNPUBLISHED:
		    $item->setValue( '#status', '<span class="label label-danger">Un-Published</span>', 1 );
		    $item->remove( '#un-publish' );
		    break;
	    }
	}
	catch ( Exception $ex )
	{
	    die( $ex->getMessage() );
	}
    }

    private function get_target_audience( $survey_id = 0 )
    {
	try
	{
	    $return = "-";
	    $result = Plusql::from( $this->profile )->publish->select( "*" )->where( "survey_id = {$survey_id} AND deleted <> 1" )->run()->publish;
	    $data = unserialize( stripcslashes( $result->data ) );
	    $return = $data[ 'audiance_count' ];
	}
	catch ( Exception $ex )
	{
	    // $return = Plusql::from( $this->profile )->publish->select( "*" )->where( "survey_id = {$survey_id} AND deleted <> 1" );
	}

	return $return;
    }

    private function populate_audience()
    {
	try
	{
	    $audiences = array();
	    $data = Plusql::from( $this->profile )->user->select( "*, user.email AS ctitle" )->where( 'role="' . Constants::USER_ROLE_USER . '"' )->orderBy( "user.email ASC" )->run()->user;
	    foreach ( $data as $row )
	    {
		$audiences[ $row->user->user_id ][ 'options' ][] = ucwords( $row->ctitle );
	    }
	}
	catch ( Exception $ex )
	{
	    $audiences = array( array(
		    'label' => 'No audience available',
		    'options' => array(),
		) );
	}

	$this->central->repeat_select_options_with_optgroups( $this->template, "#audiences", $audiences, array() );
    }

    private function populate_dropdowns()
    {
	try
	{
	    $categories = array();
	    $data = Plusql::from( $this->profile )->survey_categories->fb_pages->select( "*, survey_categories.title AS ctitle" )->orderBy( 'survey_categories.title ASC' )->run()->survey_categories;
	    foreach ( $data as $row )
	    {
		$categories[ $row->survey_categories->survey_categories_id ] = array(
		    'label' => $this->central->string_formating( ucwords( $row->ctitle ) ),
		    'options' => array(),
		);
		foreach ( $row->fb_pages as $page )
		{
		    $categories[ $row->survey_categories->survey_categories_id ][ 'options' ][ $page->fb_pages_id ] = $this->central->string_formating( $page->title );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $categories = array( array(
		    'label' => 'No option available',
		    'options' => array(),
		) );
	}

	$this->central->repeat_select_options_with_optgroups( $this->template, "#demographics", $categories, array() );

	$this->populate_location();
	$this->populate_relationship();
	$this->populate_education();
    }

    private function populate_location()
    {
	try
	{
	    $locations = array();
	    foreach ( Central::get_unique_entries( $this->profile, 'user_details', 'home_town' ) as $key => $value )
	    {
		if ( strlen( $value ) )
		    $locations[ $key ] = $this->central->string_formating( ucwords( $value ) );
		else
		    $locations[ 'not-specified' ] = "Not specified";
	    }
	}
	catch ( Exception $ex )
	{
	    $locations = array( '0' => 'No location available', );
	}

	$this->central->repeat_select_options( $this->template, "#location", $locations );
    }

    private function populate_relationship()
    {
	try
	{
	    $relationships = array();
	    foreach ( Central::get_unique_entries( $this->profile, 'user_details', 'relationship_status' ) as $key => $value )
	    {
		$relationships[ $key ] = $this->central->string_formating( ucwords( $value ) );
	    }
	}
	catch ( Exception $ex )
	{
	    $relationships = array( '0' => 'No relationship type available', );
	}

	$this->central->repeat_select_options( $this->template, "#relationship", $relationships );
    }

    private function populate_age()
    {

	return false;
	/*
	  try
	  {
	  $ages = array();
	  foreach ( Central::get_age( $this->profile ) as $ag )
	  {
	  $ages[ $ag ][ 'options' ][] = $ag;
	  }
	  }
	  catch ( Exception $ex )
	  {
	  $ages = array( array(
	  'label' => 'No age available',
	  ) );
	  }
	  $this->central->repeat_select_options_with_optgroups( $this->template, "#age", $ages, array() );
	 */
    }

    private function populate_education()
    {
	try
	{
	    $education = array();
	    $results = Plusql::from( $this->profile )->user_education->select( "*" )->orderBy( "type ASC" )->run()->user_education;
	    foreach ( $results as $row )
	    {
		$education[ $row->type ][ 'label' ] = $this->central->string_formating( ucwords( $row->type ) );
		$education[ $row->type ][ 'options' ][ $row->institute ] = $this->central->string_formating( ucwords( $row->institute ) );
	    }
	}
	catch ( Exception $ex )
	{
	    $education = array( array(
		    'label' => 'No education available',
		    'options' => array(),
		) );
	}

	$this->central->repeat_select_options_with_optgroups( $this->template, "#education", $education, array() );
    }

    private function populate_language()
    {
	try
	{
	    $languages = array();
	    $language = Plusql::from( $this->profile )->user->user_details->select( "*, user_details.language AS ctitle" )->where( 'role="' . Constants::USER_ROLE_USER . '"' )->orderBy( 'user_details.language ASC' )->run()->user_details;
	    foreach ( $laguage as $languag )
	    {
//                $languages[$languag->user_details->user_id] = array(
//                    'label' => ucwords($languag->ctitle),
//                );
		$languages[ $languag->user_details->user_id ][ 'options' ][] = ucwords( $languag->ctitle );
	    }
	}
	catch ( Exception $ex )
	{
	    $languages = array( array(
		    'label' => 'No laguage available',
		) );
	}
	$this->central->repeat_select_options_with_optgroups( $this->template, "#language", $languages, array() );
    }

    private function messages()
    {
	if ( isset( $_SESSION[ 'admin_company' ][ 'inserted' ] ) )
	{
	    $this->template->setValue( '.cominscs@style', 'display:block' );
	    $this->template->setValue( '#cominsuccmsg', 'Company has been Created Successfully.' );
	    unset( $_SESSION[ 'admin_company' ][ 'inserted' ] );
	}
	if ( isset( $_SESSION[ 'admin_company' ][ 'deleted' ] ) )
	{
	    $this->template->setValue( '.comdelscs@style', 'display:block' );
	    $this->template->setValue( '#comindelmsg', 'Company has been Deleted Successfully.' );
	    unset( $_SESSION[ 'admin_company' ][ 'deleted' ] );
	}
	if ( isset( $_SESSION[ 'admin_company' ][ 'updated' ] ) )
	{
	    $this->template->setValue( '.cominscs@style', 'display:block' );
	    $this->template->setValue( '#cominsuccmsg', 'Company has been Updated Successfully.' );
	    unset( $_SESSION[ 'admin_company' ][ 'updated' ] );
	}
    }

}

?>
<?php

@session_start();

use Config\Constants;
use Config\Central;
use Config\URLs;

class Login implements RocketSled\Runnable
{

    //--private members
    private $file_name = "pages/user/login.html";
    private $profile = "login";
    private $template;
    private $central;

    //--constructor
    public function __construct()
    {
	try
	{
	    $this->central = Central::instance();
	    // $this->check_new_user_account();
	    $this->central->set_alias_connection( $this->profile );
	    $this->template = $this->central->load_normal( $this->file_name );
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function run()
    {
	try
	{
	    $this->check_logout_request();
	    if ( !$this->central->check_user_login_status( $this->profile ) )
		$this->render();
	    else
		@header( 'location: ' . URLs::DASHBOARD );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    private function render( $display = TRUE )
    {
	try
	{
	    $this->update_main_contents();
	    if ( $display )
		$this->central->render( $this->template );
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    public function update_main_contents()
    {
	try
	{
	    $this->template->setValue( "#log_in_form@action", URLs::LOGIN . URLs::ACTION );
	    $this->template->setValue( "#signup_a@href", URLs::REGISTER );
	    $this->template->setValue( "#forgot_password@href", URLs::FORGET_PASSWORD );
	    // $this->template->setValue( ".signin@href", "javascript:void(0);" );
	    if ( isset( $_SESSION[ "success" ] ) && isset( $_SESSION[ "success" ][ "signup" ] ) && $_SESSION[ "success" ] )
	    {
		$this->template->setValue( ".register_success@style", "color: green;" );
		unset( $_SESSION[ "success" ] );
		@session_destroy();
	    }
	    else if ( isset( $_SESSION[ "error" ] ) && $_SESSION[ "error" ] )
	    {

		$this->show_login_errors();
		unset( $_SESSION[ "error" ] );
	    }
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    private function check_logout_request()
    {
	try
	{
	    $corrupt = false;
	    $logout = $this->central->getargs( 'logout', $_GET, $corrupt );
	    if ( !$corrupt )
	    {
		if ( strcmp( $logout, 'yes' ) === 0 )
		{
		    unset( $_SESSION[ 'user' ] );
		    unset( $_COOKIE[ 'user' ] );
		    setcookie( 'user', null, -1, '/' );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

    private function check_new_user_account()
    {
	try
	{
	    $corrupt = false;
	    $logout = $this->central->getargs( Central::base64url_encode( 'new_account' ), $_GET, $corrupt );
	    if ( !$corrupt )
	    {
		if ( Central::base64url_decode( $logout ) == 'yes' )
		{
		    unset( $_SESSION[ "user" ] );
		    unset( $_SESSION );
		    @session_destroy();
		}
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

    private function show_login_errors()
    {
	try
	{
	    if ( isset( $_SESSION[ 'error' ] ) )
	    {
		if ( $_SESSION[ 'error' ][ 'email' ] )
		{
		    $this->template->setValue( '.user_name_error@style', 'display:block' );
		}
		else
		{
		    $this->template->setValue( '.user_name_error@style', 'display:none' );
		}
		if ( $_SESSION[ 'error' ][ 'password' ] )
		{
		    $this->template->setValue( '.password_error@style', 'display:block' );
		}
		else
		{
		    $this->template->setValue( '.password_error@style', 'display:none' );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

}

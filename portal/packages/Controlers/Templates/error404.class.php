<?php

use \Config\Constants;
use \Config\Central;

class Error404 extends Config\RSBase
{

    //--private members
    private $file_name = "pages/error/404.html";

    //--constructor
    public function __construct()
    {
	try
	{
	    parent::__construct();
	    $this->template = $this->central->load_normal( $this->file_name );
	    $this->register_args();
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    //register args
    private function register_args()
    {
	
    }

    public function update_main_contents()
    {
	try
	{
	    
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

}

?>
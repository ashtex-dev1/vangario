<?php

use \Config\Constants;
use \Config\Central;
use Config\URLs;

class Companies extends Config\RSBase
{

    // private members
    private $view = "";

    // constructor
    public function __construct()
    {
	try
	{
	    parent::__construct();
	    $this->view = $view = ( isset( $_GET[ 'view_type' ] ) && $_GET[ 'view_type' ] ) ? $_GET[ 'view_type' ] : 'view';
	    switch ( $view )
	    {
		case 'add':
		    $this->template = $this->central->load_normal( "pages/admin/add_company.html" );
		    $this->template->query( 'li.add-company' )->item( 0 )->setAttribute( 'class', 'active add-company' );
		    break;
		default :
		    $this->template = $this->central->load_normal( "pages/admin/view_companies.html" );
		    $this->template->query( 'li.view-company' )->item( 0 )->setAttribute( 'class', 'active view-company' );
		    break;
	    }
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    public function update_main_contents()
    {
	try
	{
	    switch ( $this->view )
	    {
		case 'add':
		    $this->add_companies_view();
		    break;
		default:
		    $this->companies_view();
		    break;
	    }
	}
	catch ( Exception $ex )
	{
	    // will do something here
	}
    }

    private function add_companies_view()
    {
	try
	{
	    $corrupt = false;
	    $company_id = $this->central->getargs( "id", $_GET, $corrupt );
	    $options = array(
		Constants::STATUS_ACTIVE => 'Active',
		Constants::STATUS_INACTIVE => 'Inactive',
	    );
	    $status = Constants::STATUS_INACTIVE;
	    if ( !$corrupt )
	    {
		try
		{
		    $company = Plusql::from( $this->profile )->user->select( "*" )->where( "user_id = {$company_id} AND role = '" . Constants::USER_ROLE_COMPANY . "' AND status IN ( '" . Constants::STATUS_ACTIVE . "', '" . Constants::STATUS_INACTIVE . "' )" )->limit( "0, 1" )->run()->user;
		    $details = unserialize( $company->data );
		    $status = $company->status;
		    if ( strcmp( $status, Constants::STATUS_ACTIVE ) === 0 )
		    {
			$this->template->query( "#password" )->item( 0 )->setAttribute( "disabled", "true" );
			$this->template->setValue( "#password@value", "123456789" );
		    }
		    $this->template->setValue( "#company_id@value", $company->user_id );
		    $this->template->setValue( "#company_name@value", $company->user_name );
		    $this->template->setValue( "#email@value", $details[ 'email' ] );
		    $this->template->setValue( "#contact_no@value", $details[ 'contact_no' ] );
		    $this->template->setValue( "#office_no@value", $details[ 'office_no' ] );
		    $this->template->setValue( "#city@value", $details[ 'city' ] );
		    $this->template->setValue( "#state@value", $details[ 'state' ] );
		    $this->template->setValue( "#country@value", $details[ 'country' ] );
		    $this->template->setValue( "#address", $details[ 'address' ] );
		    $this->template->setValue( "#notes", $details[ 'notes' ] );
		    $this->template->setValue( "#action@value", CompaniesAction::UPDATE );
		}
		catch ( EmptySetException $ex )
		{
		    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
		}
		catch ( Exception $ex )
		{
		    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
		}
	    }
	    else
	    {
		$this->template->setValue( "#action@value", CompaniesAction::ADD );
	    }
	    $this->template->setValue( "#company_form@action", URLs::COMPANIES . URLs::ACTION );
	    $this->central->repeat_select_options( $this->template, "#status", $options, $status );
	}
	catch ( Exception $ex )
	{
	    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
	}
    }

    private function companies_view()
    {
	try
	{
	    $companies_lists = PluSQL::from( $this->profile )->user->select( "*" )->where( "user.role = '" . Constants::USER_ROLE_COMPANY . "' AND user.deleted <> 1" )->run()->user;
	    $item = $this->template->repeat( '.rep-companies' );
	    foreach ( $companies_lists as $k => $companies_list )
	    {
		$data = unserialize( $companies_list->data );
		$item->setValue( '#company_name', $companies_list->user_name );
		$item->setValue( '#company_id@value', $companies_list->user_id );
		$item->setValue( '#company_email', $companies_list->email );
                $item->setValue( '#company_info', $data['contact_no'] );
		$item->setValue( '#edit@href', URLs::COMPANIES . $this->central->encode_url_param( URLs::ADD_VIEW ) . $this->central->encode_url_param( ( str_replace( "%ID%", $companies_list->user_id, URLs::ID ) ) ) );
		$item->setValue( "#remove@data-action", URLs::COMPANIES . URLs::ACTION );
		$item->setValue( "#remove@data-method", CompaniesAction::REMOVE );
		$item->setValue( "#remove@data-id", $companies_list->user_id );
		$item->setValue( '#cnt', ( $k + 1 ) );
		$item->next();
	    }
	    Central::remove_last_repeating_element( $item, '.', 1, 0, 0 );
	}
	catch ( Exception $ex )
	{
	    $this->template->setValue( '.rep-companies', '<td> No Record Found... </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>', 1 );
	}
    }

    private function messages()
    {
	if ( isset( $_SESSION[ 'admin_company' ][ 'inserted' ] ) )
	{
	    $this->template->setValue( '.cominscs@style', 'display:block' );
	    $this->template->setValue( '#cominsuccmsg', 'Company has been Created Successfully.' );
	    unset( $_SESSION[ 'admin_company' ][ 'inserted' ] );
	}
	if ( isset( $_SESSION[ 'admin_company' ][ 'deleted' ] ) )
	{
	    $this->template->setValue( '.comdelscs@style', 'display:block' );
	    $this->template->setValue( '#comindelmsg', 'Company has been Deleted Successfully.' );
	    unset( $_SESSION[ 'admin_company' ][ 'deleted' ] );
	}
	if ( isset( $_SESSION[ 'admin_company' ][ 'updated' ] ) )
	{
	    $this->template->setValue( '.cominscs@style', 'display:block' );
	    $this->template->setValue( '#cominsuccmsg', 'Company has been Updated Successfully.' );
	    unset( $_SESSION[ 'admin_company' ][ 'updated' ] );
	}
    }

}

?>

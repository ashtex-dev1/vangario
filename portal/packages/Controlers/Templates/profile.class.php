<?php

use \Config\Constants;
use \Config\Central;
use Config\URLs;

class Profile extends Config\RSBase
{

    //--private members
    private $view_file = "pages/user/profile.html";

    //--constructor
    public function __construct()
    {
	try
	{
	    parent::__construct();
	    $this->template = $this->central->load_normal( $this->view_file );
	    $this->register_args();
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

    //register args
    private function register_args()
    {
	
    }

    public function update_main_contents()
    {
	try
	{
	    $this->template->setValue( "#profile_form@action", URLs::PROFILE . URLs::ACTION );
	    $user = Plusql::from( $this->profile )->user->select( "*" )->where( "user_id = {$_SESSION[ 'user' ][ 'user_id' ]}" )->limit( "0, 1" )->run()->user;
	    $this->template->setValue( "#name@value", $user->user_name );
	    $this->template->setValue( "#email@value", $user->email );
	}
	catch ( EmptySetException $ex )
	{
	    // do nothing
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

}

?>
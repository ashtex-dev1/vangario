<?php

use \Config\Constants;
use \Config\Central;
use Config\URLs;

class Reports extends Config\RSBase
{

    private $survey_id = 0;

    // --constructor
    public function __construct()
    {
	try
	{
	    parent::__construct();
	    $this->view = $view = ( isset( $_GET[ 'view_type' ] ) && $_GET[ 'view_type' ] ) ? $_GET[ 'view_type' ] : 'view';
	    switch ( $view )
	    {
		case 'detail':
		    $this->template = $this->central->load_normal( "pages/company/analysis.html" );
		    break;
		default :
		    $this->template = $this->central->load_normal( "pages/company/reports.html" );
		    break;
	    }

	    $this->template->query( 'li.general-report' )->item( 0 )->setAttribute( 'class', 'active general-report' );
	    $this->register_args();
	}
	catch ( Exception $e )
	{
	    throw $e;
	}
    }

	public function __call( $closure, $argv )
    	{
		$escape = Plusql::escape( $this->profile );
		return $escape( $argv[ 0 ] );
    	}

    //register args
    private function register_args()
    {
	
    }

    public function update_main_contents()
    {
	try
	{
	    switch ( $this->view )
	    {
		case 'detail':
		    $this->survey_detailed_analysis();
		    $this->populate_filters();
		    break;
		default:
		    $this->list_surveys();
		    break;
	    }
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    private function survey_detailed_analysis()
    {
	try
	{
	    $corrupt = false;
	    $this->survey_id = $this->central->getargs( "id", $_GET, $corrupt );
	    if ( !$corrupt )
	    {
		$columns = Statistics::get_columns( $this->profile, $this->survey_id );
		$this->template->setValue( '.export@data-action', URLs::GENERATE_EXCEL . $this->central->encode_url_param( ( str_replace( "%ID%", $this->survey_id, URLs::ID ) ) ) );
		$item = $this->template->repeat( '.title-columns' );
		foreach ( $columns as $column )
		{
		    $item->setValue( 'span', is_array( $column ) ? $this->central->string_formating( ucwords( $column[ 'title' ] )) : $this->central->string_formating(ucwords( $column ))  );
		    $item->next();
		}
		Central::remove_last_repeating_element( $item, '.', 1, 0, 0 );

		$this->list_users_data( $columns );
		$this->populate_questions_chart( $columns );
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

    private function list_users_data( $columns )
    {
	try
	{
	    $count = 1;
	    $results = Statistics::get_respondents( $this->profile, $this->survey_id );
	    $item = $this->template->repeat( '.repeat-users' );
	    if ( $results )
		while ( $row = $results->nextRow() )
		{
		    $sub_item = $item->repeat( '.column' );
		    foreach ( $columns as $column )
		    {
			if ( !is_array( $column ) )
			{
			    $sub_item->setValue( 'span', $column === '#' ? $count : ucwords( $row[ $column ] )  );
			}
			else
			{
			    switch ( $column[ 'table' ] )
			    {
				case 'user_education':
				    $sub_item->setValue( 'span', Statistics::get_user_education( $this->profile, $row[ 'uid' ] ), 1 );
				    break;
				case 'user_interests':
				    $sub_item->setValue( 'span', Statistics::get_user_interests( $this->profile, $row[ 'uid' ], $this->survey_id ), 1 );
				    break;
				case 'question':
				    $sub_item->setValue( 'span', Statistics::get_user_answers( $this->profile, $this->survey_id, $row[ 'uid' ], $column[ 'data' ]->question_id ), 1 );
				    break;
			    }
			}
			$sub_item->next();
		    }
		    Central::remove_last_repeating_element( $sub_item, '.', 1, 0, 0 );
		    $count++;
		    $item->next();
		}
	    if ( $count > 1 )
		Central::remove_last_repeating_element( $item, '.', 1, 0, 0 );
	    else
		$this->template->setValue( '.repeat-users', '<td colspan="' . count( $columns ) . '">No results found...</td>', 1 );
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

    private function populate_filters()
    {
	try
	{
	    $corrupt = false;
	    $survey_id = $this->central->getargs( "id", $_GET, $corrupt );
	    if ( !$corrupt )
	    {
		$survey_options = $this->central->check_existance( $this->profile, "publish", "survey_id = {$survey_id}" );
		if ( $survey_options )
		{
		    $this->template->setValue( "#report-survey-id@value", $survey_id );
		    $this->template->setValue( "#report-survey-action@value", $_GET[ 'reports/' ] );

		    $age_brackets = array( "Select an option" );
		    foreach ( explode( ",", $survey_options->age )as $age )
		    {
			$age = trim( $age );
			if ( strlen( $age ) )
			    $age_brackets[ $age ] = ucwords( $age );
		    }

		    $locations = array( "Select an option" );
		    foreach ( explode( "~", $survey_options->locations )as $location )
		    {
			$location = trim( $location );
			if ( strlen( $location ) )
			    $locations[ $location ] = $this->central->string_formating(ucwords( $location ));
		    }
		    $relationships = array( "Select an option" );
		    foreach ( explode( ",", $survey_options->relationship_types )as $relationship )
		    {
			$relationship = trim( $relationship );
			if ( strlen( $relationship ) )
			    $relationships[ $relationship ] = $this->central->string_formating(ucwords( $relationship ));
		    }

		    $genders = array( '0' => 'All', 'male' => 'Male', 'female' => 'Female', );
		    Central::repeat_select_options( $this->template, "#age", $age_brackets, $this->central->getargs( "age", $_POST, $corrupt, 0 ) );
		    Central::repeat_select_options( $this->template, "#gender", $genders, $this->central->getargs( "gender", $_POST, $corrupt, 'all' ) );
		    Central::repeat_select_options( $this->template, "#location", $locations, $this->central->getargs( "location", $_POST, $corrupt, 0 ) );
		    Central::repeat_select_options( $this->template, "#relationship", $relationships, $this->central->getargs( "relationship", $_POST, $corrupt, 0 ) );
		    $this->populate_education_dropdown_and_chart( $survey_options );
		    $this->populate_location_chart( $locations );
		    $this->populate_relationship_chart( $relationships );
		    $this->populate_age_and_gender_chart( $age_brackets );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    // do nothing
	}
    }

    private function populate_education_dropdown_and_chart( $survey_options )
    {
	try
	{
	    $in = "'0'";
	    $education = array();
	    foreach ( explode( ",", $survey_options->education )as $institute )
	    {
		$institute = trim( $institute );
		if ( strlen( $institute ) )
		{
		    $in .= ", '{$institute}'";
		}
	    }

	    $results = Plusql::from( $this->profile )->user_education->select( "*" )->where( "institute IN ( {$in} )" )->orderBy( "type ASC" )->run()->user_education;
	    foreach ( $results as $row )
	    {
		$education[ $row->type ][ 'label' ] = ucwords( $row->type );
		$education[ $row->type ][ 'options' ][ $row->institute ] = $this->central->string_formating(ucwords( $row->institute ));
	    }
	    $stats = $data = array();
	    $education_stats = Statistics::get_respondents_education( $this->profile, $this->survey_id, $in );

	    if ( $education_stats )
	    {
		while ( $row = $education_stats->nextRow() )
		{
		    if ( !in_array( $row[ 'type' ], $stats[ 'categories' ][ 'titles' ] ) )
			$stats[ 'categories' ][ 'titles' ][] = $row[ 'type' ];

		    if ( !in_array( $row[ 'institute' ], $stats[ 'intitutes' ][ $row[ 'type' ] ][ 'titles' ] ) )
			$stats[ 'intitutes' ][ $row[ 'type' ] ][ 'titles' ][] = $this->central->string_formating($row[ 'institute' ]);

		    $stats[ 'categories' ][ 'count' ][ $row[ 'type' ] ] ++;
		    $stats[ 'intitutes' ][ 'count' ][ $row[ 'institute' ] ] ++;
		}

		if ( $stats )
		{
		    $totals = array(
			'categories' => array_sum( $stats[ 'categories' ][ 'count' ] ),
			'intitutes' => array_sum( $stats[ 'intitutes' ][ 'count' ] ),
		    );

		    foreach ( $stats[ 'categories' ][ 'count' ] as $index => $counts )
		    {
			$stats[ 'categories' ][ 'count' ][ $index ] = ($counts / $totals[ 'categories' ]) * 100;
		    }

		    foreach ( $stats[ 'intitutes' ][ 'count' ] as $index => $counts )
		    {
			$stats[ 'intitutes' ][ 'count' ][ $index ] = ($counts / $totals[ 'intitutes' ]) * 100;
		    }

		    foreach ( $stats[ 'categories' ][ 'titles' ] as $k => $category )
		    {
			$data[ 'series' ][ $k ] = array(
			    'name' => $category,
			    'y' => $stats[ 'categories' ][ 'count' ][ $category ],
			    'drilldown' => $category,
			);

			$data[ 'drilldowns' ][ $k ] = array(
			    'name' => $category,
			    'id' => $category,
			    'innerSize' => '40%',
			    'showInLegend' => true,
			    'dataLabels' => array(
				'enabled' => false,
				'padding' => 0,
			    ),
			);
			foreach ( $stats[ 'intitutes' ][ $category ][ 'titles' ] as $institute )
			{
			    $data[ 'drilldowns' ][ $k ][ 'data' ][] = array(
				'name' => $institute,
				'y' => ($stats[ 'intitutes' ][ 'count' ][ $institute ] / array_sum( $stats[ 'intitutes' ][ 'count' ] )) * 100,
			    );
			}
		    }
		}
		else
		{
		    $data = array( 'series' => array(), 'drilldowns' => array() );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    $data = array( 'series' => array(), 'drilldowns' => array() );
	    $education = array( array(
		    'label' => 'No education available',
		    'options' => array(),
		) );
	}
	$this->template->setValue( '.education-chart/input@value', json_encode( $data ) );
	$this->central->repeat_select_options_with_optgroups( $this->template, "#education", $education, $this->central->getargs( "education", $_POST, $corrupt, 0 ) );
    }

    private function populate_location_chart( $locations )
    {
	try
	{
	    $in = "'0'";
	    foreach ( $locations as $location )
	    {
		$in .= ", '{$location}'";
	    }

	    $stats = array( 'series' => array() );
	    $results = Statistics::get_respondents_locations( $this->profile, $this->survey_id, $in );
	    if ( $results )
	    {
		$temp = array();
		while ( $row = $results->nextRow() )
		{
		    if ( !in_array( $row[ 'home_town' ], $temp[ 'titles' ] ) )
			$temp[ 'titles' ][] = $this->central->string_formating($row[ 'home_town' ]);

		    $temp[ 'counts' ][ $row[ 'home_town' ] ] ++;
		}

		if ( $temp )
		    foreach ( $temp[ 'titles' ] as $location )
		    {
			$stats[ 'series' ][] = array(
			    'name' => ucwords( $location ),
			    'y' => ($temp[ 'counts' ][ $location ] / array_sum( $temp[ 'counts' ] ) ) * 100,
			);
		    }
	    }
	}
	catch ( Exception $ex )
	{
	    // will do something here
	}

	$this->template->setValue( '.location-chart/input@value', json_encode( $stats ) );
    }

    private function populate_relationship_chart( $relationships )
    {
	try
	{
	    $in = "'0'";
	    foreach ( $relationships as $relationship )
	    {
		$in .= ", '{$relationship}'";
	    }

	    $stats = array( 'series' => array() );
	    $results = Statistics::get_respondents_relationships( $this->profile, $this->survey_id, $in );
	    if ( $results )
	    {
		$temp = array();
		while ( $row = $results->nextRow() )
		{
		    if ( !in_array( $row[ 'relationship_status' ], $temp[ 'titles' ] ) )
			$temp[ 'titles' ][] = $this->central->string_formating($row[ 'relationship_status' ]);

		    $temp[ 'counts' ][ $row[ 'relationship_status' ] ] ++;
		}

		if ( $temp )
		    foreach ( $temp[ 'titles' ] as $relationship )
		    {
			$stats[ 'series' ][] = array(
			    'name' => ucwords( $relationship ),
			    'y' => ($temp[ 'counts' ][ $relationship ] / array_sum( $temp[ 'counts' ] ) ) * 100,
			);
		    }
	    }
	}
	catch ( Exception $ex )
	{
	    // will do something here
	}

	$this->template->setValue( '.relationship-chart/input@value', json_encode( $stats ) );
    }

    private function populate_age_and_gender_chart( $ages )
    {
	try
	{
	    $where = '( ';
	    foreach ( $ages as $age )
	    {
		if ( strpos( $age, '-' ) )
		{
		    list($min, $max) = explode( '-', $age );
		    $where .= "ud.age BETWEEN {$min} AND {$max} OR ";
		}
		else if ( strpos( $age, '+' ) )
		    $where .= "ud.age >= " . explode( '+', $age )[ 0 ] . " OR ";
	    }

	    $where = rtrim( $where, "OR " );
	    $where .= ' )';
	    $stats = array( 'bars' => array(), 'pie' => array(), );
	    $results = Statistics::get_respondents_age( $this->profile, $this->survey_id, $where );
	    if ( $results )
	    {
		$temp = array();
		while ( $row = $results->nextRow() )
		{
		    $temp[ 'ages' ][ 'titles' ] = array( '10-20', '20-30', '30-40', '40-50', '50+', );
		    $row[ 'gender' ] = strlen( $row[ 'gender' ] ) ? $row[ 'gender' ] : "Not mentioned";
		    if ( !in_array( $row[ 'gender' ], $temp[ 'gender' ][ 'titles' ] ) )
			$temp[ 'gender' ][ 'titles' ][] = $row[ 'gender' ];

		    if ( $row[ 'age' ] >= 10 && $row[ 'age' ] < 20 )
			$temp[ 'counts' ][ '10-20' ][ $row[ 'gender' ] ] ++;
		    else if ( $row[ 'age' ] >= 20 && $row[ 'age' ] < 30 )
			$temp[ 'counts' ][ '20-30' ][ $row[ 'gender' ] ] ++;
		    else if ( $row[ 'age' ] >= 30 && $row[ 'age' ] < 40 )
			$temp[ 'counts' ][ '30-40' ][ $row[ 'gender' ] ] ++;
		    else if ( $row[ 'age' ] >= 40 && $row[ 'age' ] < 50 )
			$temp[ 'counts' ][ '40-50' ][ $row[ 'gender' ] ] ++;
		    else
			$temp[ 'counts' ][ '50+' ][ $row[ 'gender' ] ] ++;
		}

		if ( $temp )
		{
		    foreach ( $temp[ 'gender' ][ 'titles' ] as $k => $gender )
		    {
			$stats[ 'bars' ][ $k ] = array(
			    'type' => 'column',
			    'name' => ucwords( $gender ),
			);

			foreach ( $temp[ 'ages' ][ 'titles' ] as $age_group )
			{
			    $stats[ 'bars' ][ $k ][ 'data' ][] = $temp[ 'counts' ][ $age_group ][ $gender ] ? $temp[ 'counts' ][ $age_group ][ $gender ] : 0;
			}

			$stats[ 'pie' ][ $k ] = array(
			    'name' => ucwords( $gender ),
			    'y' => array_sum( $stats[ 'bars' ][ $k ][ 'data' ] ),
			);
		    }

		    $stats[ 'bars' ][] = array(
			'type' => 'pie',
			'name' => 'Gender',
			'data' => $stats[ 'pie' ],
			'center' => array( 40, 0 ),
			'size' => 80,
			'showInLegend' => false,
			'dataLabels' => array(
			    'enabled' => false
			),
		    );

		    unset( $stats[ 'pie' ] );
		}
	    }
	}
	catch ( Exception $ex )
	{
	    // will do something here
	}

	$this->template->setValue( '.age-chart/input@value', json_encode( $stats ) );
    }

    private function populate_questions_chart( $columns )
    {
	try
	{
	    $item = $this->template->repeat( '.question-chart' );
	    foreach ( $columns as $column )
	    {
		if (
			is_array( $column ) && isset( $column[ 'table' ] ) &&
			strcmp( $column[ 'table' ], 'question' ) === 0 && $column[ 'data' ]
		)
		{
		    $data = array();
		    $question = $column[ 'data' ];
		    if (
			    strcmp( $question->type, "short" ) !== 0 &&
			    strcmp( $question->type, "long" ) !== 0
		    )
		    {
			$data[ 'title' ] = $question->qtitle;
			$temp = array( 'wrong' => array(), 'correct' => array() );
			foreach ( $question->question_options as $option )
			{
			    if ( strlen( $option->label ) )
			    {
				$data[ 'category' ][] = $this->central->string_formating(ucwords( $option->label ));
				try
				{
				    // $question_response = PluSQL::from( $this->profile )->survey_response_answers->select( "*, COUNT(*) AS total" )->where( "question_options_id='{$option->question_options_id}'" )->run()->survey_response_answers;
				    $data[ 'options' ][ 0 ][ 'data' ][] = ( int ) Statistics::get_respondents_answers_count( $this->profile, $option->question_options_id ); // rand( 110, 200 );
				    $data[ 'options' ][ 0 ][ 'dataLabels' ] = array( 'enabled' => false );
				    $data[ 'options' ][ 0 ][ 'showInLegend' ] = false;
				}
				catch ( Exception $ex )
				{
				    $data[ 'options' ][] = array(
					'name' => $this->central->string_formating(ucwords( $option->label )),
					'data' => array(),
				    );
				}
			    }
			}

			$item->setValue( "input@value", json_encode( $data ) );
			$item->setValue( ".container@id", "container-{$question->question_id}" );
			$item->next();
		    }
		}
	    }
	    Central::remove_last_repeating_element( $item, '.', 1, 0, 0 );
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    private function list_surveys()
    {

	try
	{
	    $company_surveys_lists = PluSQL::from( $this->profile )->survey->select( "*" )->where( "user_id='{$_SESSION[ 'user' ][ 'user_id' ]}' AND " . Constants::STATUS_DELETED . " <> 1 AND status<>'" . Constants::STATUS_DRAFT . "'" )->run()->survey;
	    $repeat = $this->template->repeat( '.rep-company-survey' );
	    foreach ( $company_surveys_lists as $k => $company_surveys_list )
	    {
		$repeat->setValue( '.count', ( $k + 1 ) );
		$repeat->setValue( '.survey-name', $this->central->string_formating($company_surveys_list->title) );
		$repeat->setValue( '.status', $company_surveys_list->status );

		switch ( $company_surveys_list->status )
		{
		    case Constants::STATUS_PUBLISHED:
			$repeat->setValue( '#status', '<span class="label label-success">Published</span>', 1 );
			$repeat->setValue( '.publish-date', strlen( $company_surveys_list->published_at ) ? $company_surveys_list->published_at : "N/A"  );
			$repeat->remove( '#publish' );
			break;
		    case Constants::STATUS_UNPUBLISHED:
			$repeat->setValue( '.publish-date', strlen( $company_surveys_list->updated_at ) ? $company_surveys_list->updated_at : "N/A"  );
			$repeat->setValue( '#status', '<span class="label label-danger">Un-Published</span>', 1 );
			$repeat->remove( '#un-publish' );
			break;
		}
		$repeat->setValue( '.view-surveys@href', URLs::REPORTS . $this->central->encode_url_param( URLs::DETAIL_VIEW ) . $this->central->encode_url_param( ( str_replace( "%ID%", $company_surveys_list->survey_id, URLs::ID ) ) ) );
		$repeat->next();
	    }
	    Central::remove_last_repeating_element( $repeat, '.', 1, 0, 0 );
	}
	catch ( Exception $ex )
	{
	    $this->template->setValue( '.rep-company-survey', '<td> No Record Found... </td><td></td><td></td><td></td><td></td>', 1 );
	}
    }

}

?>

<?php

use Config\Central;
use Config\Constants;
use Config\URLs;

class Dashboard extends Config\RSBase
{

    private $file_name = "";

    public function __construct()
    {
	try
	{
	    parent::__construct();
	    switch ( Central::get_user_role() )
	    {
		case Constants::USER_ROLE_ADMIN:
		    $this->file_name = "pages/admin/dashboard.html";
		    break;
		case Constants::USER_ROLE_COMPANY:
		    $this->file_name = "pages/company/dashboard.html";
		    break;
		default:
		    $this->__redirect( URLs::PAGE_NOT_FOUND );
		    break;
	    }

	    $this->template = $this->central->load_normal( $this->file_name );
	}
	catch ( Exception $e )
	{
	    $this->__redirect( Config\URLs::PAGE_NOT_FOUND );
	}
    }

    public function update_main_contents()
    {
	try
	{
	    if ( $_SESSION[ 'user' ][ 'role' ] == Constants::USER_ROLE_ADMIN )
	    {
		$this->companies_details();
		$this->GetCount();
		$this->total_surveys_count();
		$this->company_count();
	    }
	    else if ( $_SESSION[ 'user' ][ 'role' ] == Constants::USER_ROLE_COMPANY )
	    {
		$this->company_details();
	    }
	    $this->questions_count();
	    $this->user_count();
	}
	catch ( Exception $ex )
	{
	    throw $ex;
	}
    }

    //**************************** Admin Start *************************************

    private function GetCount()
    {
//******** Comapny *********
	$this->company_count();
    }

    private function companies_details()
    {
	try
	{
	    $companies_lists = PluSQL::from( $this->profile )->user->select( "*" )->where( "role='" . Constants::USER_ROLE_COMPANY . "'AND user." . Constants::STATUS_DELETED . " <> 1" )->run()->user;
	    $repeat = $this->template->repeat( '.rep-company-details' );
	    foreach ( $companies_lists as $companies_list )
	    {

		$data[ 'company_name' ] = $companies_list->user_name;
		$user_id = $companies_list->user->user_id;
		try
		{
		    $count = 0;
		    $count = PluSQL::from( $this->profile )->survey->select( "count(*) as cnt, survey_id" )->where( "user_id='{$user_id}' AND status='published'" )->run()->survey->cnt;
		    $data[ 'company_count' ] = $count;
		    $repeat->setValue( '#publish-data@value', json_encode( $data ) );
		}
		catch ( Exception $ex )
		{
		    echo $ex->getMessage();
		}
		$repeat->next();
	    }
	    Central::remove_last_repeating_element( $this->template, '.stops', 1, 2, 0 );
	}
	catch ( Exception $ex )
	{
//	    $this->template->setValue( '.rep_events', '<td> No Record Found... </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>', 1 );
	}
    }

    private function total_surveys_count()
    {
	$_table = 'survey';
	$_select = 'survey_id';
	$_where_id = Constants::STATUS_DELETED;
	$_where = '<> 1';
	try
	{
	    $total_surveys_count = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
	    if ( $total_surveys_count != 0 )
	    {
		$this->template->setValue( '#total-surveys-count@value', $total_surveys_count );
	    }
	    else
	    {
		$this->template->setValue( '#total-surveys-count@value', "0" );
	    }
	}
	catch ( Exception $ex )
	{
	    echo $ex->getMessage();
	}
    }

//**************************** Admin End *************************************
//**************************** Company Start **********************************

    private function company_details()
    {
	try
	{
	    $this->user_outliers();
	    $this->comapny_surveys_count();
	    $this->published_surveys_count();
	    $this->un_published_surveys_count();
	}
	catch ( Exception $ex )
	{
	    echo $ex->getMessage();
	}
    }

    private function user_outliers()
    {
	$_table = 'user';
	$_select = 'user_id';
	$_where_id = 'role';
	$_where = Constants::USER_ROLE_USER;
	$_deleted = 'warnings';
	$_deleted_value = '<> 0';
	try
	{
	    $outliers_count = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
	    if ( $outliers_count != 0 )
	    {
		$this->template->setValue( '#outliers-count@value', $outliers_count );
	    }
	    else
	    {
		$this->template->setValue( '#outliers-count@value', $outliers_count );
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    private function comapny_surveys_count()
    {
	$company_id = $_SESSION[ 'user' ][ 'user_id' ];
	$_table = 'survey';
	$_select = 'survey_id';
	$_where_id = 'user_id';
	$_where = $company_id;
	$_deleted = Constants::STATUS_DELETED;
	$_deleted_value = '<> 1';
	try
	{
	    $company_surveys_count = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
	    if ( $company_surveys_count != 0 )
	    {
		$this->template->setValue( '#company-surveys-count@value', $company_surveys_count );
		$this->template->setValue( '.events-count', $company_surveys_count );
	    }
	    else
	    {
		$this->template->setValue( '#company-surveys-count@value', $company_surveys_count );
		$this->template->setValue( '.events-count', $company_surveys_count );
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    private function published_surveys_count()
    {
	$company_id = $_SESSION[ 'user' ][ 'user_id' ];
	$_table = 'survey';
	$_select = 'survey_id';
	$_where_id = 'user_id';
	$_where = $company_id;
	$_deleted = Constants::STATUS;
	$_deleted_value = '="' . Constants::STATUS_PUBLISHED . '"';
	try
	{
	    $published_surveys_count = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
	    if ( $published_surveys_count != 0 )
	    {
		$this->template->setValue( '#published-surveys-count@value', $published_surveys_count );
		$this->template->setValue( '.forms-count', $published_surveys_count );
	    }
	    else
	    {
		$this->template->setValue( '#published-surveys-count@value', $published_surveys_count );
		$this->template->setValue( '.forms-count', $published_surveys_count );
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    private function un_published_surveys_count()
    {
	$company_id = $_SESSION[ 'user' ][ 'user_id' ];
	$_table = 'survey';
	$_select = 'survey_id';
	$_where_id = 'user_id';
	$_where = $company_id;
	$_deleted = Constants::STATUS;
	$_deleted_value = '="' . Constants::STATUS_UNPUBLISHED . '"';
	try
	{
	    $un_published_surveys_count = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
	    if ( $un_published_surveys_count != 0 )
	    {
		$this->template->setValue( '#un-published-surveys-count@value', $un_published_surveys_count );
	    }
	    else
	    {
		$this->template->setValue( '#un-published-surveys-count@value', $un_published_surveys_count );
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    private function company_count()
    {
	$_table = 'user';
	$_select = 'user_id';
	$_where_id = 'role';
	$_where = Constants::USER_ROLE_COMPANY;
	$_deleted = Constants::STATUS_DELETED;
	$_deleted_value = '<> 1';
	try
	{
	    $company_count = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
	    if ( $company_count != 0 )
	    {
		$this->template->setValue( '#company-count@value', $company_count );
		$this->template->setValue( '.events-count', $company_count );
	    }
	    else
	    {
		$this->template->setValue( '#company-count@value', $company_count );
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    /*
      private function survey_count()
      {
      try
      {
      $user_lists = PluSQL::from( $this->profile )->user->select( "*" )->where( "role='" . Constants::USER_ROLE_COMPANY . "'AND user." . Constants::STATUS_DELETED . " <> 1 AND user." . Constants::STATUS . "='active'" )->run()->user;
      $repeat = $this->template->repeat( '.rep-company-question' );
      foreach ( $user_lists as $user_list )
      {

      $repeat->setValue( '#company-names@value', $user_list->user_name );
      $user_id = $user_list->user_id;
      try
      {
      $_table = 'survey';
      $_select = 'survey_id';
      $_where_id = 'user_id';
      $_where = $user_id;
      $_deleted = Constants::STATUS_DELETED;
      $_deleted_value = '<> 1';
      try
      {
      $survey_count = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
      if ( $survey_count != 0 )
      $repeat->setValue( '#survey-count@value', $survey_count );
      else
      $repeat->setValue( '#survey-count@value', "0" );
      }
      catch ( Exception $ex )
      {
      $repeat->setValue( '#question-count@value', "0" );
      }
      }
      catch ( Exception $ex )
      {
      $ex->getMessage();
      }
      try
      {
      $_table = 'question';
      $_select = 'question_id';
      $_where_id = 'user_id';
      $_where = $user_id;
      $_deleted = Constants::STATUS_DELETED;
      $_deleted_value = '<> 1';
      try
      {
      $question_count = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
      if ( $question_count != 0 )
      $repeat->setValue( '#question-count@value', $question_count );
      else
      $repeat->setValue( '#question-count@value', "0" );
      }
      catch ( Exception $ex )
      {
      $repeat->setValue( '#question-count@value', "0" );
      }

      try
      {
      $_table = 'survey';
      $_select = 'survey_id';
      $_where_id = 'user_id';
      $_where = $user_id;
      $_deleted = Constants::STATUS;
      $_deleted_value = "='" . Constants::STATUS_PUBLISHED . "'";
      $pending_surveys = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
      print_r( $pending_surveys );
      if ( $pending_surveys != 0 )
      $repeat->setValue( '#publish-count@value', $pending_surveys );
      else
      $repeat->setValue( '#publish-count@value', '0' );
      }
      catch ( Exception $ex )
      {

      }
      }
      catch ( Exception $ex )
      {
      $this->template->setValue( '#question-count@value', "0" );
      }
      $repeat->next();
      }
      Central::remove_last_repeating_element( $this->template, '.stop', 1, 2, 0 );
      }
      catch ( Exception $ex )
      {
      $ex->getMessage();
      }
      } */

//****************************Company Ends ********************************
//****************************Common Function ********************************
//****************************User Count ********************************
    private function user_count()
    {
	$_table = 'user';
	$_select = 'user_id';
	$_where_id = 'role';
	$_where = Constants::USER_ROLE_USER;
	$_deleted = Constants::STATUS_DELETED;
	$_deleted_value = '<> 1';
	try
	{
	    $user_count = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
	    if ( $user_count != 0 )
	    {
		$this->template->setValue( '.users-count', $user_count );
		$this->template->setValue( '#user-count@value', $user_count );
	    }
	    else
	    {
		$this->template->setValue( '.users-count', "0" );
		$this->template->setValue( '#user-count@value', "0" );
	    }
	}
	catch ( Exception $ex )
	{
	    
	}
    }

    private function questions_count()
    {
	$_table = 'question';
	$_select = 'question_id';
	$_where_id = Constants::STATUS_DELETED;
	$_where = '<> 1';
	try
	{
	    $question_count = $this->count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value );
	    if ( $question_count != 0 )
	    {
		$this->template->setValue( '.questions-count', $question_count );
	    }
	    else
	    {
		$this->template->setValue( '.questions-count', "0" );
	    }
	}
	catch ( Exception $ex )
	{
	    echo $ex->getMessage();
	}
    }

//***************************************  Query  ****************************************************
    private function count_query( $_table, $_select, $_where_id, $_where, $_deleted, $_deleted_value )
    {
	try
	{
	    if ( $_deleted )
		$data = PluSQL::from( $this->profile )->$_table->select( "count(*) as cnt, $_select" )->where( "{$_where_id}='{$_where}' AND {$_deleted} {$_deleted_value}" )->run()->$_table->cnt;
	    else
		$data = PluSQL::from( $this->profile )->$_table->select( "count(*) as cnt, $_select" )->where( "{$_where_id}='{$_where}'" )->run()->$_table->cnt;
	}
	catch ( Exception $ex )
	{
	    $data = 0;
	    $ex->getMessage();
	}
	return $data;
    }

    //****************************Common Function ********************************
}

?>

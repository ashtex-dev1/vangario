$( document ).ready( function ( ) {
    $( function ( ) {
	Highcharts.setOptions( {
	    chart: {
		backgroundColor: {
		    linearGradient: [ 0, 0, 500, 500 ],
		    stops: [
			[ 0, 'rgb(255, 255, 255)' ],
			[ 1, 'rgb(240, 240, 255)' ]
		    ]
		},
		borderWidth: 0,
		plotBackgroundColor: 'rgba(255, 255, 255, .9)',
		plotShadow: true,
		plotBorderWidth: 0,
	    }
	} );
//********** Survey Name ***********	
	var survey_name = [ ];
	var $company = $( ".repeat-comapy-survey" );
	$company.find( '#company-survey-name' ).each( function ( ) {
	    survey_name.push(
		    { name: $( this ).val( ), y: parseInt( $( this ).parent( ).find( "#company-filled-forms" ).val( ) ) }
	    );
	} );
//********** User Count ***********	
	var user_count = [ ];
	var $company = $( ".repeat-comapy-survey" );
	$company.find( '#company-users' ).each( function ( ) {
	    user_count.push( $( this ).val( ) );
	} );
	var user_count = '[' + user_count.join( ', ' ) + ']';
//********** Filled Survey Count ***********	
	var filled_survey_count = [ ];
	var $company = $( ".repeat-comapy-survey" );
	$company.find( '#company-filled-forms' ).each( function ( ) {
	    filled_survey_count.push( $( this ).val( ) );
	} );
	var filled_survey_count = '[' + filled_survey_count.join( ', ' ) + ']';
//********** total user Count **************
	var outliers_count = $( "#outliers-count" ).val( );
	var users_count = $( "#user-count" ).val( );
	var comapny_surveys_count = $( "#company-surveys-count" ).val( );
	var published_surveys_count = $( "#published-surveys-count" ).val( );
	var un_published_surveys_count = $( "#un-published-surveys-count" ).val( );
//************ 1st Chart ****************
	var chart = Highcharts.chart( {
	    chart: {
		renderTo: 'container',
		type: 'bar',
	    },
	    shadow: {
		color: 'yellow',
		width: 10,
		offsetX: 0,
		offsetY: 0
	    },
	    title: {
		text: 'Users and Outliers'
	    },
	    subtitle: {
		text: 'Number Of Users & Number of Outliers'
	    },
	    plotOptions: {
		column: {
		    depth: 25
		}
	    },
	    xAxis: {
		categories: [ 'Users & Outliers' ]
	    },
	    yAxis: {
		title: {
		    text: [ 'Users & Outliers' ]
		}
	    },
	    series: [
		{
		    name: 'Users',
		    data: [ eval( users_count ) ]
		},
		{
		    name: 'Outliers',
		    data: [ eval( outliers_count ) ]
		}
	    ]

	} );
	//************ 2nd Chart ****************

	// Make monochrome colors and set them as default for all pies
	Highcharts.getOptions( ).plotOptions.pie.colors = ( function ( ) {
	    var colors = [ ],
		    base = Highcharts.getOptions( ).colors[0],
		    i;
	    for ( i = 0; i < 10; i += 1 ) {
// Start out with a darkened base color (negative brighten), and end
// up with a much brighter color
		colors.push( Highcharts.Color( base ).brighten( ( i - 3 ) / 7 ).get( ) );
	    }
	    return colors;
	}( ) );
//*******************************************************************************************
	Highcharts.chart( {
	    chart: {
		renderTo: 'container1',
		type: 'column',
	    },
	    shadow: {
		color: 'yellow',
		width: 10,
		offsetX: 0,
		offsetY: 0
	    },
	    title: {
		text: 'Published Surveys & Total Surveys'
	    },
	    subtitle: {
		text: 'Number Of Published Surveys & Number of Total Surveys'
	    },
	    plotOptions: {
		column: {
		    depth: 25
		}
	    },
	    xAxis: {
		categories: [ 'Surveys' ]
	    },
	    yAxis: {
		title: {
		    text: null
		}
	    },
	    series: [
		{
		    name: 'Published Surveys',
		    data: [ eval( published_surveys_count ) ]
		}, {
		    name: 'Un-Published Surveys',
		    data: [ eval( un_published_surveys_count ) ]
		},
		{
		    name: 'Total Surveys',
		    data: [ eval( comapny_surveys_count ) ]
		}
	    ]

	} );
    }
    );
} );
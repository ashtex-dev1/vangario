$( document ).ready( function ( ) {

    // Create the educational institutes pie chart
    $( '.education-chart #container' ).highcharts( {
	chart: {
	    type: 'pie'
	},
	title: {
	    text: 'Educational Institutes'
	},
	subtitle: {
	    text: 'Click the slices to view versions.'
	},
	plotOptions: {
	    series: {
		dataLabels: {
		    enabled: true,
		    format: '{point.name}: {point.y:.1f}%'
		}
	    }
	},
	tooltip: {
	    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
	},
	series: [ {
		name: 'Institutes',
		colorByPoint: true,
		data: JSON.parse( $( '.education-chart input' ).val() )['series'],
		innerSize: '40%',
		showInLegend: true,
		dataLabels: {
		    enabled: false,
		    padding: 0
		}
	    } ],
	drilldown: {
	    series: JSON.parse( $( '.education-chart input' ).val() )['drilldowns']
	}
    } );

    // Create the locations pie chart
    $( '.location-chart #container' ).highcharts( {
	chart: {
	    type: 'pie'
	},
	title: {
	    text: 'Locations'
	},
	subtitle: {
	    text: ''
	},
	plotOptions: {
	    series: {
		dataLabels: {
		    enabled: true,
		    format: '{point.name}: {point.y:.1f}%'
		}
	    }
	},
	tooltip: {
	    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
	},
	series: [ {
		name: 'Areas',
		colorByPoint: true,
		data: JSON.parse( $( '.location-chart input' ).val() )['series'],
		innerSize: '40%',
		showInLegend: true,
		dataLabels: {
		    enabled: false,
		    padding: 0
		}
	    } ],
    } );

    // Create the relationship pie chart
    $( '.relationship-chart #container' ).highcharts( {
	chart: {
	    type: 'pie'
	},
	title: {
	    text: 'Relationships'
	},
	subtitle: {
	    text: ''
	},
	plotOptions: {
	    series: {
		dataLabels: {
		    enabled: true,
		    format: '{point.name}: {point.y:.1f}%'
		}
	    }
	},
	tooltip: {
	    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
	},
	series: [ {
		name: 'Relationship Types',
		colorByPoint: true,
		data: JSON.parse( $( '.relationship-chart input' ).val() )['series'],
		innerSize: '40%',
		showInLegend: true,
		dataLabels: {
		    enabled: false,
		    padding: 0
		}
	    } ],
    } );

    // 
    $( '.age-chart #container' ).highcharts( {
	title: {
	    text: 'Age & Gender'
	},
	xAxis: {
	    categories: [ '10-20', '20-30', '30-40', '40-50', '50+' ]
	},
	labels: {
	    items: [ {
		    html: '',
		    style: {
			left: '50px',
			top: '18px',
			color: ( Highcharts.theme && Highcharts.theme.textColor ) || 'black'
		    }
		} ]
	},
	series: JSON.parse( $( '.age-chart input' ).val() )['bars']
    } );



    $( ".question-chart" ).each( function () {
	var par = $( this );
	var data = JSON.parse( $( "input", par ).val() );
	$( '.container', par ).highcharts( {
	    chart: {
		type: 'column'
	    },
	    title: {
		text: data.title
	    },
	    subtitle: {
		text: ''
	    },
	    xAxis: {
		categories: data.category,
		crosshair: true
	    },
	    yAxis: {
		min: 0,
		title: {
		    text: '# of respondents'
		}
	    },
	    tooltip: {
		pointFormat: '{point.key} {point.y}',
	    },
	    plotOptions: {
		column: {
		    pointPadding: 0.2,
		    borderWidth: 0
		}
	    },
	    series: data.options
	} );
    } );
    $( ".export" ).on( 'click', function ( e ) {
	$( "#analysis-survey-form" ).attr( 'action', $( this ).attr( 'data-action' ) );
	$( "#analysis-survey-form" ).submit();
    } );
} );
#!/bin/bash
result=`ps aux | grep -i "staging=1" | grep -v "grep" | wc -l`
if [ $result -ge 1 ]
   then
        echo "daemon is up"
   else
	cd /var/www/html/dev_portal && sh daemon.sh
        echo "daemon restarted"
fi
